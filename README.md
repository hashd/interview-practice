## Interview Practice Realm

## Questions

### Given an unsorted array of integers, find a pair with given sum in it.

#### Input
```
[8, 7, 2, 5, 3, 1]
10
```
#### Output
```
0 2
```
#### Solution
```java
public static Pair<Integer, Integer> findPairWithSum(int[] arr, int sum) {
      Arrays.sort(arr);

      int left = 0, right = arr.length;
      while (left <= right) {
          if (arr[left] + arr[right] < sum) {
              left = left + 1;
          } else if (arr[left] + arr[right] > sum) {
              right = right + 1;
          } else {
              return new Pair<Integer, Integer>(left, right);
          }
      }
      return null;
  }
```