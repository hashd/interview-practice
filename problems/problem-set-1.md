#### Problem 1
You are given a string S. Each character of S is either ‘a’, or ‘b’. You wish to reverse exactly one sub-string of S such that the new string is lexicographically smaller than all the other strings that you can get by reversing exactly one sub-string.

#### Problem 2
Given two strings I and F, where I is the initial state and F is the final state. Each state will contain ‘a’,’b’ and only one empty slot represented by '_'. Your task is to move from the initial state to final state with minimum number of operation.
Allowed operations are
1. You can swap empty character with any adjacent character. (For example ‘aba_ab’ can be converted into ‘ab_aab’ or ‘abaa_b’).
2. You can swap empty character with next to adjacent character only if adjacent character is different from next to adjacent character. (For example ‘aba_ab’ can be converted into ‘a_abab’ or ‘ababa_’, but ‘ab_aab’ cannot be converted to ‘abaa_b’, because ‘a’ cannot jump over ‘a’).

#### Problem 3: gfg-subarray-with-given-sum
Given an unsorted array of non-negative integers, find a continuous sub-array which adds to a given number.

#### Problem 4: gfg-maximum-index
Given an array A of integers, find the maximum of j - i subjected to the constraint of A[i] <= A[j].

#### Problem 5: gfg-finding-the-numbers
You are given an array A containing 2xN+2 positive numbers, out of which N numbers are repeated exactly once and the other two numbers occur exactly once and are distinct. You need to find the other two numbers and print them in ascending order.

#### Problem 6: gfg-longest-valid-parentheses
Given a string consisting of opening and closing parenthesis, find length of the longest valid parenthesis substring.

#### Problem 7: gfg-jumping-numbers
Given a positive number x, print all Jumping Numbers smaller than or equal to x. A number is called as a Jumping Number if all adjacent digits in it differ by 1. The difference between ‘9’ and ‘0’ is not considered as 1. All single digit numbers are considered as Jumping Numbers. For example 7, 8987 and 4343456 are Jumping numbers but 796 and 89098 are not.

#### Problem 8: gfg-connect-nodes-at-same-level
Write a function to connect all the adjacent nodes at the same level in a binary tree. Initially, all the nextRight pointers point to garbage values. Your function should set these pointers to point next right for each node.

#### Problem 9: gfg-count-bst-nodes-that-lie-in-a-given-range
Given a Binary Search Tree (BST) and a range, count the number of nodes in the BST that lie in the given range.

#### Problem 10: gfg-lru-cache
The task is to design and implement methods of an LRU cache. 

The class has two methods get and set which are defined as follows.
get(x)   : Gets the value of the key x if the key exists in the cache otherwise returns -1
set(x,y) : inserts the value if the key x is not already present. If the cache reaches its capacity it should invalidate the least recently used item before inserting the new item.
In the constructor of the class the size of the cache should be intitialized.

#### Problem 11: gfg-interleaved-strings
Given three strings A, B and C your task is to complete the function isInterleave which returns true if C is an interleaving of A and B else returns false. C is said to be interleaving A and B, if it contains all characters of A and B and order of all characters in individual strings is preserved.

#### Problem 12: gfg-find-triplets-with-sum
Given an array A[] of n elements. The task is to complete the function which returns true if triplets exists in array A[] whose sum is zero else returns false.

#### Problem 13: gfg-word-break
Given an input string and a dictionary of words, find out if the input string can be segmented into a space-separated sequence of dictionary words.

#### Problem 14: gfg-duplicate-subtree-in-binary-tree
Given a Binary Tree, the task is to complete the function dupSub which takes the root of the tree as the only argument and returns true if the Binary tree contains a duplicate sub-tree of size two or more.

#### Problem 15: gfg-find-largest-word-in-dictionary
Giving a dictionary and a string ‘str’, your task is to find the longest string in dictionary of size x which can be formed by deleting some characters of the given ‘str’.

#### Problem 16: gfg-number-of-subsets-and-means
Let Max be the maximum possible mean of a multiset of values obtained from an array.
Let Min be the minimum possible mean of a multiset of values obtained from the same array. Note that in a multiset values may repeat.

The task is to find the number of multisets with mean as Max and the number of multisets with mean as Min. The answer may be too large so output the number of multisets modulo 10^9+7.

#### Problem 17: gfg-find-all-pairs-whose-sum-is-x
Given two unsorted arrays A[] of size n and B[] of size m of distinct elements, the task is to find all pairs from both arrays whose sum is equal to x.

#### Problem 18: gfg-total-decoding-messages
A top secret message containing letters from A-Z is being encoded to numbers using the following mapping: A -> 1, B -> 2 ... so on till Z -> 26. You are an FBI agent. You have to determine the total number of ways that message can be decoded.

Note: An empty digit sequence is considered to have one decoding. It may be assumed that the input contains valid digits from 0 to 9 and If there are leading 0’s, extra trailing 0’s and two or more consecutive 0’s then it is an invalid string.

#### Problem 19: gfg-word-boggle
Given a dictionary, a method to do lookup in dictionary and a M x N board where every cell has one character. Find all possible words that can be formed by a sequence of adjacent characters. Note that we can move to any of 8 adjacent characters, but a word should not have multiple instances of same cell.

#### Problem 20: gfg-activity-selection
Given N activities with their start and finish times. Select the maximum number of activities that can be performed by a single person, assuming that a person can only work on a single activity at a time.

#### Problem 21: gfg-minimum-depth-of-a-binary-tree
Given a binary tree, your task is to complete the function minDepth which takes one argument the root of the binary tree and prints the min depth of  binary tree .

#### Problem 22: gfg-implement-str-str
Your task  is to implement the function strstr. The function takes two strings as arguments(s,x) and  locates the occurrence of the string x in the string s. The function returns and integer denoting  the first occurrence of the string x .

#### Problem 23: gfg-k-palindrome
A string is k palindrome if it can be transformed into a palindrome on removing at most k characters from it. Your task is to complete the function is_k_palin which takes two arguments a string str and a number N . Your function should return true if the string is k palindrome else it should return false.

#### Problem 24: gfg-largest-fibonacci-subsequence
Given an array with positive number the task to find the largest subsequence from array that contain elements which are Fibonacci numbers.

#### Problem 25: gfg-decode-the-pattern
Given a pattern as below and an integer n your task is to decode it and print nth row of it. 

The pattern follows as :
1
11
21
1211
111221