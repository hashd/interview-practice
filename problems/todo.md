#### Problem 1
https://www.careercup.com/question?id=5721539260448768
``` text
Write a Java code that take a string of parenthesis as input and return if the string is valid or not . The input will have '(' and ')' and also '*' and * serves as wild card and can be used in place of both '(' and ')' or it can be null. 

For example,the String (*)(*)(** is a valid String. 

Follow up: What if '[]' and '{}' are also in the string along with '()' and * can be used in place of any of them or can be considered as null?
```

#### Problem 2
https://www.careercup.com/question?id=5650714578649088
```
Snake and ladder game. Given a board state with position information for snakes and ladders, find the minimum number of ways(aka dice throws) one can reach from start to end.
```

#### Problem 3