package com.gitlab.hashd;

import java.lang.reflect.Method;
import java.util.stream.Collectors;

public class App {
    public static void testReflection() {
        int[] arr = {1, 2, 3};
        Class arrayClass = arr.getClass();

        for (Method method: arrayClass.getMethods()) {
            System.out.println(method.getName());
            System.out.println(method.getParameters());
        }
    }

    public static void main( String[] args ) {
//        StringProblems.generateParenthesisStrings(4);
        StringProblems.generateParenthesisStringsAlt(4).forEach(System.out::println);
    }
}
