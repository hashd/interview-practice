package com.gitlab.hashd;

import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.heaps.MaxHeap;
import com.gitlab.hashd.heaps.MinHeap;
import com.gitlab.hashd.utils.Point;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"ManualArrayCopy", "WeakerAccess"})
public class ArrayProblems {
    public static void swap(int[] arr, int idx1, int idx2) {
        int tmp = arr[idx2];
        arr[idx2] = arr[idx1];
        arr[idx1] = tmp;
    }

    public static long findMaxProductSubarray(int[] arr) {
        long maxTillNow = 1, minTillNow = 1, maxSoFar = 0;
        for (int num: arr) {
            long tmp = maxTillNow;
            maxTillNow = Math.max(num, Math.max(num * maxTillNow, num * minTillNow));
            minTillNow = Math.min(num, Math.min(num * tmp, num * minTillNow));
            maxSoFar = Math.max(maxTillNow, maxSoFar);
        }

        return maxSoFar;
    }

    /*
    * Given an unsorted array of integers whose each element lies in range 0 to n-1 where n is the size of the array,
    * rearrange array such that A[A[i]] is set to i for every element A[i] in the array.
    * */
    public static void rearrangeWithConstraints1(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < arr.length; i++) {
            arr[arr[i] % n] = (arr[arr[i] % n] + i * arr.length);
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] /= n;
        }
    }

    /*
    * Reverse every consecutive m elements of the given subarray
    * */
    public static int[] reverseConsecutiveElementsWithConstraints1(int[] arr, int size, int begin, int end) {
        int[] copy = Arrays.copyOf(arr, arr.length);
        if (end - begin + 1 < size) {
            return copy;
        }

        for (int i = begin; i < end; i = i + size) {
            int left = i, right = left + size - 1;
            if (right > end) {
                break;
            }

            while (left < right) {
                swap(copy, left, right);
                left++;
                right--;
            }
        }

        return copy;
    }

    /*
    * Given an array of integers, find a subset in it that has maximum product of its elements.
    * */
    public static long findMaximumProductOfSubset(int[] arr) {
        if (arr.length == 0) return 0;

        long maxProduct = 1;
        int minNegative = Integer.MIN_VALUE, numOfNegatives = 0;
        for (int num : arr) {
            if (num < 0) {
                numOfNegatives++;
                minNegative = Math.max(num, minNegative);
            }

            maxProduct = maxProduct * ((num == 0) ? 1: num);
        }

        return maxProduct / (numOfNegatives % 2 == 1 ? minNegative: 1);
    }

    public static List<Pair<Integer, Integer>> mergeIntervals(List<Pair<Integer, Integer>> intervals) {
        Stack<Pair<Integer, Integer>> intervalStack = new Stack<>();
        for (Pair<Integer, Integer> interval: intervals) {
            if (intervalStack.size() == 0) {
                intervalStack.push(interval);
            } else {
                Pair<Integer, Integer> top = intervalStack.pop();
                if (top.getValue() >= interval.getKey()) {
                    intervalStack.push(new Pair<>(top.getKey(), Math.max(top.getValue(), interval.getValue())));
                } else {
                    intervalStack.push(top);
                    intervalStack.push(interval);
                }
            }
        }

        return new ArrayList<>(intervalStack);
    }

    // TODO: Complete this
    public static int findNumberOfInversions(int[] arr) {
        return 0;
    }

    public static int findNoOfRotationsInCircularSortedArray(int[] arr) {
        int low = 0, high = arr.length, mid = 0, n = arr.length;
        while (low < high) {
            mid = low + (high - low) / 2;

            if (arr[mid] < arr[(mid + 1 + n) % n] && arr[mid] < arr[(mid - 1 + n) % n]) {
                return mid;
            }

            if (arr[mid] > arr[low]) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        return 0;
    }

    // TODO: Fix this
    public static int findSmallestMissingElement(int[] arr) {
        int low = 0, high = arr.length, mid = -1;
        while (low < high) {
            mid = low + (high - low) / 2;
            if (arr[mid] == mid) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        return mid + 1;
    }

    public static int findMaximumProfit(int[] stocks) {
        int maxProfit = 0, minPriceSoFar = Integer.MAX_VALUE;
        for (int stock: stocks) {
            minPriceSoFar = Math.min(stock, minPriceSoFar);
            maxProfit = Math.max(maxProfit, stock - minPriceSoFar);
        }
        return maxProfit;
    }

    public static boolean checkIfArrayContainsDuplicatesWithinKDistance(int[] arr, int k) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            if (set.contains(arr[i])) {
                return true;
            }

            if (i >= k) {
                set.remove(arr[i - k]);
            }
            set.add(arr[i]);
        }
        return false;
    }


    public static int[] getMaxMinArrangement(int[] arr) {
        int[] maxMinArr = new int[arr.length];
        int left = 0, right = arr.length - 1, idx = 0;
        while (left < right) {
            arr[idx] = arr[right--];
            arr[idx + 1] = arr[left++];
            idx = idx + 2;
        }
        if (left == right) {
            arr[idx] = arr[left];
        }
        return maxMinArr;
    }

    public static int[] applyTransformation(int[] arr, int a, int b, int c) {
        return Arrays.stream(arr).map(v -> a * v * v + b * v + c).toArray();
    }





    public static int numberOfPointsNotDominated(Point[] points) {
        int number = 0, maxY = 0, maxX = 0;
        List<Point> sortedPoints = Arrays.stream(points).sorted(Comparator.comparing(Point::getX)).collect(Collectors.toList());
        Collections.reverse(sortedPoints);

        for (Point p: points) {
            if (p.getX() < maxX && p.getY() < maxY) {

            }
        }
        return number;
    }

    public static int minimumPositivePointsToReachDestination(int[][] grid) {
        if (grid == null) return 0;
        int rows = grid.length, cols = grid[0].length;
        int[][] maxs = new int[rows][cols];

        maxs[0][0] = grid[0][0];
        for (int i = 1; i < rows; i++) maxs[i][0] = maxs[i - 1][0] + grid[i][0];
        for (int i = 1; i < cols; i++) maxs[0][i] = maxs[0][i - 1] + grid[0][i];
        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                maxs[i][j] = grid[i][j] + Math.max(maxs[i - 1][j], maxs[i][j - 1]);
            }
        }

        for (int i = 1; i < rows; i++) maxs[i][0] = Math.min(maxs[i][0], maxs[i - 1][0]);
        for (int i = 1; i < cols; i++) maxs[0][i] = Math.min(maxs[0][i], maxs[0][i - 1]);
        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                int min =  Math.min(maxs[i - 1][j], maxs[i][j - 1]);
                maxs[i][j] = Math.min(min, maxs[i][j]);
            }
        }

        return maxs[rows - 1][cols - 1] < 0? -maxs[rows - 1][cols - 1] + 1: 0;
    }
}
