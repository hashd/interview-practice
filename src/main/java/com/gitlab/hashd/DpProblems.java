package com.gitlab.hashd;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DpProblems {
    public static long findNumberOfPaths(int rows, int cols) {
        if (rows == 0 || cols == 0) return 0;

        long[][] ways = new long[rows][cols];
        IntStream.range(0, rows).forEach(idx -> ways[idx][0] = 1);
        IntStream.range(0, cols).forEach(idx -> ways[0][idx] = 1);

        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                ways[i][j] = ways[i][j - 1] + ways[i - 1][j];
            }
        }
        return ways[rows - 1][cols - 1];
    }

    public static int getLongestCommonSubstring(String a, String b) {
        if (a == null || b == null) return 0;

        int aLength = a.length(), bLength = b.length();
        int[][] lcs = new int[aLength + 1][bLength + 1];
        IntStream.rangeClosed(0, aLength).forEach(idx -> lcs[idx][0] = 0);
        IntStream.rangeClosed(0, bLength).forEach(idx -> lcs[0][idx] = 0);

        int maxSoFar = 0;
        for (int i = 1; i <= aLength; i++) {
            for (int j = 1; j <= bLength; j++) {
                lcs[i][j] = a.charAt(i - 1) == b.charAt(j - 1)? lcs[i-1][j-1] + 1: 0;
                maxSoFar = Math.max(maxSoFar, lcs[i][j]);
            }
        }

        return maxSoFar;
    }

    public static int getLCS(String a, String b) {
        if (a == null || b == null) return 0;

        int aLength = a.length(), bLength = b.length();
        int[][] lcs = new int[aLength + 1][bLength + 1];
        IntStream.rangeClosed(0, aLength).forEach(idx -> lcs[idx][0] = 0);
        IntStream.rangeClosed(0, bLength).forEach(idx -> lcs[0][idx] = 0);

        for (int i = 1; i <= aLength; i++) {
            for (int j = 1; j <= bLength; j++) {
                lcs[i][j] = Arrays.stream(new int[]{lcs[i - 1][j], lcs[i][j - 1], lcs[i-1][j-1] + (a.charAt(i - 1) == b.charAt(j - 1)? 1: 0)}).max().getAsInt();
            }
        }

        return lcs[aLength][bLength];
    }

    public static int getLIS(int[] numbers) {
        int[] copy = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(copy);

        int[] a = numbers, b = copy;
        int aLength = a.length, bLength = b.length;
        int[][] lis = new int[aLength + 1][bLength + 1];

        IntStream.rangeClosed(0, aLength).forEach(idx -> lis[idx][0] = 0);
        IntStream.rangeClosed(0, bLength).forEach(idx -> lis[0][idx] = 0);

        for (int i = 1; i <= aLength; i++) {
            for (int j = 1; j <= bLength; j++) {
                lis[i][j] = Arrays.stream(new int[]{lis[i - 1][j], lis[i][j - 1], lis[i-1][j-1] + (a[i-1] == b[j-1]? 1: 0)}).max().getAsInt();
            }
        }

        return lis[aLength][bLength];
    }

    public static int getLongestPalindromicSubsequence(String str) {
        int[][] lps = new int[str.length()][str.length()];
        IntStream.range(0, str.length()).forEach(idx -> lps[idx][idx] = 1);

        return getLongestPalindromicSubsequence(str, 0, str.length() - 1, lps);
    }

    private static int getLongestPalindromicSubsequence(String str, int start, int end, int[][] lps) {
        if (start > end) return 0;
        if (start == end) return 1;
        if (lps[start][end] != 0) return lps[start][end];

        if (str.charAt(start) == str.charAt(end)) {
            lps[start][end] = getLongestPalindromicSubsequence(str, start + 1, end - 1, lps) + 2;
        } else {
            lps[start][end] = Math.max(getLongestPalindromicSubsequence(str, start + 1, end, lps), getLongestPalindromicSubsequence(str, start, end - 1, lps));
        }
        return lps[start][end];
    }

    public static int getMaxSum(int[][] matrix) {
        int rows = matrix.length, cols = matrix[0].length;
        int[][] sums = new int[rows][cols + 1];
        int maxSoFar = 0;

        for (int i = 0; i < rows; i++) {
            sums[i][0] = 0;
            for (int j = 1; j <= cols; j++) {
                sums[i][j] = sums[i][j - 1] + matrix[i][j - 1];
            }
        }

        int[] tmp = new int[rows];
        for (int i = 0; i < cols; i++) {
            for (int j = i + 1; j <= cols; j++) {
                Arrays.fill(tmp, 0);
                int tmpMax = 0, sumTillNow = 0;

                for (int k = 0; k < rows; k++) {
                    tmp[k] = sums[k][j] - sums[k][i];
                    sumTillNow =  Math.max(0, sumTillNow + tmp[k]);
                    tmpMax = Math.max(tmpMax, sumTillNow);
                }

                maxSoFar = Math.max(tmpMax, maxSoFar);
            }
        }

        return maxSoFar;
    }
}
