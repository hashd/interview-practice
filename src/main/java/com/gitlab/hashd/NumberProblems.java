package com.gitlab.hashd;

import java.util.stream.IntStream;

public class NumberProblems {
    public static int sumFrom1To(int n) {
        return IntStream.rangeClosed(1, n).sum();
    }

    public static int gcd(int a, int b) {
        int tmp;
        while (a != 0 && b != 0) {
            tmp = b; b = a % b; a = tmp;
        }
        return a == 0 ? b : a;
    }

    public static long lcm(int a, int b) {
        int gcd = gcd(a, b);
        return (1L * a * b) / gcd;
    }

    public static long[] fibonacciNumbers(int n) {
        long[] fibonacciNumbers = new long[n];
        fibonacciNumbers[0] = 0;
        fibonacciNumbers[1] = 1;

        for (int i = 0; i < n; i++) {
            fibonacciNumbers[i] = fibonacciNumbers[i - 1] + fibonacciNumbers[i - 2];
        }
        return fibonacciNumbers;
    }
}
