package com.gitlab.hashd;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringProblems {
    private static void addToParenthesisSet(String str, int open, int remaining, Set<String> pairs) {
        if (remaining == 0 && open == 0) {
            pairs.add(str);
            return;
        }

        if (open > 0) {
            addToParenthesisSet(str + ")", open - 1, remaining, pairs);
        }

        if (remaining > 0) {
            addToParenthesisSet(str + "(", open + 1, remaining - 1, pairs);
        }
    }

    public static Set<String> generateParenthesisStrings(int n) {
        Set<String> parenthesisStrings = new HashSet<>();
        addToParenthesisSet("", 0, n, parenthesisStrings);

        for (String str: parenthesisStrings) {
            System.out.println(str);
        }
        return parenthesisStrings;
    }

    public static Set<String> generateParenthesisStringsAlt(int n) {
        if (n <= 0) {
            return Collections.emptySet();
        }

        Set<String> parenthesisStrings = new HashSet<>();
        parenthesisStrings.add("");

        for (int i = 0; i < n; i++) {
            parenthesisStrings = parenthesisStrings.stream()
                    .flatMap(input -> Stream.of("()" + input, "(" + input + ")", input + "()"))
                    .collect(Collectors.toSet());
        }

        return parenthesisStrings;
    }

    public static List<String> getBinaryStrings(int size) {
        List<String> binaryStrings = new LinkedList<>();
        addBinaryStrings("", size, binaryStrings);

        return binaryStrings;
    }

    private static void addBinaryStrings(String s, int remaining, List<String> binaryStrings) {
        if (remaining == 0) {
            binaryStrings.add(s);
            return;
        }
        addBinaryStrings(s + "0", remaining - 1, binaryStrings);
        addBinaryStrings(s + "1", remaining - 1, binaryStrings);
        return;
    }

    public static List<String> getCombinationStrings(char[] chars, int size) {
        List<String> combinations = new LinkedList<>();
        addCombinations("", chars, size, combinations);

        return combinations;
    }

    private static void addCombinations(String s, char[] chars, int remaining, List<String> combinations) {
        if (remaining == 0) {
            combinations.add(s);
            return;
        }

        for (int i = 0; i < chars.length; i++) {
            addCombinations(s + chars[i], chars,remaining - 1, combinations);
        }
        return;
    }

    public static int getLongestSubstringOfVowels(String s) {
        int max = 0, curr = 0;
        char[] chars = s.toCharArray();
        for (char c: chars) {
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                curr = curr + 1;
            } else {
                max = Math.max(curr, max);
                curr = 0;
            }
        }

        return max;
    }
}
