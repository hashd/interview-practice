package com.gitlab.hashd.ads;

import java.util.EmptyStackException;

public class LinkedList {
    private LinkedListNode head;
    private int            length;

    public LinkedList() {
    }

    public LinkedList(int[] arr) {
        for (int i = arr.length - 1; i >= 0; i--) {
            add(arr[i]);
        }
    }

    public void add(int val) {
        LinkedListNode node = new LinkedListNode(val);
        node.next = head;
        head = node;

        length = length + 1;
    }

    public void addLast(int val) {
        LinkedListNode node = new LinkedListNode(val);
        if (head == null) {
            head = node;
        } else {
            LinkedListNode piv = head;
            while (piv.next != null) {
                piv = piv.next;
            }
            piv.next = head;
        }

        length = length + 1;
    }

    public void unshift(int val) {
        add(val);
    }

    public void push(int val) {
        addLast(val);
    }

    public int pop() {
        return remove();
    }

    public int shift() {
        return removeLast();
    }

    public int remove() {
        if (length == 0) {
            throw new EmptyStackException();
        } else {
            LinkedListNode piv = head;
            int data = piv.data;
            head = head.next;
            length = length - 1;

            return data;
        }
    }

    public int removeLast() {
        if (length == 0) {
            throw new EmptyStackException();
        } else if (length == 1) {
            int value = head.data;
            head = null;
            return value;
        } else {
            LinkedListNode piv = head;
            while (piv.next != null && piv.next.next != null) {
                piv = piv.next;
            }

            int value = piv.next.data;
            piv.next = null;
            return value;
        }
    }

    public int size() {
        return length;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LinkedList) {
            LinkedList list = (LinkedList) o;
            if (list.length != this.length) {
                return false;
            }

            LinkedListNode aPiv = head, bPiv = list.head;
            while (aPiv != null && bPiv != null) {
                if (aPiv.data != bPiv.data) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    private class LinkedListNode {
        int            data;
        LinkedListNode next;

        LinkedListNode(int data) {
            this.data = data;
            this.next = null;
        }
    }
}
