package com.gitlab.hashd.arrays;

public class KadaneWrapArray {
    // TODO: Not implemented
    public int solve(int[] arr) {
        if (arr == null || arr.length == 0) return 0;

        int[] newArr = new int[2 * arr.length];
        for (int i = 0; i < 2 * arr.length; i++) {
            newArr[i] = arr[i % arr.length];
        }

        return 0;
    }
}
