package com.gitlab.hashd.arrays;

import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.heaps.MaxHeap;

/**
 * Given an array and a number k where k is smaller than size of array, the task is to
 * find the k’th smallest element in the given array. It is given that all array elements are distinct.
 *
 * http://practice.geeksforgeeks.org/problems/kth-smallest-element/0
 */
public class KthSmallestElementFinder {
    public int findKthSmallestElement(int[] arr, int k) {
        Heap maxHeap = new MaxHeap();
        for (int i = 0; i < arr.length; i++) {
            maxHeap.push(arr[i]);
            if (maxHeap.size() > k) {
                maxHeap.pop();
            }
        }

        return maxHeap.pop();
    }
}
