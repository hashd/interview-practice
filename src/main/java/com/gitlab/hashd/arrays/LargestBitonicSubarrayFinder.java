package com.gitlab.hashd.arrays;

public class LargestBitonicSubarrayFinder {
    public int findLargestBitonicSubarray(int[] arr) {
        int max = 1, current = 0;
        boolean isIncreasing = true;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                current = current + 1;
                isIncreasing = false;
            } else if (arr[i] > arr[i - 1]) {
                current = isIncreasing ? (current + 1): 2;
                isIncreasing = true;
            } else {
                current = current + 1;
            }

            max = Math.max(max, current);
        }

        return max;
    }
}
