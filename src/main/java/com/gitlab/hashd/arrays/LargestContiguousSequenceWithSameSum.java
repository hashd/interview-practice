package com.gitlab.hashd.arrays;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class LargestContiguousSequenceWithSameSum {/*
    * Given two Boolean arrays X and Y, find the length of longest continuous sequence that starts
    * and ends at same index in both arrays and have same sum. In other words, find max(j-i+1) for
    * every j >= i where sum of sub-array X[i, j] is equal to sum of sub-array Y[i, j].
    * */
    public Pair<Integer, Integer> findLongestSequenceWithSameSum(int[] arr1, int[] arr2) {
        int[] sumTillNow1 = new int[arr1.length + 1];
        sumTillNow1[0] = 0;
        for (int i = 1; i <= arr1.length; i++) {
            sumTillNow1[i] = sumTillNow1[i - 1] + arr1[i - 1];
        }

        int[] sumTillNow2 = new int[arr2.length + 1];
        sumTillNow2[0] = 0;
        for (int i = 1; i <= arr2.length; i++) {
            sumTillNow2[i] = sumTillNow2[i - 1] + arr2[i - 1];
        }

        int maxLength = 0, maxIndex = Math.min(arr1.length, arr2.length);
        Pair<Integer, Integer> best = new Pair<>(0, 0);
        Map<Integer, Integer> differenceMap = new HashMap<>();
        for (int i = 0; i <= maxIndex; i++) {
            int difference = sumTillNow2[i] - sumTillNow1[i];
            if (differenceMap.containsKey(difference)) {
                maxLength = Math.max(maxLength, i - differenceMap.get(difference) + 1);
                if (maxLength == i - differenceMap.get(difference) + 1) {
                    best = new Pair<>(differenceMap.get(difference), i - 1);
                }
            } else {
                differenceMap.put(difference, i);
            }
        }

        return best;
    }
}
