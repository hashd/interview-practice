package com.gitlab.hashd.arrays;

import java.util.Arrays;

/*
* Find largest number possible from set of given numbers. The numbers should be appended
* to each other in any order to form the largest number.
*
* http://www.geeksforgeeks.org/given-an-array-of-numbers-arrange-the-numbers-to-form-the-biggest-number/
* */
public class LargestNumberPossibleFinder {
    // Using streams and declarative approach
    public String findLargestPossibleNumberS(int[] arr) {
        return Arrays.stream(arr)
                .mapToObj(Integer::toString)
                .sorted((String o1, String o2) -> (o2 + o1).compareTo(o1 + o2))
                .reduce("", (String acc, String str) -> acc + str);
    }

    // Using the traditional imperative approach
    public String findLargestPossibleNumber(int[] arr) {
        String[] numberStrings = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            numberStrings[i] = Integer.toString(arr[i]);
        }

        Arrays.sort(numberStrings, (String o1, String o2) -> (o2 + o1).compareTo(o1 + o2));

        StringBuilder largestNumberString = new StringBuilder();
        for (String str: numberStrings) {
            largestNumberString.append(str);
        }

        return largestNumberString.toString();
    }
}
