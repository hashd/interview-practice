package com.gitlab.hashd.arrays;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Write a program to print all the LEADERS in the array.
 * An element is leader if it is greater than all the elements to its right side.
 * The rightmost element is always a leader.
 *
 * http://practice.geeksforgeeks.org/problems/leaders-in-an-array/0
 */
public class LeadersInArrayFinder {
    public List<Integer> getLeaders(int[] arr) {
        if (arr == null || arr.length == 0) return Collections.emptyList();

        List<Integer> leaders = new LinkedList<>();
        leaders.add(arr[arr.length - 1]);

        int currentMax = arr[arr.length - 1];
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] > currentMax) {
                currentMax = arr[i];
                leaders.add(arr[i]);
            }
        }

        return leaders;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 0};
        List<Integer> leaders = new LeadersInArrayFinder().getLeaders(arr);
        leaders.forEach(System.out::println);
    }
}
