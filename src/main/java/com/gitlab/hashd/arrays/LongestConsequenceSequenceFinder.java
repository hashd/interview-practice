package com.gitlab.hashd.arrays;

import java.util.HashSet;
import java.util.Set;

public class LongestConsequenceSequenceFinder {
    public int length(int[] arr) {
        if (arr == null || arr.length == 0) return 0;

        Set<Integer> numbers = new HashSet<>();
        for (int num: arr) {
            numbers.add(num);
        }

        int result = 0;
        for (int num: arr) {
            if (numbers.contains(num)) {
                int left = 0, right = 0;

                numbers.remove(num);
                while (numbers.contains(num - left - 1)) {
                    left = left + 1;
                    numbers.remove(num - left);
                }

                while (numbers.contains(num + right + 1)) {
                    right = right + 1;
                    numbers.remove(num + right);
                }
                result = Math.max(result, left + right + 1);
            }
        }

        return result;
    }
}
