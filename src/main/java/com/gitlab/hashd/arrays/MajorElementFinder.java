package com.gitlab.hashd.arrays;

public class MajorElementFinder {
    public int findMajorityElement(int[] arr) {
        int major = 0, occurrences = 0;
        for (int num: arr) {
            if (occurrences == 0) {
                major = num;
                occurrences += 1;
            } else if (major == num) {
                occurrences += 1;
            } else {
                occurrences -= 1;
            }
        }

        return major;
    }
}
