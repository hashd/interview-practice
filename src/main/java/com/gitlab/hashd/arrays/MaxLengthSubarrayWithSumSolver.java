package com.gitlab.hashd.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaxLengthSubarrayWithSumSolver {
    public int findMaximumLengthSubArrayWithSum(int[] arr, int sum) {
        int[] sumArray = new int[arr.length + 1];
        sumArray[0] = 0;

        int sumTillNow = 0;
        for (int i = 0; i < arr.length; i++) {
            sumTillNow += arr[i];
            sumArray[i + 1] = sumTillNow;
        }

        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i <= arr.length; i++) {
            if (!map.containsKey(sumArray[i])) {
                map.put(sumArray[i], new ArrayList<>());
            }
            map.get(sumArray[i]).add(i);
        }

        int maxTillNow = -1;
        for (Map.Entry<Integer, List<Integer>> entry: map.entrySet()) {
            int key = entry.getKey();
            List<Integer> indices = entry.getValue();
            int leastIndex = indices.get(0);

            if (map.containsKey(sum + key)) {
                List<Integer> pairIndices = map.get(sum + key);
                int maxPairIndex = pairIndices.get(pairIndices.size() - 1);

                maxTillNow = Math.max(maxPairIndex - leastIndex, maxTillNow);
            }
        }

        return maxTillNow;
    }
}
