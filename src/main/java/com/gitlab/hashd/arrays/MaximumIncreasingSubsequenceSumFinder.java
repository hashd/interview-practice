package com.gitlab.hashd.arrays;

import java.util.Arrays;

/**
 * Given an array of n positive integers. Write a program to find the sum of maximum sum subsequence
 * of the given array such that the integers in the subsequence are sorted in increasing order.
 *
 * http://practice.geeksforgeeks.org/problems/maximum-sum-increasing-subsequence/0
 */
public class MaximumIncreasingSubsequenceSumFinder {
    public int getMaximumIncreasingSubsequenceSum(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return 0;
        }

        int[] maxTillNow = new int[numbers.length];
        System.arraycopy(numbers, 0, maxTillNow, 0, numbers.length);

        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] > numbers[i]) {
                    maxTillNow[j] = Math.max(maxTillNow[j], numbers[j] + maxTillNow[i]);
                }
            }
        }

        return Arrays.stream(maxTillNow).max().getAsInt();
    }

    public static void main(String[] args) {
        MaximumIncreasingSubsequenceSumFinder missf = new MaximumIncreasingSubsequenceSumFinder();
        System.out.println(missf.getMaximumIncreasingSubsequenceSum(new int[]{1, 101, 2, 3, 100, 4, 5}));
        System.out.println(missf.getMaximumIncreasingSubsequenceSum(new int[]{10, 5, 4, 3}));
    }
}
