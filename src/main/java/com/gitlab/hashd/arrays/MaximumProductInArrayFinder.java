package com.gitlab.hashd.arrays;

public class MaximumProductInArrayFinder {
    public long findMaxProduct(int[] arr) {
        int pos1 = 0, pos2 = 0, neg1 = 0, neg2 = 0;
        for (int anArr : arr) {
            if (anArr < 0) {
                if (anArr < neg1) {
                    neg2 = neg1;
                    neg1 = anArr;
                } else if (anArr < neg2) {
                    neg2 = anArr;
                }
            } else {
                if (anArr > pos1) {
                    pos2 = pos1;
                    pos1 = anArr;
                } else if (anArr > pos2) {
                    pos2 = anArr;
                }
            }
        }

        return Math.max(neg1 * neg2, pos1 * pos2);
    }
}
