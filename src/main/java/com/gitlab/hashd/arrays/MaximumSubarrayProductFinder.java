package com.gitlab.hashd.arrays;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class MaximumSubarrayProductFinder {
    public long getMaxProduct(int[] arr) {
        if (arr == null || arr.length == 0) return 0;

        int max = arr[0], min = arr[0], maxSoFar = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < 0) {
                int tmp = max;
                max = min;
                min = max;
            }

            max = max(arr[i], max * arr[i]);
            min = min(arr[i], min * arr[i]);
            System.out.println("Max: " + max + ", Min: " + min);
            maxSoFar = max(max, maxSoFar);
        }

        return maxSoFar;
    }

    public static void main(String[] args) {
        MaximumSubarrayProductFinder mspf = new MaximumSubarrayProductFinder();
        int[] arr = {2, 3, -2, 4, -8};

        System.out.println(mspf.getMaxProduct(arr));
    }
}
