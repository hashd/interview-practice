package com.gitlab.hashd.arrays;

import com.gitlab.hashd.utils.Tuple;

/**
 * Given an array containing both negative and positive integers.
 * Find the contiguous sub-array with maximum sum.
 *
 * http://practice.geeksforgeeks.org/problems/kadanes-algorithm/0
 */
public class MaximumSubarraySumFinder {
    public Tuple findMaximumSubarray(int[] arr) {
        int maxSoFar = 0, currentSum = 0;
        int leftIndex = -1, rightIndex = -1;
        Tuple indices = null;

        for (int i = 0; i < arr.length; i++) {
            currentSum += arr[i];
            if (currentSum < 0) {
                currentSum = 0;
                leftIndex = i + 1;
            } else if (currentSum > maxSoFar) {
                rightIndex = i;
                maxSoFar = currentSum;
                indices = new Tuple(leftIndex, rightIndex);
            }
        }

        return indices;
    }

    public int findMaximumSubarraySum(int[] arr) {
        int maxSoFar = 0, currentSum = 0;
        for (int n: arr) {
            currentSum = Math.max(currentSum + n, 0);
            maxSoFar = Math.max(currentSum, maxSoFar);
        }

        return maxSoFar;
    }
}
