package com.gitlab.hashd.arrays;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class MaximumSubsequenceOfOnesFinder {
    /*
    * Given an Boolean array, find the maximum sequence of continuous 1’s that can be formed
    * by replacing at-most k zeroes by ones.
    * */
    public static int findMaximumSequenceOfOnes(int[] arr, int k) {
        int onesToLeft = 0, maxSoFar = 0, sum = 0;

        // Create auxillary array with information of consecutive ones to the left
        int[] leftOnes = new int[arr.length];
        Arrays.fill(leftOnes, -1);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                leftOnes[i] = onesToLeft;
            }
            onesToLeft = (arr[i] == 1) ? onesToLeft + 1: 0;
        }

        // Create deque as a sliding window to calculate the maximum contiguous 1s by replacing k 0s
        Deque<Integer> values = new ArrayDeque<>(k + 1);
        for (int i = 0; i < arr.length; i++) {
            if (leftOnes[i] != -1) {
                values.addLast(leftOnes[i]);
                sum += leftOnes[i];
            }

            if (values.size() == k + 1) {
                maxSoFar = Math.max(sum, maxSoFar);
                sum -= values.removeFirst();
            }
        }

        return maxSoFar + k;
    }
}
