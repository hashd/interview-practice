package com.gitlab.hashd.arrays;

import com.gitlab.hashd.heaps.MinHeap;

import java.util.Arrays;

public class MergeKSortedArrays {
    public int[] merge(int[][] arrays) {
        int numOfElements = Arrays.stream(arrays).mapToInt((int[] array) -> array.length).sum();
        int[] pivots = new int[arrays.length];
        int[] sorted = new int[numOfElements];

        MinHeap heap = new MinHeap();

        for (int i = 0; i < arrays.length; i++) {
            heap.push(arrays[i][pivots[i]]);
        }

        for (int i = 0; i < numOfElements; i++) {
            sorted[i] = heap.pop();

            // TODO: Heap should store this information along with value so that this part
            //       can be omitted thus making the code more efficient
            for (int j = 0; j < arrays.length; j++) {
                if (pivots[j] < arrays[j].length && sorted[i] == arrays[j][pivots[j]]) {
                    pivots[j]++;
                    if (pivots[j] < arrays[j].length) {
                        heap.push(arrays[j][pivots[j]]);
                    }
                }
            }
        }

        return sorted;
    }
}
