package com.gitlab.hashd.arrays;

public class MergeSortedArrays {
    public void merge(int[] a, int[] b) {
        int pivot = a.length - 1;
        for (int i = pivot; i >= 0; i--) {
            if (a[i] != 0) {
                a[pivot--] = a[i];
            }
        }

        int aPivot = b.length, bPivot = 0, pos = 0;
        while (aPivot < a.length && bPivot < b.length) {
            if (a[aPivot] <= b[bPivot]) {
                a[pos++] = a[aPivot++];
            } else {
                a[pos++] = b[bPivot++];
            }
        }

        while (bPivot < b.length) {
            a[pos++] = b[bPivot++];
        }
    }
}
