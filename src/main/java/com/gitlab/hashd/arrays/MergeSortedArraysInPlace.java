package com.gitlab.hashd.arrays;

import java.util.Arrays;

/**
 * Given an array of size m + n in which the first m numbers are sorted in non-decreasing order
 * and the last n slots are empty. Given another array of size n which is sorted in non-decreasing
 * order. Write an efficient function to merge these two arrays without using any extra space.
 */
public class MergeSortedArraysInPlace {
    public void merge(int[] a, int[] b) {
        int neg = 0;
        for (neg = 0; neg < a.length; neg++) {
            if (a[neg] == -1) break;
        }

        for (int i = neg - 1; i >= 0; i--) {
            a[a.length - 1 - (neg - 1 - i)] = a[i];
        }

        int aPivot = a.length - 1 - neg + 1, bPivot = 0, pivot = 0;
        while (aPivot < a.length && bPivot < b.length) {
            if (a[aPivot] <= b[bPivot]) {
                a[pivot++] = a[aPivot++];
            } else {
                a[pivot++] = b[bPivot++];
            }
        }

        while (bPivot < b.length) a[pivot++] = b[bPivot++];
    }

    public static void main(String[] args) {
        int[] a = {1,3,5,7,9,-1,-1,-1,-1};
        int[] b = {2,4,6,8};

        MergeSortedArraysInPlace msaip = new MergeSortedArraysInPlace();
        msaip.merge(a, b);

        System.out.println(Arrays.toString(a));
    }
}
