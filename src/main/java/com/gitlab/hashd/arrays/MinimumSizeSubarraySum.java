package com.gitlab.hashd.arrays;

/*
Given an array of n positive integers and a positive integer s, find the minimal length of a subarray
of which the sum ≥ s. If there isn't one, return 0 instead.
*/
public class MinimumSizeSubarraySum {
    public int solve(int[] arr, int sum) {
        if (arr == null || arr.length == 0) return 0;

        int length = arr.length, left = 0, right = 0;
        int subarraySum = 0, result = Integer.MAX_VALUE;
        while (right < length) {
            if (subarraySum < sum) {
                subarraySum = subarraySum + arr[right++];
            }

            if (subarraySum >= sum) {
                result = Math.min(result, right - left);
                subarraySum = subarraySum - arr[left++];
            }
        }

        return result == Integer.MAX_VALUE ? 0: result;
    }
}
