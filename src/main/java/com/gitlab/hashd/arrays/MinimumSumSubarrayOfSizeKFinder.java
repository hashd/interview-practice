package com.gitlab.hashd.arrays;

public class MinimumSumSubarrayOfSizeKFinder {
    /*
    * Given an array of integers, find minimum sum sub-array of given size k.
    * */
    public int findMinimumSumSubarray(int[] arr, int k) {
        int minimum = Integer.MAX_VALUE, sum = 0;
        for (int i = 0; i < arr.length; i++) {
            // if index >= k, then check if sliding window sum is lesser than current minimum
            // and set it accordingly
            if (i >= k) {
                minimum = Math.min(minimum, sum);
                sum -= arr[i - k]; // sliding window adjustment
            }
            sum += arr[i];
        }
        return minimum;
    }
}
