package com.gitlab.hashd.arrays;

public class MissingArithmeticProgressionTermFinder {
    public int findMissingTerm(int[] arr) {
        if (arr == null || arr.length <= 2) return -1;

        int diff = Math.min(arr[2] - arr[1], arr[1] - arr[0]);
        int left = 0, right = arr.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            int expected = arr[0] + mid * diff;
            int prev = arr[0] + (mid - 1) * diff;
            if (arr[mid] == expected) {
                left = left + 1;
            } else if (arr[mid - 1] == prev && arr[mid] > expected) {
                return expected;
            } else {
                right = mid;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int input[] = {1,4,10,13,16,19,22};
        MissingArithmeticProgressionTermFinder maptf = new MissingArithmeticProgressionTermFinder();

        System.out.println(maptf.findMissingTerm(input));
    }
}
