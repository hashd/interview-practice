package com.gitlab.hashd.arrays;

import java.util.Arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

public class NextPermutationFinder {
    public int[] nextPermutation(int[] arr) {
        if (arr == null) return null;

        int[] permutation = Arrays.copyOf(arr, arr.length);
        for (int i = permutation.length - 2; i >= 0; i--) {
            if (permutation[i] < permutation[permutation.length - 1]) {
                swap(permutation, i, permutation.length - 1);
                int[] rangeToSort = Arrays.copyOfRange(permutation, i + 1, permutation.length);
                Arrays.sort(rangeToSort);
                for (int j = i + 1; j < permutation.length; j++) {
                    permutation[j] = rangeToSort[j - i - 1];
                }
                break;
            }
        }
        return permutation;
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {1, 2, 3, 6, 5, 4};
        NextPermutationFinder npf = new NextPermutationFinder();
        System.out.println(Arrays.toString(npf.nextPermutation(arr1)));
        System.out.println(Arrays.toString(npf.nextPermutation(arr2)));
    }
}
