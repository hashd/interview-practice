package com.gitlab.hashd.arrays;

import java.util.Arrays;

public class OddOccurringElementFinder {
    /*
    * Given an array of integers, duplicates are present in it in such a way that all duplicates appear even number
    * of times except one which appears odd number of times. Find that odd appearing element in linear time and
    * without using any extra memory.
    * */
    public int findOddOccurringElement(int[] arr) {
        /*
        long val = 0;
        for (int num: arr) {
            val ^= num;
        }
        return val;
        */
        return Arrays.stream(arr).reduce(0, (int acc, int val) -> acc ^ val);
    }
}
