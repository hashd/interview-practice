package com.gitlab.hashd.arrays;

import javafx.util.Pair;

import java.util.Arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

public class OddOccurringElementsFinder {
    /*
    * Given an array of integers, duplicates appear in it even number of times except two elements
    * which appears odd number of times. Find both odd appearing element without using any extra memory.
    * */
    public static Pair<Integer, Integer> findOddOccurringElements(int[] arr) {
        int axorb = Arrays.stream(arr).reduce(0, (int acc, int val) -> acc ^ val);

        int pos = 0;
        for (int i = 0; i < 32; i++) {
            if ((axorb & (1 << i)) != 0) {
                pos = i;
                break;
            }
        }

        int left = 0, right = arr.length - 1;
        while (left < right) {
            if ((arr[left] & (1 << pos)) != 0) {
                left++;
            } else {
                swap(arr, left, right);
                right--;
            }
        }

        int a = 0, b = 0;
        for (int i = 0; i < right; i++) a ^= arr[i];
        for (int i = right; i < arr.length; i++) b ^= arr[i];

        return new Pair<>(a, b);
    }
}
