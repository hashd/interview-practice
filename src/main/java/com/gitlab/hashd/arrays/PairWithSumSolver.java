package com.gitlab.hashd.arrays;

import com.gitlab.hashd.utils.Tuple;

import java.util.HashMap;
import java.util.Map;

public class PairWithSumSolver {
    public Tuple solveSorted(int[] arr, int sum) {
        int left = 0, right = arr.length;
        while (left <= right) {
            if (arr[left] + arr[right] < sum) {
                left = left + 1;
            } else if (arr[left] + arr[right] > sum) {
                right = right + 1;
            } else {
                return new Tuple(arr[left], arr[right]);
            }
        }
        return null;
    }

    public Tuple solveUnsorted(int[] arr, int sum) {
        Map<Integer, Integer> occurrences = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (occurrences.containsKey(sum - arr[i])) {
                return new Tuple(arr[i], sum - arr[i]);
            }
            occurrences.put(arr[i], i);
        }
        return null;
    }
}
