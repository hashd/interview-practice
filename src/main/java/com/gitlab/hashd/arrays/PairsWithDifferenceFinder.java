package com.gitlab.hashd.arrays;

import javafx.util.Pair;

import java.util.*;

public class PairsWithDifferenceFinder {
    /*
    * Given an unsorted array of integers, print all pairs with given difference k in it.
    * */
    public List<Pair<Integer, Integer>> findPairsWithDifference(int[] arr, int k) {
        List<Pair<Integer, Integer>> pairs = new LinkedList<>();

        Set<Integer> numbers = new HashSet<>();
        for (int anArr : arr) {
            numbers.add(anArr);
        }

        for (Integer number: numbers) {
            if (numbers.contains(number + k)) {
                pairs.add(new Pair<>(number, number + k));
            }
        }

        return pairs;
    }

    /*
    * Given an unsorted array of integers, print all pairs with given difference k in it.
    * Constant space solution
    * */
    public List<Pair<Integer, Integer>> findPairsWithDifferenceAlt(int[] arr, int k) {
        List<Pair<Integer, Integer>> pairs = new LinkedList<>();

        int[] sorted = Arrays.copyOf(arr, arr.length);
        Arrays.sort(sorted);

        for (int i = 0; i < sorted.length; i++) {
            int compliment = Arrays.binarySearch(sorted, k + sorted[i]);
            if (compliment >= 0) {
                pairs.add(new Pair<>(sorted[i], sorted[compliment]));
            }

            while (i + 1 < sorted.length && sorted[i + 1] == sorted[i]) {
                i += 1;
            }
        }

        return pairs;
    }
}
