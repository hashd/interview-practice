package com.gitlab.hashd.arrays;

public class PairwiseSortedChecker {
    public boolean isPairwiseSorted(int[] arr) {
        if (arr == null) return false;

        int len = arr.length;
        for (int i = 0; i < len / 2; i++) {
            if (arr[2 * i] > arr[2 * i + 1]) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        PairwiseSortedChecker psc = new PairwiseSortedChecker();

        System.out.println(psc.isPairwiseSorted(new int[] {10, 15, 9, 9,  1,  5}));
        System.out.println(psc.isPairwiseSorted(new int[] {10, 15, 8, 9, 10,  5}));
        System.out.println(psc.isPairwiseSorted(new int[] {10, 15, 9, 8,  5, 10}));
    }
}
