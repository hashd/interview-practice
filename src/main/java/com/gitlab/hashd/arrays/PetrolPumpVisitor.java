package com.gitlab.hashd.arrays;

public class PetrolPumpVisitor {
    public int findCircularTourThatVisitsAllPetrolPumps(int[] distances, int[] petrol) {
        int length = distances.length;

        int[] deficits = new int[length];
        for (int i = 0; i < length; i++) {
            deficits[i] = petrol[i] - distances[i];
        }

        int totalDeficit = 0, nodes = 0, nodeToExclude = 0;
        for (int i = 0; i < 2 * length; i++) {
            totalDeficit += deficits[i % length];
            nodes++;

            if (totalDeficit < 0) {
                totalDeficit -= deficits[nodeToExclude % length];
                nodeToExclude++;
                nodes--;
            }

            if (nodeToExclude == length) {
                return -1;
            }

            if (nodes == length) {
                return nodeToExclude;
            }
        }

        return -1;
    }
}
