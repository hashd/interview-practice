package com.gitlab.hashd.arrays;

import java.util.*;

public class PositiveNegativePairsFinder {
    public List<Integer> solve(int[] arr) {
        if (arr == null) return null;

        Map<Integer, Boolean> numbers = new LinkedHashMap<>();
        for (int number: arr) {
            if (numbers.containsKey(-number)) {
                numbers.put(-number, true);
            } else if (!numbers.containsKey(number)) {
                numbers.put(number, false);
            }
        }

        List<Integer> pairs = new LinkedList<>();
        for (Map.Entry<Integer, Boolean> pair: numbers.entrySet()) {
            if (pair.getValue()) {
                int val = pair.getKey();
                pairs.add(val < 0? val: -val);
                pairs.add(val < 0? -val: val);
            }
        }

        return pairs;
    }

    public static void main(String[] args) {
        PositiveNegativePairsFinder pnpf = new PositiveNegativePairsFinder();
        pnpf.solve(new int[]{4, 8, 9, -4, 1, -1, -8, -9}).forEach(System.out::println);
    }
}
