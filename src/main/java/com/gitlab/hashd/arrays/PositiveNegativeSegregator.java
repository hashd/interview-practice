package com.gitlab.hashd.arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

public class PositiveNegativeSegregator {

    public void segregatePositiveNegativeNumbers(int[] arr) {
        int lp = 0, rp = arr.length - 1, curr = 0, tmp = 0;
        while (curr < rp) {
            if (arr[curr] > 0) {
                swap(arr, rp, curr);
                rp--;
            } else {
                swap(arr, lp, curr);
                lp++;
                curr++;
            }
        }
    }
}
