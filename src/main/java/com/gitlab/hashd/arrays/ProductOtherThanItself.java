package com.gitlab.hashd.arrays;

public class ProductOtherThanItself {
    public void replaceWithProductOtherThanIt(long[] arr) {
        long[] leftProduct = new long[arr.length];
        long[] rightProduct = new long[arr.length];

        long productTillNow = 1;
        for (int i = 0; i < arr.length; i++) {
            leftProduct[i] = productTillNow;
            productTillNow *= arr[i];
        }

        productTillNow = 1;
        for (int i = 0; i < arr.length; i++) {
            rightProduct[arr.length - 1 - i] = productTillNow;
            productTillNow *= arr[arr.length - 1 - i];
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = leftProduct[i] * rightProduct[i];
        }
    }
}
