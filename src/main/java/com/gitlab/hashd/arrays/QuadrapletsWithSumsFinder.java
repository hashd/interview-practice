package com.gitlab.hashd.arrays;

import javafx.util.Pair;

import java.util.*;

public class QuadrapletsWithSumsFinder {
    /*
    * Given an unsorted array of integers, check if it contains quadraplets having given sum.
    * */
    public boolean checkIfQuadrapletWithSumExists(int[] arr, int sum) {
        Set<Integer> pairSums = new HashSet<>();
        for (int a : arr) {
            for (int b : arr) {
                if (pairSums.contains(sum - a - b)) {
                    return true;
                } else {
                    pairSums.add(a + b);
                }
            }
        }

        return false;
    }

    public Map<Set<Integer>, Integer> findQuadrapletsWithSum(int[] arr, int sum) {
        Map<Set<Integer>, Integer> quadrapletsMap = new HashMap<>();
        Map<Integer, List<Pair<Integer, Integer>>> pairSumsMap = new HashMap<>();

        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                int pairSum = arr[i] + arr[j];
                if (!pairSumsMap.containsKey(pairSum)) {
                    pairSumsMap.put(pairSum, new LinkedList<>());
                }
                pairSumsMap.get(pairSum).add(new Pair<>(arr[i], arr[j]));
            }
        }

        /*for (Map.Entry<Integer, List<Pair<Integer, Integer>>> pairSumsEntry: pairSumsMap.entrySet()) {
            int currentPairSum = pairSumsEntry.getKey();
            List<Pair<Integer, Integer>>
        }*/

        return quadrapletsMap;
    }
}
