package com.gitlab.hashd.arrays;

import java.util.LinkedList;
import java.util.List;

public class RangesFinder {
    public List<String> getRangesSummary(int[] arr) {
        if (arr.length == 0) return new LinkedList<>();

        List<String> ranges = new LinkedList<>();
        int rangeMin = arr[0];
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i + 1] != arr[i] + 1) {
                ranges.add(rangeMin == arr[i] ? String.valueOf(rangeMin) : rangeMin + " -> " + arr[i]);
                rangeMin = arr[i + 1];
            }
        }
        ranges.add(rangeMin == arr[arr.length - 1] ? String.valueOf(rangeMin) : rangeMin + " -> " + arr[arr.length - 1]);
        return ranges;
    }
}
