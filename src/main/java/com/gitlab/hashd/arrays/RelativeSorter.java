package com.gitlab.hashd.arrays;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Given two array A1[] and A2[], sort A1 in such a way that the relative order among the elements will be same as those
 * in A2. For the elements not present in A2. Append them at last in sorted order.
 * It is also given that the number of elements in A2[] are smaller than or equal to number of elements in A1[] and
 * A2[] has all distinct elements.
 *
 * http://practice.geeksforgeeks.org/problems/relative-sorting/0
 */
public class RelativeSorter {
    public int[] sortBasedOnSecondaryArray(int[] arr, int[] order) {
        Map<Integer, Integer> orderMap = new HashMap<>();
        for (int i = 0; i < order.length; i++) {
            orderMap.put(order[i], i);
        }
        for (int i = 0; i < arr.length; i++) {
            if (!orderMap.containsKey(arr[i])) {
                orderMap.put(arr[i], order.length + arr[i]);
            }
        }

        return Arrays.stream(arr)
                .boxed()
                .sorted(Comparator.comparingInt(orderMap::get))
                .mapToInt(Integer::intValue)
                .toArray();
    }
}
