package com.gitlab.hashd.arrays;

import java.util.Arrays;
import java.util.List;

public class SmallestNonconstructibleValueFinder {
    public int solve(int[] numbers) {
        int maxConstructibleValue = 0;
        Arrays.sort(numbers);

        for (int number: numbers) {
            if (number > maxConstructibleValue + 1) {
                return maxConstructibleValue + 1;
            }
            maxConstructibleValue += number;
        }

        return maxConstructibleValue + 1;
    }

    public static void main(String[] args) {
        int[] numbers = {1, 2, 2, 4, 12, 15};
        SmallestNonconstructibleValueFinder snvf = new SmallestNonconstructibleValueFinder();
        System.out.println(snvf.solve(numbers));
    }
}
