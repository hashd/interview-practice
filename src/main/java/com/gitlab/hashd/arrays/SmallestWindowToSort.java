package com.gitlab.hashd.arrays;

import com.gitlab.hashd.utils.Tuple;
import javafx.util.Pair;

public class SmallestWindowToSort {
    /*
    * Given an array of integers, find the smallest window in array sorting which will make the
    * entire array sorted in increasing order.
    * */
    public Tuple findSmallestWindowToSortToSortArray(int[] arr) {
        int leftAnomaly = 0, rightAnomaly = arr.length;

        int maxSoFar = Integer.MIN_VALUE, minSoFar = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= maxSoFar) {
                maxSoFar = arr[i];
            } else {
                rightAnomaly = i;
            }
        }

        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] <= minSoFar) {
                minSoFar = arr[i];
            } else {
                leftAnomaly = i;
            }
        }

        return new Tuple(leftAnomaly, rightAnomaly);
    }
}
