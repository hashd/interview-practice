package com.gitlab.hashd.arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

public class SortAlmostSortedArray {
    /*
    * Given an array where all its elements are sorted except two elements which were swapped,
    * sort the array in linear time. Assume there are no duplicates in the array.
    * */
    public int[] sortArrayWithSingleSwap(int[] arr) {
        int maxSoFar = arr[0], minSoFar = arr[arr.length - 1];
        int lowAnamoly = -1, highAnamoly = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < maxSoFar) {
                lowAnamoly = i;
            } else {
                maxSoFar = arr[i];
            }
        }

        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] > minSoFar) {
                highAnamoly = i;
            } else {
                minSoFar = arr[i];
            }
        }

        swap(arr, lowAnamoly, highAnamoly);
        return arr;
    }
}
