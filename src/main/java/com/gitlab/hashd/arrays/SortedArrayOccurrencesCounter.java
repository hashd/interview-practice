package com.gitlab.hashd.arrays;

import com.gitlab.hashd.arrays.medium.BinarySearchLowerBound;
import com.gitlab.hashd.arrays.medium.BinarySearchUpperBound;

public class SortedArrayOccurrencesCounter {
    public int findNoOfOccurrences(int[] arr, int key) {
        int low = new BinarySearchLowerBound().lowerBound(arr, key);
        int high = new BinarySearchUpperBound().upperBound(arr, key);

        return low == -1 ? 0: high - low + 1;
    }
}
