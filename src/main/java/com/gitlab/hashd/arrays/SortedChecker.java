package com.gitlab.hashd.arrays;

public class SortedChecker {
    public boolean isSorted(int[] arr) {
        if (arr == null) return false;
        if (arr.length == 0) return true;

        boolean isSorted = true;
        for (int i = 1; i < arr.length; i++) {
            isSorted = isSorted & (arr[i] >= arr[i - 1]);
        }
        return isSorted;
    }
}
