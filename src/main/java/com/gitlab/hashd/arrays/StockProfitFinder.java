package com.gitlab.hashd.arrays;

/**
 * The cost of a stock on each day is given in an array,
 * find the max profit that you can make by buying and selling in those days.
 *
 * http://practice.geeksforgeeks.org/problems/stock-buy-and-sell/0
 */
public class StockProfitFinder {
    public int profit(int[] arr) {
        int minSoFar = arr[0], maxDifference = 0;
        for (int n: arr) {
            maxDifference = Math.max(maxDifference, n - minSoFar);
            minSoFar = Math.min(minSoFar, n);
        }
        return maxDifference;
    }
}
