package com.gitlab.hashd.arrays;

public class StockProfitOneShare {
    /*
    * Given a list containing future prediction of share prices, find maximum profit that can be
    * earned by buying and selling shares any number of times with constraint that a new transaction
    * can only start after previous transaction is complete. i.e. we can only hold at-most one share at a time.
    * */
    public int findMaxProfitWithMultipleSales(int[] arr) {
        int profit = 0, low = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                profit += (arr[i - 1] - low);
                low = arr[i];
            }
        }

        if (arr[arr.length - 1] > arr[arr.length - 2]) {
            profit += (arr[arr.length - 1] - low);
        }

        return profit;
    }
}
