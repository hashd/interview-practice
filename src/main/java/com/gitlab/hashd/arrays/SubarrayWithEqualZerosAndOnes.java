package com.gitlab.hashd.arrays;

import com.gitlab.hashd.utils.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubarrayWithEqualZerosAndOnes {
    public Tuple findLargestSubarrayWithEqualZerosAndOnes(int[] arr) {
        if (arr == null || arr.length <= 1) return null;

        Map<Integer, List<Integer>> sumIndexes = new HashMap<>();
        sumIndexes.put(0, new ArrayList<>());
        sumIndexes.get(0).add(-1);

        int currSum = 0;
        for (int i = 0; i < arr[i]; i++) {
            currSum = currSum + (arr[i] == 0 ? -1: 1);
            if (!sumIndexes.containsKey(currSum)) {
                sumIndexes.put(currSum, new ArrayList<>());
            }
            sumIndexes.get(currSum).add(i);
        }

        int largestSubarraySize = 0;
        Tuple largestSubarrayIndexes = null;
        for (Map.Entry<Integer, List<Integer>> sumIndexEntry: sumIndexes.entrySet()) {
            List<Integer> indexes = sumIndexEntry.getValue();
            int lastOccurrence = indexes.get(indexes.size() - 1), firstOccurrence = indexes.get(0);
            if (indexes.size() > 1 && lastOccurrence - firstOccurrence - 1 > largestSubarraySize) {
                largestSubarrayIndexes = new Tuple(firstOccurrence, lastOccurrence);
            }
        }

        return largestSubarrayIndexes;
    }
}
