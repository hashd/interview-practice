package com.gitlab.hashd.arrays;

import com.gitlab.hashd.utils.Tuple;

/**
 * Given an unsorted array of non-negative integers,
 * find a continuous sub-array which adds to a given number.
 *
 * http://practice.geeksforgeeks.org/problems/subarray-with-given-sum/0
 */
public class SubarrayWithSumSolver {
    public Tuple solve(int[] arr, int sum) {
        int sumTillNow = 0;
        int[] sumArray = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            sumTillNow = sumTillNow + arr[i];
            sumArray[i] = sumTillNow;
        }

        return new PairWithSumSolver().solveUnsorted(sumArray, -sum);
    }
}
