package com.gitlab.hashd.arrays;

import java.util.HashSet;
import java.util.Set;

public class SubsetChecker {
    public boolean isSubset(int[] arr, int[] sub) {
        if (arr == null || sub == null) return false;

        Set<Integer> numbers = new HashSet<>();
        for (int num: sub) {
            numbers.add(num);
        }

        for (int num: arr) {
            numbers.remove(num);
        }

        return numbers.isEmpty();
    }
}
