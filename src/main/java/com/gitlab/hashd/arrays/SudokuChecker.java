package com.gitlab.hashd.arrays;

public class SudokuChecker {
    public boolean isValidSolution(int[][] grid) {
        if (grid == null) return false;

        int rows = grid.length, cols = grid[0].length;
        if (rows != 9 || cols != 9) {
            return false;
        }

        // Check if grid contains valid numbers
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (grid[i][j] < 1 || grid[i][j] > 9) {
                    return false;
                }
            }
        }

        // Check if each row and col contains all digits from 1 to 9
        int numRow, numCol;
        for (int i = 0; i < 9; i++) {
            numRow = 1; numCol = 1;
            for (int j = 0; j < 9; j++) {
                numRow = numRow | (1 << grid[i][j]);
                numCol = numCol | (1 << grid[j][i]);
            }
            if (numRow != 0x03FF || numCol != 0x03FF) {
                return false;
            }
        }

        // Check if each box of 3x3 contains all digits from 1 to 9
        int numBox;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                numBox = 1;
                for (int a = 0; a < 3; a++) {
                    for (int b = 0; b < 3; b++) {
                        numBox = numBox | (1 << grid[3 * i + a][3 * j + b]);
                    }
                }

                if (numBox != 0x03FF) {
                    return false;
                }
            }
        }

        return true;
    }
}
