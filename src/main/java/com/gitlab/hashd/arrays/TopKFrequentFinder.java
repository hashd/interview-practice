package com.gitlab.hashd.arrays;

import com.gitlab.hashd.heaps.GenericHeap;
import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.utils.Tuple;

import java.util.*;

public class TopKFrequentFinder {
    public List<Integer> findTopKFrequentElements(int[] arr, int k) {
        if (arr == null || arr.length == 0) return Collections.EMPTY_LIST;

        Map<Integer, Integer> frequencyMap = new HashMap<>();
        for (int num: arr) {
            if (!frequencyMap.containsKey(num)) {
                frequencyMap.put(num, 0);
            }
            frequencyMap.put(num, frequencyMap.get(num) + 1);
        }

        PriorityQueue<Tuple> pq = new PriorityQueue<>(Comparator.comparingInt(Tuple::getB));
        for (Map.Entry<Integer, Integer> entry: frequencyMap.entrySet()) {
            if (pq.size() >= k) {
                Tuple top = pq.peek();
                if (top.getB() < entry.getValue()) {
                    pq.poll();
                    pq.add(new Tuple(entry.getKey(), entry.getValue()));
                }
            } else {
                pq.add(new Tuple(entry.getKey(), entry.getValue()));
            }
        }

        List<Integer> frequentNumbers = new LinkedList<>();
        for (Tuple tuple: pq) {
            frequentNumbers.add(tuple.getA());
        }
        return frequentNumbers;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 2, 3};
        TopKFrequentFinder ff = new TopKFrequentFinder();
        ff.findTopKFrequentElements(arr, 2).forEach(System.out::println);
    }
}
