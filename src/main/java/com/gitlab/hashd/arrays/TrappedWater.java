package com.gitlab.hashd.arrays;

public class TrappedWater {
    /*
    * In trapping rain water problem, we need to find the maximum amount of water that can be trapped within
    * given set of bars where width of each bar is 1 unit.
    *
    * http://practice.geeksforgeeks.org/problems/trapping-rain-water/0
    * */
    public int findMaximumAmountOfTrappedWater(int[] arr) {
        int amount = 0, maxSoFar = 0;
        int[] leftMax = new int[arr.length], rightMax = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            leftMax[i] = maxSoFar;
            maxSoFar = Math.max(arr[i], maxSoFar);
        }

        maxSoFar = 0;
        for (int i = arr.length - 1; i >= 0; i--) {
            rightMax[i] = maxSoFar;
            maxSoFar = Math.max(arr[i], maxSoFar);
        }

        int minOfMaxes;
        for (int i = 0; i < arr.length; i++) {
            minOfMaxes = Math.min(leftMax[i], rightMax[i]);
            if (minOfMaxes > arr[i]) {
                amount += (minOfMaxes - arr[i]);
            }
        }

        return amount;
    }

    public int findMaximumAmountOfTrappedWaterAlt(int[] arr) {
        int left = 0, right = arr.length - 1, amount = 0;
        int maxLeft = arr[left], maxRight = arr[right];

        while (left < right) {
            if (arr[left] <= arr[right]) {
                left++;
                maxLeft = Math.max(maxLeft, arr[left]);
                amount += (maxLeft - arr[left]);
            } else {
                right--;
                maxRight = Math.max(maxRight, arr[right]);
                amount += (maxRight - arr[right]);
            }
        }

        return amount;
    }
}
