package com.gitlab.hashd.arrays;

/**
 * Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai).
 * n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
 *
 * Find two lines, which together with x-axis forms a container,
 * such that the container contains the most water.
 *
 * Note: You may not slant the container and n is at least 2.
 */
public class TrappedWaterMaxContainer {
    public int getMaxContainer(int[] heights) {
        if (heights == null || heights.length == 0) return 0;

        int length = heights.length, left = 0, right = length - 1, maxArea = 0;

        // Using two pointer approach
        while (left < right) {
            maxArea = Math.max(maxArea, (right - left) * Math.min(heights[left], heights[right]));
            if (heights[left] <= heights[right]) {
                left++;
            } else {
                right--;
            }
        }

        return maxArea;
    }

    public static void main(String[] args) {

    }
}
