package com.gitlab.hashd.arrays;

/*
Given a triangle, find the minimum path sum from top to bottom.
Each step you may move to adjacent numbers on the row below.

https://www.programcreek.com/2013/01/leetcode-triangle-java/
 */
public class TriangleMinimumPathSumFinder {
    public int sum(int[][] triangle) {
        if (triangle == null) return 0;

        int rows = triangle.length;
        if (rows == 0) return 0;

        // Initialize triangle to hold minimum sums to that point
        int[][] minSums = new int[rows][];
        for (int i = 0; i < rows; i++) {
            minSums[i] = new int[i + 1];
        }
        minSums[0][0] = triangle[0][0];

        // Calculate the minimum sums based on previous row data
        for (int i = 1; i < rows; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0) {
                    minSums[i][j] = minSums[i - 1][j] + triangle[i][j];
                } else if (j == i) {
                    minSums[i][j] = minSums[i - 1][j - 1] + triangle[i][j];
                } else {
                    minSums[i][j] = triangle[i][j] + Math.min(minSums[i - 1][j - 1], minSums[i - 1][j]);
                }
            }
        }

        // Find the least value present in the bottom row
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < rows; i++) {
            min = Math.min(min, minSums[rows - 1][i]);
        }

        return min;
    }
}
