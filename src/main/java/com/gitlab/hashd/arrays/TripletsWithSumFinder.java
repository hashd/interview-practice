package com.gitlab.hashd.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TripletsWithSumFinder {
    /*
    * Given an unsorted array of integers, find a triplet with given sum in it.
    * */
    public List<int[]> findTripletsWithSum(int[] arr, int sum) {
        List<int[]> solutions = new ArrayList<>();
        Arrays.sort(arr);

        for (int i = 0; i < arr.length - 2; i++) {
            int left = i + 1, right = arr.length - 1;
            while (left < right) {
                if (arr[left] + arr[right] + arr[i] > sum) {
                    right--;
                } else if (arr[left] + arr[right] + arr[i] < sum) {
                    left++;
                } else {
                    int[] numbers = new int[3];
                    numbers[0] = arr[i]; numbers[1] = arr[left]; numbers[2] = arr[right];
                    solutions.add(numbers);

                    left++;
                    right--;
                }
            }
        }

        return solutions;
    }
}
