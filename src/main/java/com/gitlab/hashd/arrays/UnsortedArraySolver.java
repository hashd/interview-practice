package com.gitlab.hashd.arrays;

/**
 * Given an unsorted array of size N. Find the first element in array such that all of its left elements
 * are smaller and all right elements to it are greater than it.
 * Note: Left and right side elements can be equal to required element. And extreme elements cannot be required element.
 *
 * http://practice.geeksforgeeks.org/problems/unsorted-array/0
 */
public class UnsortedArraySolver {
    public int getValue(int[] arr) {
        if (arr == null || arr.length == 0) return -1;

        int[] maxToLeft = new int[arr.length];
        int[] minToRight = new int[arr.length];

        maxToLeft[0] = Integer.MIN_VALUE;
        minToRight[arr.length - 1] = Integer.MAX_VALUE;

        for (int i = 1; i < arr.length; i++) {
            int rightIndex = arr.length - 1 - i;
            maxToLeft[i] = Math.max(arr[i - 1], maxToLeft[i - 1]);
            minToRight[rightIndex] = Math.min(minToRight[rightIndex + 1], arr[rightIndex + 1]);
        }

        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] > maxToLeft[i] && arr[i] < minToRight[i]) {
                return arr[i];
            }
        }

        return -1;
    }
}
