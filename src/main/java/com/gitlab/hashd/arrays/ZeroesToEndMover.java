package com.gitlab.hashd.arrays;

public class ZeroesToEndMover {
    public int[] moveZeroesToEnd(int[] arr) {
        int pos = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                arr[pos++] = arr[i];
            }
        }
        for (int i = pos; i < arr.length; i++) {
            arr[i] = 0;
        }
        return arr;
    }
}
