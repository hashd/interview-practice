package com.gitlab.hashd.arrays.easy;

import java.util.Random;

import static com.gitlab.hashd.ArrayProblems.swap;

public class ArrayShuffler {
    public void shuffle(int[] arr) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            int idx = rand.nextInt(arr.length - i);
            swap(arr, i + idx, i);
        }
    }
}
