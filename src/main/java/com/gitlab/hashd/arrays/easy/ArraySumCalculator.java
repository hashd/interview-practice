package com.gitlab.hashd.arrays.easy;

public class ArraySumCalculator {
    public long fsum(int[] arr) {
        if (arr == null) return 0;
        long length = arr.length, sum = 0;
        for (int i = 0; i < length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    public long wsum(int[] arr) {
        if (arr == null) return 0;
        long length = arr.length, sum = 0;
        int i = 0;
        while (i < length) {
            sum = sum + arr[i++];
        }
        return sum;
    }

    private long rsum(int[] arr, int pos) {
        if (pos == arr.length) {
            return 0;
        }
        return arr[pos] + rsum(arr, pos + 1);
    }

    public long rsum(int[] arr) {
        if (arr == null) return 0;
        return rsum(arr, 0);
    }
}
