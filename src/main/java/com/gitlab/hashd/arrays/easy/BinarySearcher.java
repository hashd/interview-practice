package com.gitlab.hashd.arrays.easy;

public class BinarySearcher {
    public int binarySearch(int[] arr, int key) {
        int low = 0, high = arr.length, mid;

        while (low < high) {
            mid = low + (high - low) / 2;
            if (arr[mid] == key) {
                return mid;
            } else if (arr[mid] > key) {
                high = mid;
            } else if (arr[mid] < key) {
                low = mid + 1;
            }
        }

        return -1;
    }
}
