package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

/**
 * Given an array A[] of N integers where each value represents number of chocolates in a packet.
 * Each packet can have variable number of chocolates.
 * There are m students, the task is to distribute chocolate packets such that :
 * 1. Each student gets one packet.
 * 2. The difference between the number of chocolates given to the students in packet with
 *    maximum chocolates and packet with minimum chocolates is minimum.
 *
 * http://practice.geeksforgeeks.org/problems/chocolate-distribution-problem/0
 */
public class ChocolateDistributionSolver {
    public int solve(int[] arr, int m) {
        Arrays.sort(arr);

        int minimum = Integer.MAX_VALUE;
        for (int i = m - 1; i < arr.length; i++) {
            minimum = Math.min(minimum, arr[i] - arr[i - m + 1]);
        }

        return minimum;
    }

    public static void main(String[] args) {
        ChocolateDistributionSolver cds = new ChocolateDistributionSolver();
        System.out.println(cds.solve(new int[]{3, 4, 1, 9, 56, 7, 9, 12}, 5));
        System.out.println(cds.solve(new int[]{7, 3, 2, 4, 9, 12, 56}, 3));
    }
}
