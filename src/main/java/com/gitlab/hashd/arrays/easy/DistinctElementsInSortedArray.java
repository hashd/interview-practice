package com.gitlab.hashd.arrays.easy;

import java.util.LinkedList;
import java.util.List;

public class DistinctElementsInSortedArray {
    public int[] getUniqueElements(int[] arr) {
        if (arr == null) return null;

        List<Integer> distinctValues = new LinkedList<>();
        if (arr.length > 0) {
            distinctValues.add(arr[0]);
        }

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[i - 1]) {
                distinctValues.add(arr[i]);
            }
        }

        return distinctValues.stream().mapToInt(i -> i).toArray();
    }
}
