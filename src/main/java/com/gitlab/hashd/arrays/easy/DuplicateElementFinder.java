package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

public class DuplicateElementFinder {
    public int findDuplicateInRange(int[] arr, int low, int high) {
        int n = high - low + 1, arrSum = Arrays.stream(arr).sum();
        int sum = (high * (high + 1))/2 - n * (low - 1);
        return arrSum - sum;
    }
}
