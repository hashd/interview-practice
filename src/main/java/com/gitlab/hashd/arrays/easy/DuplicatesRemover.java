package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

public class DuplicatesRemover {
    public int[] removeDuplicates(int[] sortedArray) {
        if (sortedArray == null) return null;
        if (sortedArray.length == 0 || sortedArray.length == 1) return sortedArray;

        int length = sortedArray.length, pivot = 0;
        for (int i = 0; i < length; i++) {
            while (i < sortedArray.length - 1 && sortedArray[i + 1] == sortedArray[i]) {
                i++;
            }
            sortedArray[pivot++] = sortedArray[i];
        }

        return Arrays.copyOf(sortedArray, pivot);
    }

    public int[] removeDuplicates(int[] array, boolean isSorted) {
        if (!isSorted) {
            Arrays.sort(array);
        }

        return removeDuplicates(array);
    }
}
