package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

public class ElementsRemover {
    public int[] removeElements(int[] array, int value) {
        if (array == null) return null;
        if (array.length == 0) return array;

        int pivot = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != value) {
                array[pivot++] = array[i];
            }
        }

        return Arrays.copyOf(array, pivot);
    }
}
