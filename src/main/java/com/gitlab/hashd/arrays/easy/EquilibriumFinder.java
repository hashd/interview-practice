package com.gitlab.hashd.arrays.easy;

/**
 * Given an array A your task is to tell at which position the equilibrium first occurs in the array.
 * Equilibrium position in an array is a position such that the sum of elements below it
 * is equal to the sum of elements after it.
 *
 * http://practice.geeksforgeeks.org/problems/equilibrium-point/0
 */
public class EquilibriumFinder {
    public int findEquilibriumIndex(int[] arr) {
        int totalSum = 0;
        for (int n : arr) { totalSum += n; }

        int sumTillNow = 0;
        for (int i = 0; i < arr.length; i++) {
            if (sumTillNow == (totalSum - sumTillNow - arr[i])) {
                return i;
            }
            sumTillNow += arr[i];
        }

        return -1;
    }
}
