package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

public class EvenOddSegregator {
    public void segregate(int[] arr) {
        if (arr == null || arr.length == 0) return;

        int left = 0, pivot = 0;
        while (pivot < arr.length) {
            if (arr[pivot] % 2 == 0) {
                swap(arr, left++, pivot);
            }
            pivot++;
        }
    }

    public static void main(String[] args) {
        EvenOddSegregator eos = new EvenOddSegregator();
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        eos.segregate(arr);
        System.out.println(Arrays.toString(arr));
    }
}
