package com.gitlab.hashd.arrays.easy;

import java.util.Arrays;

public class GeometricMean {
    public double calculate(int[] arr) {
        long product = Arrays.stream(arr).reduce(1, (acc, num) -> acc * num);
        return Math.pow(product, 1D/arr.length);
    }
}
