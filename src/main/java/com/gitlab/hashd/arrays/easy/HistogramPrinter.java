package com.gitlab.hashd.arrays.easy;

public class HistogramPrinter {
    public void printHistogram(int[] numbers) {
        if (numbers == null || numbers.length == 0) return;

        int max = 0, length = numbers.length;
        for (int i = 0; i < length; i++) {
            max = numbers[i] > max ? numbers[i]: max;
        }

        for (int i = max; i >= 0; i--) {
            for (int j = 0; j < length; j++) {
                System.out.printf("%c", numbers[j] >= i ? '*':' ');
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        HistogramPrinter hp = new HistogramPrinter();
        int[] numbers = {5, 4, 0, 3, 4, 1};
        hp.printHistogram(numbers);
    }
}
