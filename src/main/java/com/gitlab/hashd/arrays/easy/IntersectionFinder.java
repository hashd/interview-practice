package com.gitlab.hashd.arrays.easy;

/**
 * Write an efficient function to find the intersection of two sorted arrays, each of size n
 * respectively, in O(n) time
 */
public class IntersectionFinder {
    public Integer findIntersection(int[] a, int[] b) {
        int aPivot = 0, bPivot = 0;
        while (aPivot < a.length && bPivot < b.length) {
            if (a[aPivot] == b[bPivot]) {
                return a[aPivot];
            } else if (a[aPivot] < b[bPivot]) {
                aPivot++;
            } else {
                bPivot++;
            }
        }

        return null;
    }
}
