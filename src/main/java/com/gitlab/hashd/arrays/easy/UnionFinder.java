package com.gitlab.hashd.arrays.easy;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Write an efficient function to find the common elements between two unsorted arrays,
 * each of size n respectively
 */
public class UnionFinder {
    public int[] getUnion(int[] arr1, int[] arr2) {
        if (arr1 == null || arr2 == null) return new int[0];

        Set<Integer> arr2Set = new HashSet<>();
        for (int i = 0; i < arr2.length; i++) {
            arr2Set.add(arr2[i]);
        }

        List<Integer> union = new LinkedList<>();
        for (int i = 0; i < arr1.length; i++) {
            if (arr2Set.contains(arr1[i])) {
                union.add(arr1[i]);
            }
        }

        int[] unionArr = new int[union.size()];
        int i = 0;
        for (Integer number: union) {
            unionArr[i++] = number;
        }

        return unionArr;
    }
}
