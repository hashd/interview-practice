package com.gitlab.hashd.arrays.hard;

import java.util.Arrays;

public class ArrayNumberAdder {
    public int[] add(int[] a, int[] b) {
        if (a == null || b == null) return null;

        int aLen = a.length, bLen = b.length, aPiv = a.length - 1, bPiv = b.length - 1, carry = 0;
        int[] number = new int[Math.max(aLen, bLen) + 1];
        int nPiv = number.length - 1;
        while (aPiv >= 0 && bPiv >= 0) {
            int sum = a[aPiv--] + b[bPiv--] + carry;
            number[nPiv--] = sum % 10;
            carry = sum / 10;
        }

        while (aPiv >= 0) {
            int sum = a[aPiv--] + carry;
            number[nPiv--] = sum % 10;
            carry = sum / 10;
        }

        while (bPiv >= 0) {
            int sum = b[bPiv--] + carry;
            number[nPiv--] = sum % 10;
            carry = sum / 10;
        }

        while (carry != 0) {
            number[nPiv--] = carry % 10;
            carry = carry / 10;
        }

        return number;
    }

    public static void main(String[] args) {
        ArrayNumberAdder ana = new ArrayNumberAdder();
        int[] a = {1,2,3,4,5,6,7,8,9};
        int[] b = {4,3,2,1};

        System.out.println(Arrays.toString(ana.add(a, b)));
    }
}
