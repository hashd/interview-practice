package com.gitlab.hashd.arrays.hard;

import java.util.*;

public class DuplicatesWithinKDistanceFinder {
    public Set<Integer> getDuplicates(int[] arr, int k) {
        if (arr == null || arr.length == 0) return Collections.emptySet();

        Set<Integer> duplicates = new HashSet<>();

        Map<Integer, Integer> numbers = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (i >= k) {
                numbers.put(arr[i - k], numbers.get(arr[i - k]) - 1);
            }

            if (numbers.containsKey(arr[i])) {
                duplicates.add(arr[i]);
            }
            numbers.put(arr[i], !numbers.containsKey(arr[i]) ? 1: numbers.get(arr[i]) + 1);
        }

        return duplicates;
    }
}
