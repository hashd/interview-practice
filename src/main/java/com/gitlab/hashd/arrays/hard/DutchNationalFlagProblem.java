package com.gitlab.hashd.arrays.hard;

import java.util.Arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

/**
 * Write a program to sort an array of 0's,1's and 2's in ascending order.
 *
 * http://practice.geeksforgeeks.org/problems/sort-an-array-of-0s-1s-and-2s/0
 */
public class DutchNationalFlagProblem {
    // Single pass approach using 2 pointers
    public void solve(int[] arr) {
        int start = 0, end = arr.length - 1, current = 0, tmp;
        while (current <= end) {
            if (arr[current] == 0) {
                swap(arr, start, current);
                start++;
                current++;
            } else if (arr[current] == 2) {
                swap(arr, current, end);
                end--;
            } else {
                current++;
            }
        }
    }

    // Double pass approach using 1 pointer
    public void solveAlt(int[] arr) {
        int start = 0, current = 0, len = arr.length;
        while (current < len) {
            if (arr[current] < 1) {
                swap(arr, start++, current++);
            } else {
                current++;
            }
        }

        current = start;
        while (current < len) {
            if (arr[current] == 1) {
                swap(arr, start++, current++);
            } else {
                current++;
            }
        }
    }

    public static void main(String[] args) {
        DutchNationalFlagProblem dnfp = new DutchNationalFlagProblem();
        int[] arr = {0, 1, 1, 2, 0, 2, 1, 2, 0};
        dnfp.solveAlt(arr);

        System.out.println(Arrays.toString(arr));
    }
}
