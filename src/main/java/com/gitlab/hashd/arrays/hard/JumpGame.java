package com.gitlab.hashd.arrays.hard;

import java.util.Arrays;

/**
 * JUMP GAME
 * Write a program which takes an array of n integers, where A[i] denotes the maximum you can advance from index i,
 * and returns whether it is possible to advance to the last index starting from the beginning of the array.
 *
 * Variant:
 * Find minimum number of jumps needed to advance to last index starting from the beginning of the array
 */
public class JumpGame {
    public boolean canJump(int[] arr) {
        int jump = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i > jump) { // Can't reach here so we're done
                break;
            }
            jump = Math.max(jump, i + arr[i]);
        }

        return jump >= arr.length - 1;
    }

    public int minJumpsNeededToReachLastPosition(int[] arr) {
        int[] jumps = new int[arr.length];
        int length = arr.length, maxJump = 0;
        Arrays.fill(jumps, Integer.MAX_VALUE);

        jumps[0] = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i > maxJump) {
                return -1;
            }
            maxJump = Math.max(maxJump, i + arr[i]);

            for (int j = 1; j <= arr[i] && i + j < length; j++) {
               jumps[i + j] = Math.min(jumps[i + j], jumps[i] + 1);
            }
        }
        System.out.println(Arrays.toString(jumps));

        return jumps[length - 1];
    }

    public static void main(String[] args) {
        int[] arr = {3, 3, 1, 0, 2, 0, 1};
        JumpGame jg = new JumpGame();

        System.out.println(jg.canJump(arr));
        System.out.println(jg.minJumpsNeededToReachLastPosition(arr));
    }
}
