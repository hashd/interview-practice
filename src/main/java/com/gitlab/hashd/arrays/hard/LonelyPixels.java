package com.gitlab.hashd.arrays.hard;

/**
 * Lonely Pixel
 * Given an N x M image with black pixels and white pixels, if a pixel is the only one in its color
 * throughout its entire row and column, then it is a lonely pixel. Find the number of lonely pixels
 * in black from the image.
 */
public class LonelyPixels {
    public int getNumber(int[][] grid) {
        if (grid == null) return 0;

        int rows = grid.length, cols = grid[0].length, count = 0;
        int[] rowCounts = new int[rows];
        int[] colCounts = new int[cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                rowCounts[i] += grid[i][j];
                colCounts[j] += grid[i][j];
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1 && rowCounts[i] == 1 && colCounts[j] == 1) {
                    count = count + 1;
                }
            }
        }

        return count;
    }
}
