package com.gitlab.hashd.arrays.medium;

import java.util.Arrays;

public class BinaryArrayZeroSwapper {
    public int findIndex(int arr[]) {
        int[] leftOnes = new int[arr.length], rightOnes = new int[arr.length];
        Arrays.fill(leftOnes, 0);
        Arrays.fill(rightOnes, 0);

        leftOnes[0] = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i - 1] == 0) {
                leftOnes[i] = 0;
            } else {
                leftOnes[i] = leftOnes[i - 1] + 1;
            }
        }

        rightOnes[arr.length - 1] = 0;
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i + 1] == 0) {
                rightOnes[i] = 0;
            } else {
                rightOnes[i] = rightOnes[i + 1] + 1;
            }
        }

        int maxTillNow = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0 && leftOnes[i] != 0 && rightOnes[i] != 0) {
                maxTillNow = Math.max(maxTillNow, leftOnes[i] + rightOnes[i] + 1);
            }
        }

        return maxTillNow;
    }
}
