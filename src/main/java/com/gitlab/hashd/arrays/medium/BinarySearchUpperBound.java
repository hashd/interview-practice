package com.gitlab.hashd.arrays.medium;

public class BinarySearchUpperBound {
    public int upperBound(int[] arr, int key) {
        int low = 0, high = arr.length, mid;

        while (low < high) {
            mid = low + (high - low) / 2;
            if (arr[mid] == key && (mid == arr.length - 1 || arr[mid] != arr[mid + 1])) {
                return mid;
            } else if (arr[mid] > key) {
                high = mid;
            } else if (arr[mid] <= key) {
                low = mid + 1;
            }
        }

        return -1;
    }
}
