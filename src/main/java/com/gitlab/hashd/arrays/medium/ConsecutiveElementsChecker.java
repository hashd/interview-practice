package com.gitlab.hashd.arrays.medium;

import java.util.HashSet;
import java.util.Set;

public class ConsecutiveElementsChecker {
    public boolean hasConsecutiveElements(int[] arr) {
        if (arr == null || arr.length == 0) return false;

        Set<Integer> numbers = new HashSet<>();
        for (int num: arr) {
            numbers.add(num);
        }

        int pivot = arr[0];
        while (numbers.contains(pivot)) {
            numbers.remove(pivot++);
        }
        pivot = arr[0] - 1;
        while (numbers.contains(pivot)) {
            numbers.remove(pivot--);
        }

        return numbers.isEmpty();
    }
}
