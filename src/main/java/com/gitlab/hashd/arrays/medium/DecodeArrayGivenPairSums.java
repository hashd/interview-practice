package com.gitlab.hashd.arrays.medium;

public class DecodeArrayGivenPairSums {
    /*
    * Given an array constructed from another array by taking sum of every distinct pair in it,
    * decode the array to get back the original array elements.
    * */
    public int[] decodeOriginalArray(int[] arr) {
        int[] original = new int[(1 + (int) Math.sqrt(1 + 8 * arr.length)) / 2];
        original[0] = (arr[0] + arr[1] - arr[original.length - 1]) / 2;
        for (int i = 1; i < original.length; i++) {
            original[i] = arr[i - 1] - original[0];
        }

        return original;
    }
}
