package com.gitlab.hashd.arrays.medium;

import com.gitlab.hashd.utils.Tuple;

public class FloorAndCeilSortedArrayFinder {
    public Tuple getFloorAndCeil(int[] arr, int key) {
        if (arr == null) return null;

        if (arr[0] > key) {
            return new Tuple(-1, arr[0]);
        }

        int lastElement = arr[arr.length - 1];
        if (lastElement <= key) {
            return new Tuple(lastElement, -1);
        }

        int left = 0, right = arr.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (key >= arr[mid] && key < arr[mid + 1]) {
                return new Tuple(arr[mid], arr[mid + 1]);
            } else if (key <= arr[mid]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        return new Tuple(-1, -1);
    }

    public static void main(String[] args) {
        int[] arr = {1,2,5,6,11,15};
        FloorAndCeilSortedArrayFinder facsaf = new FloorAndCeilSortedArrayFinder();
        System.out.println(facsaf.getFloorAndCeil(arr, 15));
        System.out.println(facsaf.getFloorAndCeil(arr, 1));
    }
}
