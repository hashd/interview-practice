package com.gitlab.hashd.arrays.medium;

import com.gitlab.hashd.utils.Tuple;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Given an array of integers, sort the array according to frequency of elements.
 * If frequencies of two elements are same, print them in increasing order.
 *
 * http://practice.geeksforgeeks.org/problems/sorting-elements-of-an-array-by-frequency/0
 */
public class FrequencyBasedSorter {
    public int[] sortElementsByFrequency(int[] arr) {
        if (arr == null) return null;

        Map<Integer, Integer> elementsMap = new HashMap<>();
        for (int element: arr) {
            if (!elementsMap.containsKey(element)) {
                elementsMap.put(element, 0);
            }
            elementsMap.put(element, elementsMap.get(element) + 1);
        }

        List<Tuple> elementCounts = elementsMap.entrySet().stream().map(e -> new Tuple(e.getKey(), e.getValue())).collect(Collectors.toList());
        elementCounts.sort((t1, t2) -> (t1.getB() == t2.getB())? t1.getA() - t2.getA(): t2.getB() - t1.getB());
        return elementCounts.stream().flatMap(p -> Collections.nCopies(p.getB(), p.getA()).stream()).mapToInt(i -> i).toArray();
    }
}
