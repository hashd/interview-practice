package com.gitlab.hashd.arrays.medium;

public class InsertPositionSearcher {
    public int findPosition(int[] arr, int key) {
        if (arr == null) return -1;

        int left = 0, right = arr.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (arr[mid] == key) {
                return mid;
            } else if (arr[mid] > key && (mid == 0 || arr[mid - 1] < key)) {
                return mid;
            } else if (arr[mid] > key) {
                right = mid;
            } else if (arr[mid] < key && mid == arr.length - 1) {
                return arr.length;
            } else {
                left = mid + 1;
            }
        }

        return -1;
    }
}
