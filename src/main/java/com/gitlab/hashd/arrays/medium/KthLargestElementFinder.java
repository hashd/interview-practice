package com.gitlab.hashd.arrays.medium;

import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.heaps.MinHeap;

public class KthLargestElementFinder {
    public int findKthLargestElement(int[] arr, int k) {
        Heap minHeap = new MinHeap();
        for (int i = 0; i < arr.length; i++) {
            minHeap.push(arr[i]);
            if (minHeap.size() > k) {
                minHeap.pop();
            }
        }

        return minHeap.pop();
    }
}
