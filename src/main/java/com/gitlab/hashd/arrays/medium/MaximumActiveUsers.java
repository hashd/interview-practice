package com.gitlab.hashd.arrays.medium;

import com.gitlab.hashd.utils.Tuple;

import java.util.Arrays;
import java.util.Collections;

/*
Here are tuples given for each users of a website (Si,Ei) where
 Si denotes the when the user entered the website and
 Ei denotes when the user exits the website .

Find the maximum number of users active of website at any time duration.
 */
public class MaximumActiveUsers {
    public int solve(Tuple[] sessions) {
        if (sessions == null || sessions.length == 0) return 0;

        int[] arrivals = Arrays.stream(sessions).mapToInt(Tuple::getA).toArray();
        int[] departures = Arrays.stream(sessions).mapToInt(Tuple::getB).toArray();
        int arrived = 0, departed = 0, curr = 0, maxUsers = 0;

        while (arrived < sessions.length) {
            if (arrivals[arrived] < departures[departed]) {
                curr = curr + 1;
                maxUsers = Math.max(curr, maxUsers);
                arrived = arrived + 1;
            } else {
                curr = curr - 1;
                departed = departed - 1;
            }
        }

        return maxUsers;
    }
}
