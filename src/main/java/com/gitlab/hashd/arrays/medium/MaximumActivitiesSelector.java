package com.gitlab.hashd.arrays.medium;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MaximumActivitiesSelector {
    public List<Pair<Integer, Integer>> selectMaximumActivities(List<Pair<Integer, Integer>> activities) {
        activities.sort((o1, o2) -> o1.getValue() - o2.getValue());
        Stack<Pair<Integer, Integer>> intervalStack = new Stack<>();
        for (Pair<Integer, Integer> activity: activities) {
            if (intervalStack.size() == 0) {
                intervalStack.push(activity);
            } else {
                Pair<Integer, Integer> top = intervalStack.peek();
                if (top.getValue() <= activity.getKey()) {
                    intervalStack.push(activity);
                }
            }
        }

        return new ArrayList<>(intervalStack);
    }
}
