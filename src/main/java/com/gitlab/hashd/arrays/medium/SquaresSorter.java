package com.gitlab.hashd.arrays.medium;

import java.util.Arrays;

/**
 * Given a sorted list of integers, square the elements and give the output in sorted order.
 *
 * https://www.careercup.com/question?id=5754478975254528
 */
public class SquaresSorter {
    public int[] sort(int[] numbers) {
        if (numbers == null) return null;
        if (numbers.length == 0) return new int[]{};

        // Find where the positive integers begin
        int positiveStart = numbers.length;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] >= 0) {
                positiveStart = i; break;
            }
        }
        int negativeEnd = positiveStart - 1, nPivot = negativeEnd, pPivot = positiveStart;

        // Merge the negative and positive integers based on absolute value
        int idx = 0;
        int[] arr = new int[numbers.length];
        while (nPivot >= 0 && pPivot < numbers.length) {
            if (Math.abs(numbers[nPivot]) < numbers[pPivot]) {
                arr[idx++] = numbers[nPivot--];
            } else {
                arr[idx++] = numbers[pPivot++];
            }
        }

        while (nPivot >= 0) arr[idx++] = numbers[nPivot--];
        while (pPivot < numbers.length) arr[idx++] = numbers[pPivot++];

        // Square individual numbers before returning the final array
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * arr[i];
        }

        return arr;
    }

    public static void main(String[] args) {
        SquaresSorter ss = new SquaresSorter();
        System.out.println(Arrays.toString(ss.sort(new int[]{-3, -1, 0, 2, 4})));
        System.out.println(Arrays.toString(ss.sort(new int[]{-3, -1, 0, 2, 4})));
        System.out.println(Arrays.toString(ss.sort(new int[]{ 0,  1, 2, 3, 4})));
        System.out.println(Arrays.toString(ss.sort(new int[]{-4, -3, -2, -1, 0})));
    }
}
