package com.gitlab.hashd.arrays.medium;

import java.util.Arrays;

import static com.gitlab.hashd.ArrayProblems.swap;

/**
 * Given an array of distinct elements, rearrange the elements of array in zig-zag fashion in O(n) time.
 * The converted array should be in form a < b > c < d > e < f.
 * The relative order of elements is same in the output i.e you have to iterate on the original array only.
 */
public class ZigZagRearranger {
    public int[] rearrange(int[] arr) {
        if (arr == null || arr.length == 0) return null;

        for (int i = 0; i < arr.length - 1; i++) {
            if (i % 2 == 0 && (arr[i + 1] < arr[i])) {
                swap(arr, i + 1, i);
            } else if (i % 2 == 1 && (arr[i + 1] > arr[i])) {
                swap(arr, i + 1, i);
            }
        }

        return arr;
    }

    public static void main(String[] args) {
        ZigZagRearranger ar = new ZigZagRearranger();

        System.out.println(Arrays.toString(ar.rearrange(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9})));
        System.out.println(Arrays.toString(ar.rearrange(new int[]{4, 3, 7, 8, 6, 2, 1})));
        System.out.println(Arrays.toString(ar.rearrange(new int[]{1, 4, 3, 2})));
   }
}
