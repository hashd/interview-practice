package com.gitlab.hashd.bit;

public class BitUtils {
    public static int swapAdjacentBits(int num) {
        return ((num & 0xAAAAAAAA) >> 1) | ((num & 0x55555555) << 1);
    }

    public static boolean isEven(int num) {
        return (num & 1) == 0;
    }

    public static boolean isOdd(int num) {
        return (num & 1) == 1;
    }

    public static boolean areOfOppositeSigns(int a, int b) {
        return (a ^ b) < 0;
    }

    public static int addOne(int num) {
        return -(~num);
    }

    public static int setBit(int num, int k) {
        return (num | (1 << (k - 1)));
    }

    public static int unsetBit(int num, int k) {
        return (num & ~(1 << (k - 1)));
    }

    public static boolean isSet(int num, int k) {
        return (num & (1 << (k - 1))) != 0;
    }

    public static int toggleBit(int num, int k) {
        return num ^ (1 << (k - 1));
    }

    public static boolean isPowerOf2(int num) {
        return (num & (num - 1)) == 0;
    }

    public static boolean getParity(int num) {
        int parity = 0;
        while (num != 0) {
            num = num & (num - 1);
            parity++;
        }
        return parity % 2 != 0;
    }

    public static char toLowercase(char c) {
        return (char) (c | ' ');
    }

    public static char toUppercase(char c) {
        return (char) (c & '_');
    }

    public static char invertCase(char c) {
        return (char) (c ^ ' ');
    }
}
