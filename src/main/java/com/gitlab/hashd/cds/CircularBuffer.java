package com.gitlab.hashd.cds;

public class CircularBuffer {
    private int[] buffer;
    private int count;
    private int size;

    public CircularBuffer(int size) {
        this.size = size;
        this.buffer = new int[size];
    }

    public void add(int element) {
        this.buffer[count % size] = element;

        count = count + 1;
    }

    public int remove() {
        int element = this.buffer[count];
        this.buffer[count] = Integer.MIN_VALUE;

        count = count - 1;
        return element;
    }
}
