package com.gitlab.hashd.cds;

import java.util.*;

/**
 * Implement data structure “Map” storing pairs of integers (key, value)
 *
 * Define following member functions in O(1) runtime:
 *
 * void insert(key, value),
 * void delete(key),
 * int get(key),
 * int getRandomKey().
 */
public class MapWithRandomKeyFetcher {
    private Map<Integer, Integer> store      = new HashMap<>();
    private Map<Integer, Integer> indexesMap = new HashMap<>();
    private List<Integer>         keys       = new ArrayList<>();
    private int                   size       = 0;

    public MapWithRandomKeyFetcher() {

    }

    public void insert(int key, int value) {
        store.put(key, value);
        keys.add(key);
        indexesMap.put(key, size);
        size++;
    }

    public void delete(int key) {
        if (store.containsKey(key)) {
            store.remove(key);

            // Replace the keys in keys list with the last element in keys
            int idx = indexesMap.get(key);
            keys.set(idx, keys.get(size - 1));
            keys.remove(size - 1);

            size--;
        }
    }

    public int get(int key) {
        return store.get(key);
    }

    public int getRandomKey() {
        Random rand = new Random();
        return keys.get(rand.nextInt(size));
    }
}
