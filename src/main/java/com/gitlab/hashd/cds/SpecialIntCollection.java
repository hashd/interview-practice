package com.gitlab.hashd.cds;

import java.util.ArrayList;
import java.util.List;

/**
 * create a class of integer collection,
     APIs:
     append(int x),
     get(int idx),
     add_to_all(int x)

     add API:
     multiply_to_all(int x)
 *
 */
public class SpecialIntCollection {
    private List<Integer> numbers = new ArrayList<>();
    private List<Integer> divisors = new ArrayList<>();
    private List<Integer> subtractions = new ArrayList<>();
    private int factor = 1;
    private int zeroMultiplierIdx = -1;
    private int addition = 0;

    public void append(int x) {
        numbers.add(x);
        subtractions.add(addition);
        divisors.add(factor);
    }

    public int get(int i) {
        if (i >= numbers.size()) throw new RuntimeException();
        if (i < zeroMultiplierIdx) return addition;

        return numbers.get(i) * (factor/divisors.get(i)) + addition - ((i < zeroMultiplierIdx) ? 0: subtractions.get(i));
    }

    public void addToAll(int x) {
        addition += x;
    }

    public void multiplyToAll(int x) {
        if (x == 0) {
            factor = 1;
            addition = 0;
            zeroMultiplierIdx = numbers.size();
        } else {
            addition = addition * x;
            factor = factor * x;
        }
    }
}
