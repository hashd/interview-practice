package com.gitlab.hashd.combinations;

import java.util.LinkedList;
import java.util.List;

public class BracketsFormer {
    public List<String> getWellFormedStrings(int size) {
        List<String> wellFormedStrings = new LinkedList<>();
        generateWellFormedStrings("", 0, size, wellFormedStrings);
        return wellFormedStrings;
    }

    private void generateWellFormedStrings(String str, int open, int remaining, List<String> wellFormedStrings) {
        if (open == 0 && remaining == 0) {
            wellFormedStrings.add(str);
        }

        if (remaining > 0) {
            generateWellFormedStrings(str + "(", open + 1, remaining - 1, wellFormedStrings);
        }

        if (open > 0) {
            generateWellFormedStrings(str + ")", open - 1, remaining, wellFormedStrings);
        }
    }
}
