package com.gitlab.hashd.combinations;

import java.util.*;

public class CombinationsCreator {
    private void addToCombination(int[] arr, int size, Stack<Integer> current, Stack<List<Integer>> combinations, int currentIdx) {
        if (current.size() == size) {
            combinations.add(new LinkedList<>(current));
            return;
        }

        if (currentIdx < 0 || currentIdx >= arr.length) {
            return;
        }

        current.add(arr[currentIdx]);
        addToCombination(arr, size, current, combinations, currentIdx + 1);

        current.pop();
        addToCombination(arr, size, current, combinations, currentIdx + 1);
    }

    public Stack<List<Integer>> getAllCombinations(int[] arr, int size, boolean isRepetitionAllowed) {
        Stack<Integer> current = new Stack<>();
        Stack<List<Integer>> combinations = new Stack<>();

        Set<Integer> uniqueNumberSet = new HashSet<>();
        for (int number: arr) {
            uniqueNumberSet.add(number);
        }

        int[] uniqueNumbers = new int[uniqueNumberSet.size()];
        int i = 0;
        for (Integer number: uniqueNumberSet) {
            uniqueNumbers[i++] = number;
        }

        if (size > uniqueNumbers.length) {
            return combinations;
        }

        addToCombination(isRepetitionAllowed ? arr: uniqueNumbers, size, current, combinations, 0);
        for (List<Integer> list: combinations) {
            for (Integer num: list) {
                System.out.print(num + ", ");
            }
            System.out.println();
        }

        return combinations;
    }
}
