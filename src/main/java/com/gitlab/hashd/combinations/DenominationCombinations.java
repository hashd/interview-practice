package com.gitlab.hashd.combinations;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class DenominationCombinations {
    public List<List<Integer>> getDenominations(int[] change, int total) {
        List<List<Integer>> denominations = new LinkedList<>();
        addDenominations(change, 0, total, 0, denominations, new Stack<Integer>());
        return denominations;
    }

    private void addDenominations(int[] change, int start, int total, int currentTotal, List<List<Integer>> denominations, Stack<Integer> stack) {
        if (currentTotal == total) {
            List<Integer> newCombination = new LinkedList<>(stack);
            denominations.add(newCombination);
            return;
        }

        for (int i = start; i < change.length; i++) {
            if (change[i] + currentTotal > total) {
                return;
            }
            stack.push(change[i]);
            addDenominations(change, i, total, change[i] + currentTotal, denominations, stack);
            stack.pop();
        }
    }

    public static void main(String[] args) {
        DenominationCombinations dc = new DenominationCombinations();
        int[] arr = {1, 2, 5};

        List<List<Integer>> combinations = dc.getDenominations(arr, 20);
        for (List<Integer> combination: combinations) {
            combination.forEach(i -> System.out.print(i + ", "));
            System.out.println();
        }
    }
}
