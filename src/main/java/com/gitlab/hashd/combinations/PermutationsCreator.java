package com.gitlab.hashd.combinations;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class PermutationsCreator {
    public List<int[]> getPermutations(int[] arr) {
        List<int[]> permutations = new LinkedList<>();
        generatePermutations(arr, new boolean[arr.length], new Stack<>(), permutations);
        return permutations;
    }

    private void generatePermutations(int[] arr, boolean[] selected, Stack<Integer> numbers, List<int[]> permutations) {
        if (numbers.size() == arr.length) {
            int[] permutation = new int[arr.length];
            int i = 0;
            for (int num: numbers) {
                permutation[i++] = num;
            }
            permutations.add(permutation);
        }

        for (int i = 0; i < arr.length; i++) {
            if (!selected[i]) {
                numbers.add(arr[i]);
                selected[i] = true;
                generatePermutations(arr, selected, numbers, permutations);

                numbers.pop();
                selected[i] = false;
            }
        }
    }
}
