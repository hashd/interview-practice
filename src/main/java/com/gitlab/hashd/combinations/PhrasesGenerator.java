package com.gitlab.hashd.combinations;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class PhrasesGenerator {
    public List<String> generate(String[][] words) {
        if (words == null || words.length == 0) return Collections.emptyList();
        List<String> phrases = new LinkedList<>();
        addPhrases(words, 0, new Stack<String>(), phrases);
        return phrases;
    }

    private void addPhrases(String[][] words, int idx, Stack<String> strings, List<String> phrases) {
        if (idx == words.length) {
            phrases.add(strings.stream().reduce("", (acc, str) -> acc + str + " "));
            return;
        }

        for (int i = 0; i < words[idx].length; i++) {
            strings.push(words[idx][i]);
            addPhrases(words, idx + 1, strings, phrases);
            strings.pop();
        }
    }

    public static void main(String[] args) {
        PhrasesGenerator pg = new PhrasesGenerator();
        String[][] words = {
            {"white", "red", "blue", "orange", "purple"},
            {"dotted", "stripped", "checked"},
            {"shorts", "jeans", "pants", "socks", "sleeve"}
        };
        pg.generate(words).forEach(System.out::println);
    }
}
