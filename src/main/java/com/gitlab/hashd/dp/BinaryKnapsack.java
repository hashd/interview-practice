package com.gitlab.hashd.dp;

import java.util.HashMap;
import java.util.Map;

public class BinaryKnapsack {
    private static class Item {
        int weight;
        int value;

        public Item(int weight, int value) {
            this.weight = weight;
            this.value = value;
        }
    }

    public int naiveKnapsack(Item[] items, int weight) {
        return naiveKnapsack(items, weight, 0);
    }

    private int naiveKnapsack(Item[] items, int weight, int i) {
        if (i == items.length) return 0;
        if (items[i].weight > weight) return naiveKnapsack(items, weight, i + 1);

        return Math.max(
          naiveKnapsack(items, weight - items[i].weight, i + 1) + items[i].value,
          naiveKnapsack(items, weight, i + 1)
        );
    }

    public int topDownKnapsack(Item[] items, int weight) {
        return topDownKnapsack(items, new HashMap<Integer, Map<Integer, Integer>>(), weight, 0);
    }

    private int topDownKnapsack(Item[] items, Map<Integer, Map<Integer, Integer>> knapsackCache, int weight, int i) {
        if (i == items.length) return 0;
        if (items[i].weight > weight) return topDownKnapsack(items, knapsackCache, weight, i + 1);
        if (!knapsackCache.containsKey(i)) {
            knapsackCache.put(i, new HashMap<>());
        }
        if (knapsackCache.get(i).containsKey(weight)) {
            return knapsackCache.get(i).get(weight);
        }

        int maxValue = Math.max(
          topDownKnapsack(items, knapsackCache, weight - items[i].weight, i + 1) + items[i].value,
          topDownKnapsack(items, knapsackCache, weight, i + 1)
        );
        knapsackCache.get(i).put(weight, maxValue);
        return maxValue;
    }

    public int bottomUpKnapsack(Item[] items, int weight) {
        if (items.length == 0) return 0;
        int[][] knapsackCache = new int[items.length + 1][weight + 1];
        for (int i = 1; i <= items.length; i++) {
            for (int j = 1; j <= weight; j++) {
                if (items[i - 1].weight > j) {
                    knapsackCache[i][j] = knapsackCache[i - 1][j];
                } else {
                    knapsackCache[i][j] = Math.max(
                      knapsackCache[i - 1][j],
                      knapsackCache[i - 1][j - items[i - 1].weight] + items[i - 1].value
                    );
                }
            }
        }

        return knapsackCache[items.length][weight];
    }

    public static void main(String[] args) {
        Item i1 = new Item(1, 6);
        Item i2 = new Item(2, 10);
        Item i3 = new Item(3, 12);
        Item[] items = {i1, i2, i3};

        System.out.println(new BinaryKnapsack().naiveKnapsack(items, 5));
        System.out.println(new BinaryKnapsack().topDownKnapsack(items, 5));
        System.out.println(new BinaryKnapsack().bottomUpKnapsack(items, 5));
    }
}
