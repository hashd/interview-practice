package com.gitlab.hashd.dp;

public class DecodeWays {
    public int getNoOfWays(String str) {
        if (str == null || str.length() == 0) return 0;
        char[] chars = str.toCharArray();

        int[] ways = new int[chars.length + 1];
        ways[0] = 1; ways[1] = 1;

        for (int i = 1; i < chars.length; i++) {
            ways[i + 1] = ways[i] + (((chars[i - 1] - '0') * 10 + (chars[i] - '0') <= 26)? ways[i - 1]: 0);
        }

        return ways[chars.length];
    }

    public static void main(String[] args) {
        DecodeWays dw = new DecodeWays();
        System.out.println(dw.getNoOfWays("12"));
        System.out.println(dw.getNoOfWays("1223"));
    }
}
