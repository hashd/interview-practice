package com.gitlab.hashd.dp;

/*
There are N number of houses in a straight line. House at index i has valuables worth V[i].

A thief is planning to rob as many houses as possible on this straight line.

Constraint: If he robs house at index i, then he cannot rob houses at indices (i - 1) and (i + 1).

Maximize the total value of valuables which he can rob.
 */
public class HouseRobber {
    public int maximize(int[] valuables) {
        if (valuables == null || valuables.length == 0) return 0;

        int length = valuables.length;
        int[] profits = new int[length + 1];

        profits[0] = 0;
        profits[1] = valuables[0];
        for (int i = 2; i <= length; i++) {
            profits[i] = Math.max(profits[i - 1], profits[i - 2] + valuables[i - 1]);
        }
        return profits[length - 1];
    }
}
