package com.gitlab.hashd.dp;

public class JumpGame {
    public boolean canReach(int[] jumps) {
        if (jumps == null || jumps.length == 0) return false;

        boolean[] jumpable = new boolean[jumps.length];

        jumpable[0] = true;
        for (int i = 0; i < jumps.length - 1; i++) {
            if (jumpable[i] && i + jumps[i] < jumps.length) {
                jumpable[i + jumps[i]] = true;
            }
        }

        return jumpable[jumps.length - 1];
    }

    public static void main(String[] args) {
        JumpGame jg = new JumpGame();
        int[] case1 = {2, 3, 1, 1, 4};
        int[] case2 = {3, 2, 1, 0, 4};

        System.out.println(jg.canReach(case1));
        System.out.println(jg.canReach(case2));
    }
}
