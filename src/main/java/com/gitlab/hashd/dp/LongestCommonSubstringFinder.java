package com.gitlab.hashd.dp;

/**
 * Given two strings ‘X’ and ‘Y’, find the length of the longest common substring.
 *
 * http://practice.geeksforgeeks.org/problems/longest-common-substring/0
 */
public class LongestCommonSubstringFinder {
    public int getLCS(String a, String b) {
        if (a == null || b == null) return 0;
        if (a.length() == 0 || b.length() == 0) return 0;

        int aLen = a.length(), bLen = b.length();
        int[][] lcs = new int[aLen + 1][bLen + 1];

        int max = 0;
        for (int i = 1; i <= aLen; i++) {
            for (int j = 1; j <= bLen; j++) {
                if (a.charAt(i - 1) == b.charAt(j - 1)) {
                    lcs[i][j] = lcs[i - 1][j - 1] + 1;
                    max = Math.max(max, lcs[i][j]);
                } else {
                    lcs[i][j] = 0;
                }
            }
        }

        return max;
    }

    public static void main(String[] args) {
        LongestCommonSubstringFinder lcsf = new LongestCommonSubstringFinder();
        System.out.println(lcsf.getLCS("abcdxyz", "xyzabcd"));
        System.out.println(lcsf.getLCS("zxabcdezy", "yzabcdezx"));
    }
}
