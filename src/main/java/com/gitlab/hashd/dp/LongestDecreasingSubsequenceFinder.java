package com.gitlab.hashd.dp;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LongestDecreasingSubsequenceFinder {
    public int findLongestDecreasingSubsequence(int[] arr) {
        List<Integer> sorted = Arrays.stream(arr).boxed().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        int[][] lis = new int[arr.length + 1][arr.length + 1];
        for (int i = 0; i <= arr.length; i++) {
            lis[i][0] = 0;
            lis[0][i] = 0;
        }

        for (int i = 1; i <= arr.length; i++) {
            for (int j = 1; j <= arr.length; j++) {
                lis[i][j] = ((arr[i-1] == sorted.get(j-1)) ? 1: 0) + Math.max(lis[i - 1][j], lis[i][j - 1]);
            }
        }

        return lis[arr.length][arr.length];
    }
}
