package com.gitlab.hashd.dp;

public class LongestIncreasingSubarrayFinder {
    public int getLength(int[] arr) {
        if (arr == null || arr.length == 0) return 0;

        int max = 0, curr = 1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] >= arr[i - 1]) {
                curr = curr + 1;
            } else {
                max = Math.max(curr, max);
                curr = 1;
            }
        }

        return max;
    }

    public static void main(String[] args) {
        LongestIncreasingSubarrayFinder lisf = new LongestIncreasingSubarrayFinder();

        int[] arr = {1, 3, 2, 3, 4, 8, 7, 9};
        System.out.println(lisf.getLength(arr));
    }
}
