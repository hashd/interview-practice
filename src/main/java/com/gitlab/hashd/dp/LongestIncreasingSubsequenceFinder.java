package com.gitlab.hashd.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class LongestIncreasingSubsequenceFinder {
    public int findLongestIncreasingSubsequence(int[] arr) {
        int[] sorted = Arrays.copyOf(arr, arr.length);
        Arrays.sort(sorted);

        int[][] lis = new int[arr.length + 1][arr.length + 1];
        for (int i = 0; i <= arr.length; i++) {
            lis[i][0] = 0;
            lis[0][i] = 0;
        }

        for (int i = 1; i <= arr.length; i++) {
            for (int j = 1; j <= arr.length; j++) {
                lis[i][j] = ((arr[i-1] == sorted[j-1]) ? 1: 0) + Math.max(lis[i - 1][j], lis[i][j - 1]);
            }
        }

        return lis[arr.length][arr.length];
    }

    public int[] getLongestIncreasingSubsequence(int[] arr) {
        if (arr == null || arr.length <= 1) return arr;

        List<Stack<Integer>> increasingStackList = new ArrayList<>();
        Stack<Integer> increasingStack = new Stack<>();
        increasingStack.push(arr[0]);
        increasingStackList.add(increasingStack);

        for (int i = 1; i < arr.length; i++) {
            int pivot = arr[i];
            increasingStack = new Stack<>();
            Stack<Integer> lastIncreasingStack = increasingStackList.get(increasingStackList.size() - 1);
            Stack<Integer> firstIncreasingStack = increasingStackList.get(0);

            // If pivot is the smallest of all the end elements
            if (pivot < firstIncreasingStack.peek()) {
                increasingStack.push(pivot);
                increasingStackList.set(0, increasingStack);
            // If pivot is the greatest of all the end elements
            } else if (pivot > lastIncreasingStack.peek()) {
                increasingStack.addAll(lastIncreasingStack);
                increasingStack.add(pivot);
                increasingStackList.add(increasingStack);
            // If pivot is somewhere in between, find the index at which smaller than the end of the next but
            // larger than the current one
            } else {
                int left = 0, right = increasingStackList.size() - 1;
                while (left < right) {
                    int mid = left + (right - left) / 2;
                    Stack<Integer> midStack = increasingStackList.get(mid);
                    Stack<Integer> nextToMidStack = increasingStackList.get(mid + 1);
                    if (pivot > midStack.peek() && pivot < nextToMidStack.peek()) {
                        increasingStack.addAll(midStack);
                        increasingStack.add(pivot);
                        increasingStackList.set(mid + 1, increasingStack);
                    } else if (pivot < midStack.peek()) {
                        right = mid;
                    } else {
                        left = mid + 1;
                    }
                }
            }
        }

        Stack<Integer> lastIncreasingStack = increasingStackList.get(increasingStackList.size() - 1);
        int[] lis = new int[lastIncreasingStack.size()];
        for (int j = lis.length - 1; j >= 0; j--) {
            lis[j] = lastIncreasingStack.pop();
        }

        return lis;
    }

    public static void main(String[] args) {
        int[] arr =  {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};

        System.out.println(
            Arrays.toString(
                new LongestIncreasingSubsequenceFinder().getLongestIncreasingSubsequence(arr)
            )
        );
    }
}
