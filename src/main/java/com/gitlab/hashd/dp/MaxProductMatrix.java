package com.gitlab.hashd.dp;

public class MaxProductMatrix {
    public long findMaxProduct(int[][] matrix) {
        if (matrix == null || matrix.length == 0) return 0;
        int rows = matrix.length, cols = matrix[0].length;

        int[][] maxs = new int[rows][cols];
        int[][] mins = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            maxs[i][0] = matrix[i][0];
            mins[i][0] = matrix[i][0];
        }
        for (int i = 0; i < cols; i++) {
            maxs[0][i] = matrix[0][i];
            mins[0][i] = matrix[0][i];
        }

        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < cols; j++) {
                maxs[i][j] = Math.max(maxs[i - 1][j], maxs[i][j - 1]) * matrix[i][j];
                mins[i][j] = Math.min(maxs[i - 1][j], maxs[i][j - 1]) * matrix[i][j];
            }
        }

        return maxs[rows - 1][cols - 1];
    }
}
