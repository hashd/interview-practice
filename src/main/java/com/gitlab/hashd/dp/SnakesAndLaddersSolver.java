package com.gitlab.hashd.dp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://www.careercup.com/question?id=5650714578649088
 *
 * Snake and ladder game. Given a board state with position information for snakes and ladders,
 * find the minimum number of ways(aka dice throws) one can reach from start to end.
 */
public class SnakesAndLaddersSolver {
    public int getMinimumRollsToFinish(int size, Map<Integer, Integer> portalMap) {
        int[] moves = new int[size + 1];
        Arrays.fill(moves, size + 1);
        moves[0] = 0;

        // DP being applied
        for (int i = 0; i < size; i++) {
            for (int roll = 1; roll <= 6; roll++) {
                // Make adjustments according to portal map to next position, if any
                int nextPosition = i + roll;
                while (portalMap.containsKey(nextPosition)) {
                    nextPosition = portalMap.get(nextPosition);
                }

                // If move is within the boundary, update minimum number of moves if necessary
                if (nextPosition <= size) {
                    moves[nextPosition] = Math.min(moves[nextPosition], moves[i] + 1);
                }
            }
        }

        return moves[size];
    }

    public static void main(String[] args) {
        SnakesAndLaddersSolver sals = new SnakesAndLaddersSolver();
        Map<Integer, Integer> portalMap = new HashMap<>();
        portalMap.put(15, 55);
        System.out.println(sals.getMinimumRollsToFinish(100, portalMap));
    }
}
