package com.gitlab.hashd.dp;

import java.util.stream.IntStream;

/**
 * You are climbing a stair case. It takes n steps to reach to the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways
 * can you climb to the top?
 *
 * Note: Given n will be a positive integer.
 */
public class StairClimber {
    public long getNoOfWaysToReachTop(int height) {
        if (height == 0 || height == 1) return 1;

        int[] steps = new int[height + 1];
        steps[0] = 1;
        steps[1] = 1;

        for (int i = 2; i <= height; i++) {
            steps[i] = steps[i - 1] + steps[i - 2];
        }
        return steps[height];
    }

    public static void main(String[] args) {
        StairClimber sc = new StairClimber();
        IntStream.range(1, 10).forEach(i -> {
            System.out.println(sc.getNoOfWaysToReachTop(i));
        });
    }
}
