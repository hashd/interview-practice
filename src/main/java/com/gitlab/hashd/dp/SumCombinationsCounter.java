package com.gitlab.hashd.dp;

// TODO: Fix implementation
public class SumCombinationsCounter {
    public int getNumOfCombinations(int[] arr, int sum) {
        if (arr == null || arr.length == 0) return 0;

        int[][] dp = new int[arr.length + 1][sum + 1];
        for (int i = 1; i <= arr.length; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i <= arr.length; i++) {
            for (int j = 0; j <= sum; j++) {
                int total = j + arr[i - 1];
                if (total <= sum) {
                    dp[i][total] = dp[i][j] + (j != 0? dp[i - 1][j]: 0) + dp[i - 1][total];
                }
                dp[i][j] = Math.max(dp[i][j], dp[i - 1][j]);
            }
        }

        return dp[arr.length][sum];
    }

    public static void main(String[] args) {
        SumCombinationsCounter scc = new SumCombinationsCounter();
        int[] arr = {1, 2, 3};
        System.out.println(scc.getNumOfCombinations(arr, 4));
    }
}
