package com.gitlab.hashd.dp;

public class TowersOfHanoiSolver {
    public void solve(int n, char src, char des, char tmp) {
        if (n <= 0) return;

        solve(n - 1, src, tmp, des);
        System.out.printf("Move disc %d from %c to %c\n", n, src, des);
        solve(n - 1, tmp, des, src);
    }

    public static void main(String[] args) {
        new TowersOfHanoiSolver().solve(4, 'A', 'Z', 'T');
    }
}
