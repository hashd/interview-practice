package com.gitlab.hashd.dp;

import java.util.Arrays;
import java.util.Stack;

public class UnlimitedKnapsack {
    public int getLeastNoOfCoins(int[] coins, int total) {
        Arrays.sort(coins);
        for (int i = 0; i < coins.length; i++) {
            int tmp = coins[coins.length - 1 - i];
            coins[coins.length - 1 - i] = coins[i];
            coins[i] = tmp;
        }

        Stack<Integer> denomination = getLeastNoOfCoins(coins, total, new Stack<Integer>());
        return denomination.size();
    }

    private Stack<Integer> getLeastNoOfCoins(int[] coins, int total, Stack<Integer> integers) {
        if (total == 0) {
            return integers;
        }
        if (total < 0) {
            return null;
        }

        for (int i = 0; i < coins.length; i++) {
            if (coins[i] >= total) {
                integers.push(coins[i]);
                Stack<Integer> result = getLeastNoOfCoins(coins, total - coins[i], integers);
                if (result != null) {
                    return result;
                }
                integers.pop();
            }
        }

        return null;
    }

    public int leastNumberUsingDP(int[] coins, int total) {
        if (coins == null || coins.length == 0 || total == 0) return 0;

        int[][] dp = new int[coins.length + 1][total + 1];
        for (int i = 0; i <= coins.length; i++) {
            dp[i][0] = 0;
            for (int j = 1; j <= total; j++) {
                dp[i][j] = -1;
            }
        }

        for (int i = 1; i <= coins.length; i++) {
            for (int j = 0; j <= total; j++) {
                dp[i][j] = dp[i - 1][j];
            }

            for (int j = 0; j <= total; j++) {
                int newTotal = j + coins[i - 1];
                if (dp[i][j] != -1 && j + coins[i - 1] <= total) {
                    if (dp[i][newTotal] == -1) {
                        dp[i][newTotal] = dp[i][j] + 1;
                    } else {
                        dp[i][newTotal] = Math.min(dp[i][j] + 1, dp[i][newTotal]);
                    }
                }
            }
        }

        return dp[coins.length][total];
    }

    public static void main(String[] args) {
        int[] coins = {1, 5, 10};
        UnlimitedKnapsack lcd = new UnlimitedKnapsack();
        System.out.println(lcd.leastNumberUsingDP(coins, 1));
        System.out.println(lcd.leastNumberUsingDP(coins, 3));
        System.out.println(lcd.leastNumberUsingDP(coins, 7));
        System.out.println(lcd.leastNumberUsingDP(coins, 32));
    }
}
