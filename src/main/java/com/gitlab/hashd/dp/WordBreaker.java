package com.gitlab.hashd.dp;

import java.util.HashSet;
import java.util.Set;

public class WordBreaker {
    public boolean canBeBroken(String str, Set<String> dictionary) {
        if (str == null) return false;

        int length = str.length();
        boolean[] broken = new boolean[length + 1];
        broken[0] = true;

        for (int i = 1; i <= length; i++) {
            if (broken[i - 1]) {
                for (int j = i; j <= length; j++) {
                    String substr = str.substring(i - 1, j);
                    if (dictionary.contains(substr)) {
                        broken[j] = true;
                    }
                }
            }
        }

        return broken[length];
    }

    public static void main(String[] args) {
        Set<String> dictionary = new HashSet<>();
        dictionary.add("samsung");
        dictionary.add("and");
        dictionary.add("mango");

        WordBreaker wb = new WordBreaker();
        System.out.println(wb.canBeBroken("samsungandmango", dictionary));
        System.out.println(wb.canBeBroken("samsungandapple", dictionary));
    }
}
