package com.gitlab.hashd.geometry;

import com.gitlab.hashd.utils.Point;

public class CirclesOverlapChecker {
    public boolean doOverlap(Point c1, int r1, Point c2, int r2) {
        return Point.distanceBetween(c1, c2) < r1 + r2;
    }
}
