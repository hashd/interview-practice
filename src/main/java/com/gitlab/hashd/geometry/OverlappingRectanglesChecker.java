package com.gitlab.hashd.geometry;

import com.gitlab.hashd.utils.Point;

public class OverlappingRectanglesChecker {
    public boolean doOverlap(Point a1, Point a2, Point b1, Point b2) {
        if (a1 == null || b1 == null || a2 == null || b2 == null) {
            return false;
        }

        Point b3 = new Point(b1.getX(), b2.getY()), b4 = new Point(b2.getX(), b1.getY());
        Point[] bPoints = {b1, b2, b3, b4};

        for (Point b : bPoints) {
            if ((b.getX() > Math.min(a1.getX(), a2.getX()) && b.getX() < Math.max(a1.getX(), a2.getX())) &&
                    (b.getY() > Math.min(a1.getY(), a2.getY()) && b.getY() < Math.max(a1.getY(), a2.getY()))) {
                return true;
            }
        }
        return false;
    }

    public int area(Point a1, Point a2, Point b1, Point b2) {
        if (a1 == null || b1 == null || a2 == null || b2 == null) {
            return -1;
        }

        int x1 = Math.max(Math.min(a1.getX(), a2.getX()), Math.min(b1.getX(), b2.getX()));
        int x2 = Math.min(Math.max(a1.getX(), a2.getX()), Math.max(b1.getX(), b2.getX()));
        int y1 = Math.max(Math.min(a1.getY(), a2.getY()), Math.min(b1.getY(), b2.getY()));
        int y2 = Math.min(Math.max(a1.getY(), a2.getY()), Math.max(b1.getY(), b2.getY()));

        if (x2 <= x1 || y2 <= y1) {
            return 0;
        }

        return (x2 - x1) * (y2 - y1);
    }

    public static void main(String[] args) {
        Point a1 = new Point(2, 1);
        Point a2 = new Point(5, 5);
        Point b1 = new Point(3, 2);
        Point b2 = new Point(5, 7);

        OverlappingRectanglesChecker orc = new OverlappingRectanglesChecker();
        System.out.println(orc.doOverlap(a1, a2, b1, b2));
        System.out.println(orc.area(a1, a2, b1, b2));
    }
}
