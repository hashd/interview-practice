package com.gitlab.hashd.graphs;

public class Edge {
    GraphNode src;
    GraphNode des;

    public Edge(GraphNode src, GraphNode des) {
        this.src = src;
        this.des = des;
    }
}
