package com.gitlab.hashd.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    private Map<String, GraphNode> nodesMap;
    private List<GraphNode> nodes;
    private List<Edge> edges;

    public Graph() {
        nodesMap = new HashMap<>();
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
    }

    public void addNode(String name) {
        if (!nodesMap.containsKey(name)) {
            GraphNode node = new GraphNode(name);
            nodesMap.put(name, node);
            nodes.add(node);
        }
    }

    public void addEdge(String src, String des) {
        if (nodesMap.containsKey(src) && nodesMap.containsKey(des)) {
            GraphNode srcNode = nodesMap.get(src);
            GraphNode desNode = nodesMap.get(des);
            edges.add(new Edge(srcNode, desNode));
            edges.add(new Edge(desNode, srcNode));
            srcNode.addConnection(desNode);
            desNode.addConnection(srcNode);
        }
    }
}
