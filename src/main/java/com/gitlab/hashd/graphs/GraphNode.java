package com.gitlab.hashd.graphs;

import java.util.LinkedList;
import java.util.List;

public class GraphNode {
    String name;
    List<GraphNode> connectedNodes;

    public GraphNode(String name) {
        this.name = name;
        this.connectedNodes = new LinkedList<>();
    }

    public GraphNode(String name, List<GraphNode> connectedNodes) {
        this.name = name;
        this.connectedNodes = connectedNodes;
    }

    public void addConnection(GraphNode desNode) {
        this.connectedNodes.add(desNode);
    }
}
