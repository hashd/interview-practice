package com.gitlab.hashd.graphs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class TopologicalSorter {
    public Stack<String> sort(DirectedGraph graph) {
        List<GraphNode> nodes = graph.nodes;
        List<Edge> edges = graph.edges;
        Map<String, GraphNode> nodesMap = graph.nodesMap;
        Map<String, Integer> inwardEdgesMap = new HashMap<>();

        for (GraphNode node: nodes) {
            inwardEdgesMap.put(node.name, 0);
        }
        for (Edge edge: edges) {
            String name = edge.des.name;
            inwardEdgesMap.put(name, inwardEdgesMap.get(name) + 1);
        }

        Stack<String> resolvedStack = new Stack<>();
        while (!nodesMap.isEmpty()) {
            for (GraphNode node: nodes) {
                if (nodesMap.containsKey(node.name) && inwardEdgesMap.get(node.name) == 0) {
                    resolvedStack.push(node.name);
                    nodesMap.remove(node.name);
                    for (Edge edge: edges) {
                        if (edge.src == node) {
                            inwardEdgesMap.put(edge.des.name, inwardEdgesMap.get(edge.des.name) - 1);
                        }
                    }
                }
            }
        }

        return resolvedStack;
    }

    public static void main(String[] args) {
        DirectedGraph dg = new DirectedGraph();
        dg.addNode("a");
        dg.addNode("b");
        dg.addNode("c");
        dg.addNode("d");
        dg.addNode("e");
        dg.addNode("f");
        dg.addEdge("a", "d");
        dg.addEdge("f", "b");
        dg.addEdge("f", "a");
        dg.addEdge("b", "d");
        dg.addEdge("d", "c");

        TopologicalSorter dr = new TopologicalSorter();
        Stack<String> resolved = dr.sort(dg);

        for (String dep: resolved) {
            System.out.println(dep);
        }
    }
}
