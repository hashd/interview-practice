package com.gitlab.hashd.heaps;

public abstract class AbstractHeap implements Heap {
    protected int left(int idx) {
        return idx * 2 + 1;
    }

    protected int right(int idx) {
        return idx * 2 + 2;
    }

    protected int parent(int idx) {
        return (idx - 1) / 2;
    }

    public boolean isEmpty() {
        return size() == 0;
    }
}
