package com.gitlab.hashd.heaps;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GenericHeap extends AbstractHeap {
    private static final int INITIAL_CAPACITY = 8;
    private static final Comparator<Integer> DEFAULT_COMPARATOR = Comparator.naturalOrder();

    private List<Integer> contents;
    private Comparator<Integer> comparator;
    private int capacity;

    public GenericHeap() {
        contents = new ArrayList<>(INITIAL_CAPACITY);
        capacity = INITIAL_CAPACITY;
        this.comparator = DEFAULT_COMPARATOR;
    }

    public GenericHeap(Comparator<Integer> comparator) {
        this();
        this.comparator = comparator;
    }

    private void heapifyDown(int idx) {
        if (left(idx) >= contents.size()) {
            return;
        }

        int leftChild = contents.get(left(idx));
        int rightChild = right(idx) < contents.size() ? contents.get(right(idx)) : Integer.MAX_VALUE;
        int current = contents.get(idx);

        if (comparator.compare(leftChild, current) < 0 && comparator.compare(leftChild, rightChild) <= 0) {
            contents.set(idx, leftChild);
            contents.set(left(idx), current);
            heapifyDown(left(idx));
        } else if (comparator.compare(rightChild, current) < 0 && comparator.compare(rightChild, leftChild) <= 0) {
            contents.set(idx, rightChild);
            contents.set(right(idx), current);
            heapifyDown(right(idx));
        } else {
            return;
        }
    }

    private void heapifyUp(int idx) {
        int parent = contents.get(parent(idx));
        int current = contents.get(idx);

        if (comparator.compare(current, parent) < 0) {
            contents.set(parent(idx), current);
            contents.set(idx, parent);
            heapifyUp(parent(idx));
        }
    }

    public void push(int num) {
        if (contents.size() == capacity) {
            List<Integer> copy = new ArrayList<>(capacity * 2);
            for (Integer content: contents) copy.add(content);
            contents = copy;
            capacity = capacity * 2;
        }
        contents.add(num);
        heapifyUp(contents.size() - 1);
    }

    public int peek() {
        return contents.get(0);
    }

    public int pop() {
        int top = contents.get(0);
        contents.set(0, contents.get(contents.size() - 1));
        contents.remove(contents.size() - 1);

        heapifyDown(0);
        return top;
    }

    public int size() {
        return contents.size();
    }
}
