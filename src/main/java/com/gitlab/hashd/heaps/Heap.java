package com.gitlab.hashd.heaps;

public interface Heap {
    void push(int num);
    int peek();
    int pop();
    int size();
    boolean isEmpty();
}
