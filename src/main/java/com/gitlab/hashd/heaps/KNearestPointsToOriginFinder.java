package com.gitlab.hashd.heaps;

import com.gitlab.hashd.utils.Point;

import java.util.*;

public class KNearestPointsToOriginFinder {
    public List<Point> getKClosestPoints(List<Point> points, int k) {
        if (points == null || points.size() == 0) return Collections.emptyList();

        PriorityQueue<Point> maxHeap = new PriorityQueue<>(Comparator.comparingInt(Point::distanceFromOrigin));
        for (Point p: points) {
            if (maxHeap.size() < k) {
                maxHeap.add(p);
            } else {
                if (maxHeap.peek().distanceFromOrigin() > p.distanceFromOrigin()) {
                    maxHeap.poll();
                    maxHeap.add(p);
                }
            }
        }

        return new ArrayList<>(maxHeap);
    }
}
