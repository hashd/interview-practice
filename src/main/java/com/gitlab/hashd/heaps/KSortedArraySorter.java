package com.gitlab.hashd.heaps;

import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.heaps.MinHeap;

public class KSortedArraySorter {
    public void sortKSortedArray(int[] arr, int k) {
        int idx = 0;
        Heap minHeap = new MinHeap();
        for (int i = 0; i < arr.length; i++) {
            if (i > k) {
                arr[idx++] = minHeap.pop();
            }
            minHeap.push(arr[i]);
        }

        // Add remaining elements in the heap to the array
        while (!minHeap.isEmpty()) {
            arr[idx++] = minHeap.pop();
        }
    }
}
