package com.gitlab.hashd.heaps;

import java.util.LinkedList;
import java.util.List;

public class MedianInAStreamFinder {
    public List<Double> getMedians(int[] arr) {
        List<Double> medians = new LinkedList<>();
        MaxHeap maxHeap = new MaxHeap();
        MinHeap minHeap = new MinHeap();

        for (int i = 0; i < arr.length; i++) {
            if (maxHeap.size() == minHeap.size()) {
                minHeap.push(arr[i]);
                medians.add((double) minHeap.peek());
            } else {
                if (minHeap.peek() < arr[i]) {
                    maxHeap.push(minHeap.pop());
                    minHeap.push(arr[i]);
                } else {
                    maxHeap.push(arr[i]);
                }
                medians.add((minHeap.peek() + maxHeap.peek()) / 2.0);
            }
        }

        return medians;
    }

    public static void main(String[] args) {
        int[] arr = {3, 2, 6, 5, 4, 7, 8, 9, 10};
        MedianInAStreamFinder miasf = new MedianInAStreamFinder();
        miasf.getMedians(arr).forEach(System.out::println);
    }
}
