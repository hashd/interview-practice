package com.gitlab.hashd.heaps;

public class MinimumSumReducer {
    public int reduce(int[] arr) {
        MinHeap heap = new MinHeap();
        for (int i = 0; i < arr.length; i++) {
            heap.push(arr[i]);
        }

        int sum = 0;
        while (heap.size() != 1) {
            int least = heap.pop(), secondLeast = heap.pop();
            heap.push(least + secondLeast);

            sum += (least + secondLeast);
        }

        return sum;
    }

    public static void main(String[] args) {
        MinimumSumReducer msr = new MinimumSumReducer();
        int[] arr = {1, 2, 3};
        System.out.println(msr.reduce(arr));
    }
}
