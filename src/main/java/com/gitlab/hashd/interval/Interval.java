package com.gitlab.hashd.interval;

public class Interval {
    private int start;
    private int end;

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    // Alternate: Length of merged interval should be less than sum of the both
    public static boolean doOverlap(Interval i1, Interval i2) {
        return Math.max(i1.getStart(), i2.getStart()) < Math.min(i2.getEnd(), i1.getEnd());
    }

    public static Interval getOverlap(Interval i1, Interval i2) {
        int start = Math.max(i1.getStart(), i2.getStart());
        int end   = Math.min(i2.getEnd(), i1.getEnd());

        return start < end ? new Interval(start, end): null;
    }

    public static Interval merge(Interval i1, Interval i2) {
        return new Interval(Math.min(i1.getStart(), i2.getStart()), Math.max(i2.getStart(), i1.getEnd()));
    }
}
