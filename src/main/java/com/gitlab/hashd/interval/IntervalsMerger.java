package com.gitlab.hashd.interval;

import java.util.*;

public class IntervalsMerger {
    public static class Interval {
        int start;
        int end;

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "Interval{" +
                    "start=" + start +
                    ", end=" + end +
                    '}';
        }
    }

    public List<Interval> merge(List<Interval> intervals) {
        Collections.sort(intervals, Comparator.comparingInt(o -> o.start));
        Stack<Interval> intervalsStack = new Stack<>();

        for (Interval interval: intervals) {
            if (intervalsStack.empty()) {
                intervalsStack.push(interval);
                continue;
            }

            if (interval.start < intervalsStack.peek().end) {
                Interval prev = intervalsStack.pop();
                intervalsStack.push(new Interval(prev.start, Math.max(prev.end, interval.end)));
            } else {
                intervalsStack.push(interval);
            }
        }

        return new LinkedList<>(intervalsStack);
    }

    public static void main(String[] args) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(8, 16));
        intervals.add(new Interval(15, 18));

        List<Interval> merged = new IntervalsMerger().merge(intervals);
        merged.stream().forEach(System.out::println);
    }
}
