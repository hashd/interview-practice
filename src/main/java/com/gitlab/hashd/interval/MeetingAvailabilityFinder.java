package com.gitlab.hashd.interval;

import com.gitlab.hashd.utils.Tuple;

import java.util.*;

public class MeetingAvailabilityFinder {
    public List<Tuple> getAvailability(List<Tuple> meetings) {
        List<Tuple> availability = new LinkedList<>();

        meetings.sort(Comparator.comparingInt(Tuple::getA));
        int nextAvailable = 0;
        for (Tuple meeting: meetings) {
            if (meeting.getA() > nextAvailable) {
                availability.add(new Tuple(nextAvailable, meeting.getA()));
                nextAvailable = meeting.getB();
            } else {
                nextAvailable = Math.max(nextAvailable, meeting.getB());
            }
        }

        if (nextAvailable != 24) availability.add(new Tuple(nextAvailable, 24));
        return availability;
    }

    public static void main(String[] args) {
        MeetingAvailabilityFinder maf = new MeetingAvailabilityFinder();
        List<Tuple> meetings = new ArrayList<>();
        meetings.add(new Tuple(4, 5));
        meetings.add(new Tuple(5, 9));
        meetings.add(new Tuple(12, 14));
        meetings.add(new Tuple(5, 10));
        meetings.add(new Tuple(13, 16));
        meetings.add(new Tuple(11, 14));

        List<Tuple> availability = maf.getAvailability(meetings);
        availability.forEach(System.out::println);
    }
}
