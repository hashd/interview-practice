package com.gitlab.hashd.interval;

import java.util.Arrays;

/**
 * Given arrival and departure times of all trains that reach a railway station,
 * find the minimum number of platforms required for the railway station so that no train waits.
 *
 * http://practice.geeksforgeeks.org/problems/minimum-platforms/0
 */
public class MinimumPlatformsRequiredFinder {
    /*
    * Given a schedule containing arrival and departure time of trains in a station, find minimum number of platforms
    * needed in the station so to avoid any delay in arrival of any train.
    * */
    public int findMinimumNumberOfPlatformsNeeded(int[] arrivals, int[] departures) {
        int[] sArrivals = Arrays.copyOf(arrivals, arrivals.length);
        int[] sDepartures = Arrays.copyOf(departures, departures.length);

        Arrays.sort(sArrivals);
        Arrays.sort(sDepartures);

        int aPivot = 0, dPivot = 0;
        int minimumPlatformsNeeded = 0, currentNumberOfTrains = 0;

        while (aPivot < sArrivals.length) {
            if (sArrivals[aPivot] < sDepartures[dPivot]) {
                currentNumberOfTrains++;
                aPivot++;
                minimumPlatformsNeeded = Math.max(minimumPlatformsNeeded, currentNumberOfTrains);
            } else {
                currentNumberOfTrains--;
                dPivot++;
            }
        }

        return minimumPlatformsNeeded;
    }
}
