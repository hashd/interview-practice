package com.gitlab.hashd.lists;

import java.util.HashSet;
import java.util.Set;

public class DuplicatesRemover {
    public void removeDuplicates(LinkedList head) {
        Set<Integer> elementsSet = new HashSet<>();

        LinkedList pivot = head;
        while (pivot != null && pivot.next != null) {
            elementsSet.add(pivot.data);

            if (elementsSet.contains(pivot.next.data)) {
                pivot.next = pivot.next.next;
            } else {
                pivot = pivot.next;
            }
        }
    }
}
