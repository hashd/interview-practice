package com.gitlab.hashd.lists;

import java.util.HashSet;
import java.util.Set;

public class LinkedList {
    int        data;
    LinkedList next;

    public LinkedList(int d) {
        data = d;
    }

    public static int size(LinkedList node) {
        if (node == null) return 0;

        int length = 0;
        while (node != null) {
            length = length + 1;
            node = node.next;
        }
        return length;
    }

    public static LinkedList connect(LinkedList...nodes) {
        for (int i = 0; i < nodes.length; i++) {
            nodes[i].next = (i != nodes.length - 1)? nodes[i + 1]: null;
        }
        return nodes[0];
    }

    public static String toString(LinkedList node) {
        String str = "";
        LinkedList pivot = node;
        while (pivot != null) {
            str += (pivot.data + " -> ");
            pivot = pivot.next;
        }
        str += "()";
        return str;
    }

    public static void print(LinkedList node) {
        System.out.println(toString(node));
    }

    public static int rSize(LinkedList node) {
        if (node == null) return 0;

        return 1 + rSize(node.next);
    }

    public static int rAccSize(LinkedList node) {
        return rAccSize(node, 0);
    }

    private static int rAccSize(LinkedList node, int acc) {
        if (node == null) return 0;

        return rAccSize(node.next, acc + 1);
    }

    public static LinkedList add(LinkedList head, LinkedList node) {
        if (head == null) return head;

        LinkedList pivot = head;
        while (pivot.next != null) {
           pivot = pivot.next;
        }
        pivot.next = node;

        return head;
    }

    public static LinkedList push(LinkedList head, LinkedList node) {
        return add(head, node);
    }

    public static LinkedList unshift(LinkedList head, LinkedList node) {
        if (node == null) return null;

        node.next = head;
        return node;
    }

    public static LinkedList shift(LinkedList head) {
        return head == null ? null: head.next;
    }

    public static LinkedList pop(LinkedList head) {
        if (head == null || head.next == null) return head;

        LinkedList pivot = head, val;
        while (pivot.next.next != null) {
            pivot = pivot.next;
        }
        val = pivot.next;
        pivot.next = null;

        return val;
    }

    public static LinkedList rReverse(LinkedList head) {
        if (head == null || head.next == null) {
            return head;
        }

        LinkedList newHead = rReverse(head.next);
        add(newHead, head);

        return newHead;
    }

    public static LinkedList reverse(LinkedList head) {
        if (head == null || head.next == null) {
            return head;
        }

        LinkedList prev = null, curr = head, next = curr.next;
        while (curr != null) {
            curr.next = prev;
            prev = curr;
            curr = next;
            next = next == null ? null: next.next;
        }

        return prev;
    }

    public static boolean hasALoop(LinkedList node) {
        LinkedList slow = node, fast = node;
        while (slow != null && fast != null) {
            slow = slow.next;
            fast = fast.next;

            if (fast == null) break;
            fast = fast.next;

            if (slow == fast) return true;
        }
        return false;
    }

    public static LinkedList connectAlternateNodes(LinkedList head) {
        if (head == null || head.next == null) return head;

        LinkedList pivot = head.next.next, odds = head, evens = head.next;
        evens.next = null;
        odds.next = null;

        while (pivot != null && pivot.next != null) {
            LinkedList nextIter = pivot.next.next;

            LinkedList even = pivot.next;
            LinkedList odd = pivot;
            even.next = null;
            odd.next = null;

            add(odds, odd);
            add(evens, even);

            pivot = nextIter;
        }

        if (pivot != null) pivot.next = null;

        add(odds, pivot);
        add(odds, evens);

        return head;
    }

    public static LinkedList addLastToFirst(LinkedList head) {
        return unshift(head, pop(head));
    }

    public static LinkedList splitIntoTwoHalves(LinkedList head) {
        if (head == null || head.next == null) return null;

        LinkedList slow = head, fast = head.next;
        while (fast != null) {
            slow = fast.next != null ? slow.next: slow;
            fast = fast.next != null ? fast.next.next: null;
        }
        LinkedList val = slow.next;
        slow.next = null;

        return val;
    }

    public static boolean isPalindrome(LinkedList head) {
        LinkedList first = head, second = reverse(splitIntoTwoHalves(head));

        while (first != null && second != null) {
            if (first.data != second.data) {
                return false;
            }

            first = first.next;
            second = second.next;
        }

        return first.next == null;
    }

    public static LinkedList mergeAlternateNodes(LinkedList a, LinkedList b) {
        LinkedList left = a, right = b; int count = 0;
        while (left != null && right != null) {
            if (count % 2 == 0) {
                LinkedList leftNext = left.next;
                left.next = right;
                left = leftNext;
            } else {
                LinkedList rightNext = right.next;
                right.next = left;
                right = rightNext;
            }
            count += 1;
        }

        return a;
    }

    public static LinkedList findIntersectionOfSortedLists(LinkedList a, LinkedList b) {
        while (a != null && b != null) {
            if (a == b) {
                return a;
            } else if (a.data < b.data) {
                a = a.next;
            } else if (a.data > b.data) {
                b = b.next;
            } else {
                a = a.next;
                b = b.next;
            }
        }
        return null;
    }

    public static LinkedList findKthNodeFromEnd(LinkedList a, int k) {
        LinkedList right = a, left = a;

        int num = 0;
        while (num < k && right != null) {
            right = right.next;
            num = num + 1;
        }

        if (num != k) return null;

        while (right != null) {
            left = left.next;
            right = right.next;
        }
        return left;
    }

    public static void processFromEnd(LinkedList head, LinkedListProcessor fn) {
        if (head == null) {
            return;
        }
        processFromEnd(head.next, fn);
        fn.apply(head);
    }

    public static void printFromEnd(LinkedList head) {
        processFromEnd(head, node -> System.out.println(node.data));
    }
}
