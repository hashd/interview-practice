package com.gitlab.hashd.lists;

@FunctionalInterface
public interface LinkedListProcessor {
    void apply(LinkedList node);
}
