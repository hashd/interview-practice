package com.gitlab.hashd.lists;

public class LinkedListReverser {
    public LinkedList reverse(LinkedList head) {
        if (head == null || head.next == null) {
            return head;
        }

        LinkedList prev = null, curr = head, next = curr.next;
        while (curr != null) {
            curr.next = prev;
            prev = curr;
            curr = next;
            next = next == null ? null: next.next;
        }

        return prev;
    }
}
