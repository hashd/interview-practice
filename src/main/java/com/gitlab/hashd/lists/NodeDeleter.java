package com.gitlab.hashd.lists;

public class NodeDeleter {
    public void delete(LinkedList ll) {
        if (ll == null || ll.next == null) return;

        ll.data = ll.next.data;
        ll.next = ll.next.next;
    }
}
