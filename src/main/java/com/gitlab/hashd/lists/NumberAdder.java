package com.gitlab.hashd.lists;

public class NumberAdder {
    public LinkedList add(LinkedList a, LinkedList b) {
        LinkedList aPivot = a, bPivot = b, result = null, rPivot = null;
        int carry = 0;

        while (aPivot != null && bPivot != null) {
            int sum = aPivot.data + bPivot.data + carry;
            carry = sum / 10;

            LinkedList tmp = new LinkedList(sum % 10);
            if (result == null) {
                result = tmp;
            } else {
                rPivot.next = tmp;
            }
            rPivot = tmp;

            aPivot = aPivot.next;
            bPivot = bPivot.next;
        }

        LinkedList pivot = (aPivot == null) ? bPivot: aPivot;
        while (pivot != null) {
            int sum = pivot.data + carry;
            carry = sum / 10;

            LinkedList tmp = new LinkedList(sum % 10);
            rPivot.next = tmp;
            rPivot = tmp;

            pivot = pivot.next;
        }

        while (carry != 0) {
            LinkedList tmp = new LinkedList(carry % 10);
            rPivot.next = tmp;
            rPivot = tmp;

            carry = carry / 10;
        }

        return result;
    }
}
