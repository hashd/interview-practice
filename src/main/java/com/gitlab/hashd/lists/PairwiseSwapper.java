package com.gitlab.hashd.lists;

public class PairwiseSwapper {
    public void pairwiseSwap(LinkedList head) {
        if (head == null || head.next == null) return;

        LinkedList pivot = head;
        while (pivot != null && pivot.next != null) {
            int tmp = pivot.data;
            pivot.data = pivot.next.data;
            pivot.next.data = tmp;

            pivot = pivot.next.next;
        }
    }

    public static void main(String[] args) {
        LinkedList a1 = new LinkedList(1);
        LinkedList a2 = new LinkedList(2);
        LinkedList a3 = new LinkedList(3);
        LinkedList a4 = new LinkedList(4);
        LinkedList a5 = new LinkedList(5);
        LinkedList a6 = new LinkedList(6);

        LinkedList.connect(a1, a2, a3, a4, a5);
        new PairwiseSwapper().pairwiseSwap(a1);

        System.out.println(LinkedList.toString(a1));
    }
}
