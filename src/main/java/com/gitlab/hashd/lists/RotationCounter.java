package com.gitlab.hashd.lists;

public class RotationCounter {
    public int findRotations(LinkedList ll) {
        if (ll == null || ll.next == null) return 0;

        LinkedList pivot = ll;
        int count = 1;
        while (pivot.next != null) {
            if (pivot.data <= pivot.next.data) {
                count++;
            } else {
                return count;
            }
            pivot = pivot.next;
        }

        return 0;
    }
}
