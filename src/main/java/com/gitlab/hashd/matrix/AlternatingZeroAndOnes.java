package com.gitlab.hashd.matrix;

public class AlternatingZeroAndOnes {
    public int[][] fillAlternateRectanglesOfZerosAndOnes(int rows, int cols) {
        int min = Math.min(rows, cols) / 2;
        int[][] matrix = new int[rows][cols];

        for (int i = 0; i < min; i++) {
            int lowRow = i, highRow = rows - 1 - i;
            int lowCol = i, highCol = cols - 1 - i;

            for (int j = lowCol; j <= highCol; j++) {
                matrix[lowRow][j] = (i + 1) % 2;
                matrix[highRow][j] = (i + 1) % 2;
            }

            for (int j = lowRow + 1; j < highRow; j++) {
                matrix[j][lowCol] = (i + 1) % 2;
                matrix[j][highCol] = (i + 1) % 2;
            }
        }

        return matrix;
    }
}
