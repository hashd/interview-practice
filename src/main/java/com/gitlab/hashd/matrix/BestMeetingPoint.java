package com.gitlab.hashd.matrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BestMeetingPoint {
    public int totalDistanceFromBestMeetingPoint(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;

        int rows = grid.length, cols = grid[0].length;
        List<Integer> verticals = new ArrayList<>(), horizontals = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1) {
                    verticals.add(i);
                    horizontals.add(j);
                }
            }
        }

        Collections.sort(verticals);
        Collections.sort(horizontals);

        int v = verticals.get(verticals.size() / 2);
        int h = horizontals.get(horizontals.size() / 2);

        int distance = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1) {
                    distance += (Math.abs(v - i) + Math.abs(h - j));
                }
            }
        }

        return distance;
    }
}
