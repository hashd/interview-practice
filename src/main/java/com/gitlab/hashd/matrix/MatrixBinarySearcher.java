package com.gitlab.hashd.matrix;

import com.gitlab.hashd.arrays.easy.BinarySearcher;
import com.gitlab.hashd.utils.Tuple;

public class MatrixBinarySearcher {
    public Tuple search(int[][] matrix, int key) {
        if (matrix == null || matrix.length == 0) return null;
        int rows = matrix.length, cols = matrix[0].length;

        int top = 0, bottom = rows, row = -1;
        while (top < bottom) {
            int mid = top + (bottom - top) / 2;
            if (key < matrix[mid][0] && mid == 0) {
                break;
            } else if (key >= matrix[mid][0] && (mid == bottom - 1 || (key < matrix[mid + 1][0]))) {
                row = mid;
            } else if (key > matrix[mid][0]) {
                top = mid;
            } else {
                bottom = mid - 1;
            }
        }

        if (row == -1) return null;
        int col = new BinarySearcher().binarySearch(matrix[row], key);
        if (col == -1) return null;

        return new Tuple(row, col);
    }
}
