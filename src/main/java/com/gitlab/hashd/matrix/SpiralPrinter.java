package com.gitlab.hashd.matrix;

/**
 * Print a given matrix in a spiral manner.
 *
 * http://practice.geeksforgeeks.org/problems/spirally-traversing-a-matrix/0
 */
public class SpiralPrinter {
    public void print(int[][] matrix) {
        if (matrix == null) return;
        if (matrix.length == 0) return;

        int rows = matrix.length, cols = matrix[0].length;
        int min = (Math.min(rows, cols) + 1) / 2;

        for (int i = 0; i < min; i++) {
            int topRow = i, bottomRow = rows - 1 - i;
            int leftCol = i, rightCol = cols - 1 - i;
            for (int j = leftCol; j <= rightCol; j++) {
                System.out.print(matrix[topRow][j] + " ");
            }
            for (int j = topRow + 1; j <= bottomRow; j++) {
                System.out.print(matrix[j][rightCol] + " ");
            }
            if (topRow != bottomRow) {
                for (int j = rightCol; j >= leftCol; j--) {
                    System.out.print(matrix[bottomRow][j] + " ");
                }
            }
            if (leftCol != rightCol) {
                for (int j = bottomRow - 1; j > topRow; j--) {
                    System.out.print(matrix[j][leftCol] + " ");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] matrix = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix[i][j] = 5 * i + j;
            }
        }
        new SpiralPrinter().print(matrix);
    }
}
