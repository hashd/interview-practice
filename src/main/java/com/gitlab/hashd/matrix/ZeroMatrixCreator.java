package com.gitlab.hashd.matrix;

public class ZeroMatrixCreator {
    public void update(int[][] matrix) {
        if (matrix == null || matrix.length == 0) return;
        int rows = matrix.length, cols = matrix[0].length;

        boolean[] zeroRows = new boolean[rows], zeroCols = new boolean[cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == 1) {
                    zeroRows[i] = true;
                    zeroCols[j] = true;
                }
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (zeroRows[i] || zeroCols[j]) {
                    matrix[i][j] = 1;
                }
            }
        }
    }

    public void updateAlt(boolean[][] matrix) {
        if (matrix == null || matrix.length == 0) return;
        int rows = matrix.length, cols = matrix[0].length;

        boolean zeroCol = false, zeroRow = false;
        for (int i = 0; i < rows; i++) {
            zeroCol = zeroCol || matrix[i][0];
        }
        for (int i = 0; i < cols; i++) {
            zeroRow = zeroRow || matrix[0][i];
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][0] = matrix[i][0] || matrix[i][j];
                matrix[0][j] = matrix[0][j] || matrix[i][j];
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = matrix[i][0] || matrix[0][j];
            }
        }

        for (int i = 0; i < rows; i++) {
            matrix[i][0] = zeroCol;
        }
        for (int i = 0; i < cols; i++) {
            matrix[0][i] = zeroRow;
        }
    }
}
