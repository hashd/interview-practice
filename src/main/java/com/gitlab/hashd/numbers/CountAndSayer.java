package com.gitlab.hashd.numbers;

public class CountAndSayer {
    public void countAndSay(int n) {
        String number = "1";
        for (int i = 0; i < n; i++) {
            System.out.println(number);

            char[] digits = number.toCharArray();
            number = "";

            char currentDigit = digits[0]; int count = 0;
            StringBuilder numberBuilder = new StringBuilder(number);
            for (char digit : digits) {
                if (digit == currentDigit) {
                    count = count + 1;
                } else {
                    numberBuilder.append(String.valueOf(count) + currentDigit);
                    currentDigit = digit;
                    count = 1;
                }
            }
            number = numberBuilder.toString();
            number = number + count + currentDigit;
        }
    }
}
