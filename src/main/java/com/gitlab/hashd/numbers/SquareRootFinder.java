package com.gitlab.hashd.numbers;

public class SquareRootFinder {
    public static final double TOLERANCE = 0.0000001;

    public double squareRoot(double num) {
        double root = 1;
        while (num > root * root) {
            root = root * 2;
        }

        double low = root/2, high = root, mid = low + (high - low) / 2;
        while (Math.abs(num - (mid * mid)) > TOLERANCE) {
            if (num - mid * mid > TOLERANCE) {
                low = mid;
            } else if (num - mid * mid < TOLERANCE) {
                high = mid;
            } else {
                return mid;
            }
            mid = low + (high - low) / 2;
        }

        return mid;
    }

    public double squareRoot(int num) {
      return squareRoot(1D * num);
    };
}
