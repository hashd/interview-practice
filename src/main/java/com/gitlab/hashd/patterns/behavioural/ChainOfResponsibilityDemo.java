package com.gitlab.hashd.patterns.behavioural;

/**
 * - Decouples sender and receiver.
 * - Runtime configuration
 * - Hierarchical in nature
 * - Careful with large chains
 */

enum RequestType {
    CONFERENCE, PURCHASE, SALE
}

class Request {
    RequestType requestType;
    int cost;

    public Request(RequestType type, int price) {
        requestType = type;
        cost = price;
    }
}

interface Handler {
    void handleRequest(Request request);
}

class Director implements Handler {
    Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    @Override
    public void handleRequest(Request request) {
        if (request.requestType == RequestType.CONFERENCE) {
            System.out.println("Director can approve this");
        } else {
            successor.handleRequest(request);
        }
    }
}

class VP implements Handler {
    Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    @Override
    public void handleRequest(Request request) {
        if (request.requestType == RequestType.PURCHASE) {
            System.out.println("VP can approve this");
        } else {
            successor.handleRequest(request);
        }
    }
}

class CEO implements Handler {
    Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    @Override
    public void handleRequest(Request request) {
        System.out.println("CEO can approve this");
    }
}

public class ChainOfResponsibilityDemo {
    public static void main(String[] args) {
        Director d = new Director();
        VP v = new VP();
        CEO c = new CEO();

        d.setSuccessor(v);
        v.setSuccessor(c);

        Request r = new Request(RequestType.CONFERENCE, 500);
        d.handleRequest(r);

        r = new Request(RequestType.PURCHASE, 2000);
        d.handleRequest(r);

        r = new Request(RequestType.SALE, 50000);
        d.handleRequest(r);
    }
}
