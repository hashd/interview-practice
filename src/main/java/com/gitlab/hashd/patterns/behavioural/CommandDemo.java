package com.gitlab.hashd.patterns.behavioural;

/**
 * - Object per Command
 * - Encapsulates action
 * - Class contains the what
 */
interface Command {
    void execute();
}

class Light {
    boolean isOn;

    public void toggle() {
        if (isOn) {
            off();
        } else {
            on();
        }
        isOn = !isOn;
    }

    public void on() {
        System.out.println("Light is switched on");
    }

    public void off() {
        System.out.println("Light is switched off");
    }
}

class Switch {
    Command command;

    public void store(Command command) {
        this.command = command;
    }

    public void execute() {
        this.command.execute();
    }
}

class ToggleCommand implements Command {
    Light light;

    public ToggleCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        this.light.toggle();
    }
}

public class CommandDemo {
    public static void main(String[] args) {
        Light light = new Light();
        Switch sw = new Switch();

        Command command = new ToggleCommand(light);
        sw.store(command);
        sw.execute();
        sw.execute();
        sw.execute();
    }
}
