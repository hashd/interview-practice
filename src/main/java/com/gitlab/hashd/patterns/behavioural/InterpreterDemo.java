package com.gitlab.hashd.patterns.behavioural;

import java.util.StringTokenizer;

/**
 * - Class per Rule
 */

interface Expression {
    boolean interpret(String context);
}

class TerminalExpression implements Expression {
    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpret(String str) {
        StringTokenizer st = new StringTokenizer(str);
        while (st.hasMoreTokens()) {
            String test = st.nextToken();
            if (test.equals(data)) {
                return true;
            }
        }
        return false;
    }
}

class OrExpression implements Expression {
    Expression expr1 = null;
    Expression expr2 = null;

    public OrExpression(Expression expr1, Expression expr2) {
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public boolean interpret(String context) {
        return expr1.interpret(context) || expr2.interpret(context);
    }
}

class AndExpression implements Expression {
    Expression expr1 = null;
    Expression expr2 = null;

    public AndExpression(Expression expr1, Expression expr2) {
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public boolean interpret(String context) {
        return expr1.interpret(context) && expr2.interpret(context);
    }
}

public class InterpreterDemo {
    static Expression buildExpressionTree() {
        Expression terminal1 = new TerminalExpression("Lions");
        Expression terminal2 = new TerminalExpression("Tigers");
        Expression terminal3 = new TerminalExpression("Bears");

        Expression alt1 = new AndExpression(terminal2, terminal3);
        Expression alt2 = new OrExpression(terminal1, alt1);

        return new AndExpression(terminal3, alt2);
    }

    public static void main(String[] args) {
        String context = "Lions Tigers";
        Expression define = buildExpressionTree();

        System.out.println(context + " is " + define.interpret(context));
    }
}
