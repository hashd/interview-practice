package com.gitlab.hashd.patterns.behavioural;

import java.util.Iterator;

/**
 * - Traverse a Container
 * - Doesn't expose underlying structure
 * - Decouples algorithm
 * - Sequential
 */
class BikeRepository implements Iterable<String> {
    String[] bikes = new String[10];
    int index;

    public void addBike(String bike) {
        bikes[index++] = bike;
    }

    @Override
    public Iterator<String> iterator() {
        Iterator<String> it = new Iterator<String>() {
            private int currentIdx = 0;

            @Override
            public boolean hasNext() {
                return currentIdx < bikes.length && bikes[currentIdx] != null;
            }

            @Override
            public String next() {
                return bikes[currentIdx++];
            }
        };

        return it;
    }
}

public class IteratorDemo {
    public static void main(String[] args) {
        BikeRepository repo = new BikeRepository();
        repo.addBike("Honda");
        repo.addBike("Hero");
        repo.addBike("Suzuki");
        repo.addBike("Yamaha");

        for (String bike: repo) {
            System.out.println(bike);
        }
    }
}
