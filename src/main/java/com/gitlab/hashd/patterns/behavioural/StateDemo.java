package com.gitlab.hashd.patterns.behavioural;

interface State {
    void handleRequest();
}

class FanOffState implements State {
    Fan fan;

    public FanOffState(Fan fan) {
        this.fan = fan;
    }

    @Override
    public void handleRequest() {
        System.out.println("Fan is being put in low mode");
        this.fan.setState(new FanLowState(this.fan));
    }
}

class FanLowState implements State {
    Fan fan;

    public FanLowState(Fan fan) {
        this.fan = fan;
    }

    @Override
    public void handleRequest() {
        System.out.println("Fan is being put in med mode");
        this.fan.setState(new FanMedState(this.fan));
    }
}

class FanMedState implements State {
    Fan fan;

    public FanMedState(Fan fan) {
        this.fan = fan;
    }

    @Override
    public void handleRequest() {
        System.out.println("Fan is being put in off mode");
        this.fan.setState(new FanOffState(this.fan));
    }
}

class Fan {
    State state;

    public Fan() {
        state = new FanOffState(this);
    }

    public void setState(State state) {
        this.state = state;
    }

    public void toggle() {
        this.state.handleRequest();
    }
}

public class StateDemo {
    public static void main(String[] args) {
        Fan fan = new Fan();
        fan.toggle();
        fan.toggle();
        fan.toggle();
    }
}
