package com.gitlab.hashd.patterns.behavioural;

/**
 * - Eliminate conditional statements
 * - Behaviour encapsulated in classes
 * - Strategies are independent
 * - Client is aware of strategies
 */
abstract class ValidationStrategy {
    abstract boolean isValid(CreditCard card);

    protected boolean passesLuhn(String creditCardNumber) {
        return true;
    }
}

class AmexStrategy extends ValidationStrategy {
    @Override
    boolean isValid(CreditCard card) {
        return passesLuhn(card.number);
    }
}

class VisaStrategy extends ValidationStrategy {
    @Override
    boolean isValid(CreditCard card) {
        return passesLuhn(card.number);
    }
}

class MasterCardStrategy extends ValidationStrategy {
    @Override
    boolean isValid(CreditCard card) {
        return passesLuhn(card.number);
    }
}

class CreditCard {
    ValidationStrategy strategy;

    String number;
    String date;
    String cvvNumber;

    public CreditCard(ValidationStrategy strategy) {
        this.strategy = strategy;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCvvNumber() {
        return cvvNumber;
    }

    public void setCvvNumber(String cvvNumber) {
        this.cvvNumber = cvvNumber;
    }

    public boolean isValid() {
        return strategy.isValid(this);
    }
}

public class StrategyDemo {
    public static void main(String[] args) {
        CreditCard card = new CreditCard(new AmexStrategy());
        card.cvvNumber = "111";
        card.date = "20171106";
        card.number = "123456789101112";

        System.out.println("Is valid Amex card: " + card.isValid());
    }
}
