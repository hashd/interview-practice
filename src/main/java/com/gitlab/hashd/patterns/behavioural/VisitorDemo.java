package com.gitlab.hashd.patterns.behavioural;

import java.util.LinkedList;
import java.util.List;

interface AtvPart {
    void accept(AtvPartVisitor visitor);
}

class Wheel implements AtvPart {

    @Override
    public void accept(AtvPartVisitor visitor) {
        visitor.visit(this);
    }
}

class Fender implements AtvPart {

    @Override
    public void accept(AtvPartVisitor visitor) {
        visitor.visit(this);
    }
}

class Oil implements AtvPart {

    @Override
    public void accept(AtvPartVisitor visitor) {
        visitor.visit(this);
    }
}

interface AtvPartVisitor {
    void visit(Fender fender);
    void visit(Oil oil);
    void visit(Wheel wheel);
    void visit(PartsOrder partsOrder);
}

class PartsOrder {
    List<AtvPart> parts;

    public PartsOrder() {
        this.parts = new LinkedList<>();
    }

    void add(AtvPart part) {
        this.parts.add(part);
    }

    void accept(AtvPartVisitor visitor) {
        for (AtvPart part: parts) {
            part.accept(visitor);
        }
        visitor.visit(this);
    }
}

class AtvPartsShippingVisitor implements AtvPartVisitor {

    @Override
    public void visit(Fender fender) {
        System.out.println("Visited fender");
    }

    @Override
    public void visit(Oil oil) {
        System.out.println("Visited oil");
    }

    @Override
    public void visit(Wheel wheel) {
        System.out.println("Visited wheel");
    }

    @Override
    public void visit(PartsOrder partsOrder) {
        System.out.println("Visited partsOrder");
    }
}

public class VisitorDemo {
    public static void main(String[] args) {
        PartsOrder partsOrder = new PartsOrder();
        partsOrder.add(new Wheel());
        partsOrder.add(new Fender());
        partsOrder.add(new Oil());

        partsOrder.accept(new AtvPartsShippingVisitor());
    }
}
