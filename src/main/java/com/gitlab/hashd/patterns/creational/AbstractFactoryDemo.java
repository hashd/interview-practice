package com.gitlab.hashd.patterns.creational;

enum Manufacturer {
    BMW, AUDI, HONDA
}

enum Model {
    X1, X2, X3, A1, A2, A3, CITY, ACCORD, JAZZ
}

interface Car {}

class X1 implements Car {}
class X2 implements Car {}
class X3 implements Car {}
class A1 implements Car {}
class A2 implements Car {}
class A3 implements Car {}
class City implements Car {}
class Accord implements Car {}
class Jazz implements Car {}

abstract class CarFactory {
    static CarFactory getFactory(Manufacturer manufacturer) {
        switch (manufacturer) {
            case BMW:   return new BMWCarFactory();
            case AUDI:  return new AudiCarFactory();
            case HONDA: return new HondaCarFactory();
            default:
                return null;
        }
    }

    abstract Car manufacture(Model model);
}

class BMWCarFactory extends CarFactory {

    @Override
    Car manufacture(Model model) {
        switch(model) {
            case X1: return new X1();
            case X2: return new X2();
            case X3: return new X3();
            default: return null;
        }
    }
}

class AudiCarFactory extends CarFactory {

    @Override
    Car manufacture(Model model) {
        switch(model) {
            case A1: return new A1();
            case A2: return new A2();
            case A3: return new A3();
            default: return null;
        }
    }
}

class HondaCarFactory extends CarFactory {

    @Override
    Car manufacture(Model model) {
        switch(model) {
            case CITY:      return new City();
            case ACCORD:    return new Accord();
            case JAZZ:      return new Jazz();
            default:        return null;
        }
    }
}

public class AbstractFactoryDemo {
    public static void main(String[] args) {
        CarFactory bmwFactory = CarFactory.getFactory(Manufacturer.BMW);
        CarFactory audiFactory = CarFactory.getFactory(Manufacturer.AUDI);
        CarFactory hondaFactory = CarFactory.getFactory(Manufacturer.HONDA);

        System.out.println(bmwFactory.manufacture(Model.X1));
        System.out.println(audiFactory.manufacture(Model.A1));
        System.out.println(hondaFactory.manufacture(Model.CITY));
    }
}
