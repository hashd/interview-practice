package com.gitlab.hashd.patterns.creational;

import com.gitlab.hashd.patterns.creational.Pizza.PizzaBuilder;

class Pizza {
    String base;
    String sauce;
    String cheese;
    String toppings;

    public Pizza(PizzaBuilder pizzaBuilder) {
        this.base = pizzaBuilder.base;
        this.sauce = pizzaBuilder.sauce;
        this.cheese = pizzaBuilder.cheese;
        this.toppings = pizzaBuilder.toppings;
    }

    public String getBase() {
        return base;
    }

    public String getSauce() {
        return sauce;
    }

    public String getCheese() {
        return cheese;
    }

    public String getToppings() {
        return toppings;
    }

    @Override
    public String toString() {
        return "Pizza [\n" +
                "\tBase: " + base + "\n" +
                "\tSauce: " + sauce + "\n" +
                "\tCheese: " + cheese + "\n" +
                "\tToppings: " + toppings + "\n" +
                "]\n";
    }

    static class PizzaBuilder {
        String base;
        String sauce;
        String cheese;
        String toppings;

        PizzaBuilder base(String base) {
            this.base = base;
            return this;
        }

        PizzaBuilder sauce(String sauce) {
            this.sauce = sauce;
            return this;
        }

        PizzaBuilder cheese(String cheese) {
            this.cheese = cheese;
            return this;
        }

        PizzaBuilder toppings(String toppings) {
            this.toppings = toppings;
            return this;
        }

        Pizza build() {
            return new Pizza(this);
        }
    }
}

public class BuilderDemo {
    public static void main(String[] args) {
        Pizza pizza = new PizzaBuilder()
            .base("Thin Crust")
            .cheese("Mayoineese")
            .sauce("Tomato")
            .toppings("Cottage Cheese")
            .build();

        System.out.println(pizza);
    }
}
