package com.gitlab.hashd.patterns.creational;

import static com.gitlab.hashd.patterns.creational.FruitType.*;

enum FruitType {
    ORANGE, MANGO, APPLE
}

abstract class Fruit {

}

class Orange extends Fruit {
    public String toString() {
        return "Orange";
    }
}

class Mango extends Fruit {
    public String toString() {
        return "Mango";
    }
}

class Apple extends Fruit {
    public String toString() {
        return "Apple";
    }
}

class FruitFactory {
    static Fruit getFruit(FruitType type) {
        switch (type) {
            case ORANGE: return new Orange();
            case MANGO: return new Mango();
            case APPLE: return new Apple();
            default:
                return null;
        }
    }
}

public class FactoryDemo {
    public static void main(String[] args) {
        Fruit a = FruitFactory.getFruit(ORANGE);
        Fruit b = FruitFactory.getFruit(MANGO);
        Fruit c = FruitFactory.getFruit(APPLE);

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
