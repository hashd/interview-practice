package com.gitlab.hashd.patterns.creational;

/**
 * - Avoids costly creation
 * - Avoids subclassing
 * - Typically doesn't use 'new'
 * - Often utilizes an Interface
 * - Usually implemented with a registry
 */
abstract class Item implements Cloneable {
    String title;
    double price;
    String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Movie extends Item {
    int runtime;

    public Movie(String title, int runtime) {
        this.title = title;
        this.runtime = runtime;
    }
}

public class PrototypeDemo {
}
