package com.gitlab.hashd.patterns.creational;

/**
 * - Only one instance created
 * - Guarantees control of a resource
 */
class EagerDbSingleton {
    private static EagerDbSingleton instance = new EagerDbSingleton();

    private EagerDbSingleton() {

    }

    static EagerDbSingleton getInstance() {
        return instance;
    }
}

class LazySyncDbSingleton {
    private static LazySyncDbSingleton instance = null;

    private LazySyncDbSingleton() {

    }

    static synchronized LazySyncDbSingleton getInstance() {
        if (instance == null) {
            instance = new LazySyncDbSingleton();
        }
        return instance;
    }
}

class DbSingleton {
    private static DbSingleton instance = null;

    private DbSingleton() {

    }

    static DbSingleton getInstance() {
        if (instance == null) {
            synchronized (DbSingleton.class) {
                if (instance == null) {
                    instance = new DbSingleton();
                }
            }
        }
        return instance;
    }
}

public class SingletonDemo {
    public static void main(String[] args) {
        DbSingleton dbSingleton1 = DbSingleton.getInstance();
        DbSingleton dbSingleton2 = DbSingleton.getInstance();

        System.out.println(dbSingleton1 == dbSingleton2);
    }
}
