package com.gitlab.hashd.queues;

import com.gitlab.hashd.utils.Tuple;

import java.util.LinkedList;
import java.util.Queue;

public class AnimalShelter implements QueueInterface {
    private Queue<Tuple> oddQueue = new LinkedList<>();
    private Queue<Tuple> evenQueue = new LinkedList<>();
    private int size = 0;

    @Override
    public void enqueue(int num) {
        if (num % 2 == 0) {
            evenQueue.add(new Tuple(size, num));
        } else {
            oddQueue.add(new Tuple(size, num));
        }
        size++;
    }

    @Override
    public int dequeue() {
        return evenQueue.peek().getA() < oddQueue.peek().getA()? evenQueue.poll().getB(): oddQueue.peek().getB();
    }

    public int dequeueOdd() {
        return oddQueue.poll().getB();
    }

    public int dequeueEven() {
        return evenQueue.poll().getB();
    }

    @Override
    public int size() {
        return evenQueue.size() + oddQueue.size();
    }

    @Override
    public int peek() {
        return evenQueue.peek().getA() < oddQueue.peek().getA()? evenQueue.peek().getB(): oddQueue.peek().getB();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
}
