package com.gitlab.hashd.queues;

public interface QueueInterface {
    void enqueue(int num);

    int dequeue();

    int size();

    int peek();

    boolean isEmpty();
}
