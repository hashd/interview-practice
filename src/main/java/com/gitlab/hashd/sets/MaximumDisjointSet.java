package com.gitlab.hashd.sets;

public class MaximumDisjointSet {
    public int getMaxDisjointSize(char[][] elements, char occupied, char vacant) {
        if (elements == null || elements.length == 0) return 0;

        int rows = elements.length, cols = elements[0].length;
        boolean[][] visited = new boolean[rows][cols];

        int max = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (!visited[i][j] && elements[i][j] == occupied) {
                    max = Math.max(max, traverse(elements, visited, vacant, i, j));
                }
            }
        }

        return max;
    }

    private int traverse(char[][] elements, boolean[][] visited, char vacant, int currRow, int currCol) {
        if (currRow < 0 || currRow >= elements.length) return 0;
        if (currCol < 0 || currCol >= elements[0].length) return 0;
        if (elements[currRow][currCol] == vacant) return 0;
        if (visited[currRow][currCol]) return 0;

        visited[currRow][currCol] = true;

        return 1 +
            traverse(elements, visited, vacant, currRow + 1, currCol) +
            traverse(elements, visited, vacant, currRow, currCol + 1) +
            traverse(elements, visited, vacant, currRow - 1, currCol) +
            traverse(elements, visited, vacant, currRow, currCol - 1);
    }

    public int getMaxDisjointSize(char[][] elements) {
        return getMaxDisjointSize(elements, 'x', '.');
    }
}
