package com.gitlab.hashd.sets;

import java.util.Arrays;

public class PowerSet {
    public void generate(String str) {
        boolean[] flags = new boolean[str.length()];
        char[] chars = str.toCharArray();
        Arrays.sort(chars);

        print("", 0, chars);
    }

    public void print(String subsetStr, int pos, char[] charSet) {
        if (!subsetStr.equals("") && !subsetStr.endsWith("0")) {
            System.out.println(subsetStr);
        }

        if (pos >= charSet.length) {
            return;
        }

        print(subsetStr + "1", pos + 1, charSet);
        print(subsetStr + "0", pos + 1, charSet);
    }

    public static void main(String[] args) {
        new PowerSet().generate("abcd");
    }
}
