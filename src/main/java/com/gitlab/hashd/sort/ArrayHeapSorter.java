package com.gitlab.hashd.sort;

import com.gitlab.hashd.heaps.Heap;
import com.gitlab.hashd.heaps.MinHeap;

public class ArrayHeapSorter {
    public static int[] heapSort(int[] arr) {
        // Create a Min-Heap from all elements in Array
        Heap heap = new MinHeap();
        for (int num: arr) {
            heap.push(num);
        }

        // Add every element popped to a new array
        int[] sorted = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            sorted[i] = heap.pop();
        }
        return sorted;
    }
}
