package com.gitlab.hashd.sort;

public class ArrayInsertionSorter {
    public void insertionSort(int[] arr) {
        int length = arr.length;
        for (int i = 1; i < length; i++) {
            int j = i, curr = arr[i];
            while (j > 0 && curr < arr[j - 1]) {
                j--;
            }
            for (int k = i; k > j; k--) {
                arr[k] = arr[k - 1];
            }
            arr[j] = curr;
        }
    }
}
