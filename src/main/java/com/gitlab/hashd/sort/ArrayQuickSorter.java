package com.gitlab.hashd.sort;

import java.util.Random;

import static com.gitlab.hashd.ArrayProblems.swap;

public class ArrayQuickSorter {
    private void quickSort(int[] arr, int begin, int end) {
        if (begin >= end - 1) {
            return;
        }

        Random rand = new Random();
        int pivotIdx = rand.nextInt(end - begin) + begin;
        int left = begin, right = end - 1, pivot = arr[pivotIdx];
        arr[pivotIdx] = arr[begin];

        while (left < right) {
            if (arr[left] <= pivot) {
                left++;
            } else {
                swap(arr, left, right);
                right--;
            }
        }
        arr[left] = pivot;
        // System.out.printf("%d:%s\n", pivot, Arrays.stream(arr).mapToObj(Integer::toString).reduce("", (String acc, String val) -> acc + val + " "));

        quickSort(arr, begin, left);
        quickSort(arr, left, end);
    }

    public void quickSort(int[] arr) {
        quickSort(arr, 0, arr.length);
    }
}
