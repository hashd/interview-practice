package com.gitlab.hashd.sort;

public class ArraySelectionSorter {
    public void selectionSort(int[] arr) {
        int min = 0, minIndex = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            min = arr[i]; minIndex = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j]; minIndex = j;
                }
            }

            arr[minIndex] = arr[i]; arr[i] = min;
        }
    }
}
