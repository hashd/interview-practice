package com.gitlab.hashd.sort;

import com.gitlab.hashd.ArrayProblems;

public class BinaryArraySorter {
    public void sortBinaryArray(int[] arr) {
        int left = 0, right = arr.length - 1, tmp = 0;
        while (left < right) {
            if (arr[left] == 1) {
                ArrayProblems.swap(arr, left, right);
                right = right - 1;
            } else {
                left = left + 1;
            }
        }
    }
}
