package com.gitlab.hashd.sort;

import com.gitlab.hashd.utils.Tuple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MultiArraySorter {
    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 4, 3},
                {-1, -2, 3, 2},
                {-5, 3, -8, 10}
        };

        MultiArraySorter mas = new MultiArraySorter();
        int[] sorted = mas.sort(array);

        System.out.println(Arrays.toString(sorted));
    }

    public int[] sort(int[][] arrays) {
        PriorityQueue<Tuple> minHeap = new PriorityQueue<>(Comparator.comparingInt(Tuple::getB));

        int size = 0;
        for (int i = 0; i < arrays.length; i++) {
            size = size + arrays[i].length;
            Arrays.sort(arrays[i]);
            minHeap.add(new Tuple(i, arrays[i][0]));
        }
        int[] result = new int[size];
        int[] pivot = new int[arrays.length];


        int count = 0, subArrayIndex;
        while (count < size) {
            Tuple t = minHeap.poll();
            subArrayIndex = t.getA();
            result[count] = t.getB();

            count++;
            pivot[subArrayIndex]++;
            if (pivot[subArrayIndex] < arrays[subArrayIndex].length) {
                minHeap.add(new Tuple(subArrayIndex, arrays[subArrayIndex][pivot[subArrayIndex]]));
            }
        }

        return result;
    }
}
