package com.gitlab.hashd.stacks;

import java.util.Stack;

public class HistogramAreaLargestCalculator {
    public int getLargestArea(int[] arr) {
        Stack<Integer> stack = new Stack<>();
        stack.push(0);

        int i, area = 0, maxArea = 0;
        for (i = 1; i < arr.length;) {
            if (stack.isEmpty() || arr[stack.peek()] <= arr[i]) {
                stack.add(i);
                i = i + 1;
            } else {
                int top = stack.pop();
                if (stack.isEmpty()) {
                    area = arr[top] * i;
                } else {
                    area = arr[top] * (i - stack.peek() - 1);
                }
                maxArea = Math.max(area, maxArea);
            }
        }

        while (!stack.isEmpty()) {
            int top = stack.pop();
            if (stack.isEmpty()) {
                area = arr[top] * i;
            } else {
                area = arr[top] * (i - stack.peek() - 1);
            }
            maxArea = Math.max(area, maxArea);
        }

        return maxArea;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 1, 2};
        System.out.println(new HistogramAreaLargestCalculator().getLargestArea(arr));
    }
}
