package com.gitlab.hashd.stacks;

import java.util.Arrays;
import java.util.Stack;

public class LargerElementInRightFinder {
    public int[] findLargerElementToRight(int[] arr) {
        if (arr == null || arr.length == 0) return null;

        int[] largerToRight = new int[arr.length];
        Arrays.fill(largerToRight, -1);

        Stack<Integer> positions = new Stack<>();
        positions.push(0);

        for (int i = 1; i < arr.length; i++) {
            while (!positions.isEmpty() && arr[i] > arr[positions.peek()]) {
                largerToRight[positions.pop()] = arr[i];
            }
            positions.push(i);
        }

        return largerToRight;
    }

    public static void main(String[] args) {
        int[] arr1 = {4, 5, 2, 25};
        int[] arr2 = {13, 7, 6, 12};

        System.out.println(Arrays.toString(new LargerElementInRightFinder().findLargerElementToRight(arr1)));
        System.out.println(Arrays.toString(new LargerElementInRightFinder().findLargerElementToRight(arr2)));
    }
}
