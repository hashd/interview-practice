package com.gitlab.hashd.stacks;

public class LargestRectangleOfOnes {
    public int getLargestRectangleContainingOnes(int[][] matrix) {
        if (matrix == null || matrix.length == 0) return -1;
        int rows = matrix.length, cols = matrix[0].length, maxArea = 0;

        int[][] onesToRight = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            int ones = 0;
            for (int j = cols - 1; j >= 0; j--) {
                ones = (matrix[i][j] == 1)? ones + 1: 0;
                onesToRight[i][j] = ones;
            }
        }

        HistogramAreaLargestCalculator halc = new HistogramAreaLargestCalculator();
        int[] heights = new int[rows];
        for (int j = 0; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                heights[i] = onesToRight[i][j];
            }
            maxArea = Math.max(maxArea, halc.getLargestArea(heights));
        }

        return maxArea;
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 0, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 0, 1},
                {1, 0, 0, 1}
        };

        LargestRectangleOfOnes lroo = new LargestRectangleOfOnes();
        System.out.println(lroo.getLargestRectangleContainingOnes(matrix));
    }
}
