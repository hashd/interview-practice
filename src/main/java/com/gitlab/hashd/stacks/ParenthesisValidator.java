package com.gitlab.hashd.stacks;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ParenthesisValidator {
    public static Map<Character, Character> parenthesisMap = new HashMap<Character, Character>();

    static {
        parenthesisMap.put('(', ')');
        parenthesisMap.put('{', '}');
        parenthesisMap.put('[', ']');
    }

    public boolean isValid(String str) {
        if (str == null) return true;
        int length = str.length();

        Stack<Character> parenthesisStack = new Stack<>();
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            switch (c) {
                case '(':
                case '{':
                case '[':
                    parenthesisStack.push(c);
                    break;
                case ')':
                case '}':
                case ']':
                    char top = parenthesisStack.pop();
                    if (parenthesisMap.get(top) != c) {
                        return false;
                    }
                    break;
            }
        }

        return true;
    }
}
