package com.gitlab.hashd.stacks;

import java.util.Stack;

public class QueueUsingSingleStack<E> {
    private Stack<E> stack = new Stack<>();

    public void enqueue(E data) {
        stack.push(data);
    }

    public E dequeue() {
        return QueueUsingSingleStack.dequeue(stack);
    }

    public int size() {
        return stack.size();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    private static<E> E dequeue(Stack<E> stack) {
        if (stack.size() == 0) {
            return null;
        }

        if (stack.size() == 1) {
            return stack.pop();
        }

        E data = stack.pop();
        E returnValue = dequeue(stack);
        stack.push(data);
        return returnValue;
    }
}
