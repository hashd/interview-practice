package com.gitlab.hashd.stacks;

import com.gitlab.hashd.queues.QueueInterface;

import java.util.Stack;

public class QueueUsingTwoStacks implements QueueInterface {
    private Stack<Integer> numbers;
    private Stack<Integer> tmpStack;

    public QueueUsingTwoStacks() {
        numbers = new Stack<>();
        tmpStack = new Stack<>();
    }

    @Override
    public void enqueue(int num) {
        numbers.push(num);
    }

    @Override
    public int dequeue() {
        while (!numbers.isEmpty()) {
            tmpStack.push(numbers.pop());
        }
        int value = tmpStack.pop();
        while (!tmpStack.isEmpty()) {
            numbers.push(tmpStack.pop());
        }
        return value;
    }

    @Override
    public int size() {
        return numbers.size();
    }

    @Override
    public int peek() {
        while (!numbers.isEmpty()) {
            tmpStack.push(numbers.pop());
        }
        int value = tmpStack.peek();
        while (!tmpStack.isEmpty()) {
            numbers.push(tmpStack.pop());
        }
        return value;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
}
