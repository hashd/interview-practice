package com.gitlab.hashd.stacks;

import java.util.Stack;

public class SortStack implements StackInterface {
    private Stack<Integer> numbers;
    private Stack<Integer> tmp;

    public SortStack() {
        numbers = new Stack<>();
        tmp = new Stack<>();
    }

    @Override
    public int pop() {
        return numbers.pop();
    }

    @Override
    public int peek() {
        return numbers.peek();
    }

    @Override
    public void push(int num) {
        while (!numbers.isEmpty() && numbers.peek() < num) {
            tmp.push(numbers.pop());
        }
        numbers.push(num);
        while (!tmp.isEmpty()) {
            numbers.push(tmp.pop());
        }
    }

    @Override
    public int size() {
        return numbers.size();
    }

    @Override
    public boolean isEmpty() {
        return numbers.isEmpty();
    }
}
