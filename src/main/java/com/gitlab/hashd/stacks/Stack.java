package com.gitlab.hashd.stacks;

class LinkedList {
    int        data;
    LinkedList next;

    public LinkedList(int d) {
        data = d;
    }
}

public class Stack {
    private LinkedList list;
    private int        length;

    public Stack() {
    }

    public void push(int data) {
        LinkedList node = new LinkedList(data);
        node.next = list;
        length = length + 1;

        list = node;
    }

    public int peek() {
        if (length > 0) {
            return list.data;
        }
        return -1;
    }

    public int pop() {
        if (length > 0) {
            LinkedList pivot = list;
            list = pivot.next;
            length = length - 1;

            return pivot.data;
        }
        return -1;
    }

    public int size() {
        return length;
    }
}
