package com.gitlab.hashd.stacks;

public interface StackInterface {
    int pop();

    int peek();

    void push(int num);

    int size();

    boolean isEmpty();
}
