package com.gitlab.hashd.stacks;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StackOfPlates {
    private List<Stack<Integer>> stackList;
    private int limit;

    public StackOfPlates(int limit) {
        stackList = new ArrayList<>();
        stackList.add(new Stack<>());
        this.limit = limit;
    }

    public int pop() {
        int nStacks = stackList.size();
        int value = stackList.get(nStacks - 1).pop();
        if (stackList.get(nStacks - 1).size() == 0) {
            stackList.remove(nStacks - 1);
        }
        return value;
    }

    public int peek() {
        return stackList.get(stackList.size() - 1).peek();
    }

    public void push(int num) {
        int nStacks = stackList.size();
        stackList.get(nStacks - 1).push(num);
        if (stackList.get(nStacks - 1).size() == limit) {
            stackList.add(new Stack<>());
        }
    }

    public int size() {
        int size = 0;
        for (Stack<Integer> st: stackList) {
            size = size + st.size();
        }
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }
}
