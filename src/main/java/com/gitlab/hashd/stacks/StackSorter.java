package com.gitlab.hashd.stacks;

import java.util.LinkedList;
import java.util.Stack;

public class StackSorter {
    public void sort(Stack<Integer> stack) {
        if (stack != null && stack.size() == 1) return;

        Stack<Integer> tmpstack = new Stack<>();
        while (!stack.isEmpty()) {
            tmpstack.push(stack.pop());
        }

        while (!tmpstack.isEmpty()) {
            int min = tmpstack.pop();
            while (!tmpstack.isEmpty()) {
                int value = tmpstack.pop();
                if (value < min) {
                    stack.push(min);
                    min = value;
                } else {
                    stack.push(value);
                }
            }

            while (!stack.isEmpty()) {
                tmpstack.push(stack.pop());
            }

            stack.push(min);
        }
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(3);
        stack.push(4);
        stack.push(1);
        stack.push(6);
        stack.push(2);

        StackSorter ss = new StackSorter();
        ss.sort(stack);

        System.out.println(new LinkedList<>(stack));
    }
}
