package com.gitlab.hashd.stacks;

import java.util.Stack;

public class StackWithMinimum {
    private Stack<Integer> numStack;
    private Stack<Integer> minStack;

    public StackWithMinimum() {
        numStack = new Stack<>();
        minStack = new Stack<>();
    }

    public int pop() {
        int value = numStack.pop();
        if (value == minStack.peek()) {
            minStack.pop();
        }
        return value;
    }

    public int peek() {
        return numStack.peek();
    }

    public void push(int num) {
        numStack.push(num);
        if (num <= min()) {
            minStack.push(num);
        }
    }

    public int min() {
        if (minStack.isEmpty()) {
            return Integer.MAX_VALUE;
        }
        return minStack.peek();
    }

    public int size() {
        return numStack.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }
}
