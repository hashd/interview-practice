package com.gitlab.hashd.stacks;

public class ThreeStacksUsingArray {
    private int[] content;
    private int[] sizes = new int[3];
    private int size;

    public ThreeStacksUsingArray(int size) {
        content = new int[3 * size];
        this.size = size;
    }

    public int pop(int stackNum) {
        if (size(stackNum) != 0) {
            sizes[stackNum]--;
            return content[stackNum * size + sizes[stackNum]];
        }
        return -1;
    }

    public void push(int stackNum, int num) {
        if (size(stackNum) != size) {
            content[stackNum * size + sizes[stackNum]] = num;
            sizes[stackNum]++;
        }
    }

    public int size(int stackNum) {
        return sizes[stackNum];
    }

    public boolean isEmpty(int stackNum) {
        return sizes[stackNum] == 0;
    }
}
