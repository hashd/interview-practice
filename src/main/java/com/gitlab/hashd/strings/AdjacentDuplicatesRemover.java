package com.gitlab.hashd.strings;

import java.util.Stack;

public class AdjacentDuplicatesRemover {
    public String removeAdjacentDuplicates(String str) {
        if (str == null || str.length() <= 1) return str;

        char[] chars = str.toCharArray();
        char ch = chars[0];
        int pos = 1;
        for (int i = 1; i < chars.length; i++) {
            if (chars[i] != ch) {
                ch = chars[i];
                chars[pos++] = ch;
            }
        }

        return String.valueOf(chars, 0, pos);
    }

    public String removeAdjacentDuplicatesRecursively(String str) {
        if (str == null || str.length() <= 1) return str;

        Stack<Character> chStack = new Stack<>();
        chStack.push(str.charAt(0));
        for (int i = 1; i < str.length(); i++) {
            if (chStack.peek() == str.charAt(i)) {
                chStack.pop();
            } else {
                chStack.push(str.charAt(i));
            }
        }

        StringBuilder sb = new StringBuilder();
        while (!chStack.isEmpty()) {
            sb.append(chStack.pop());
        }
        return sb.reverse().toString();
    }

    public String removeAdjacentDuplicatesRecursivelyAlt(String str) {
        if (str == null || str.length() == 0) return "";
        if (str.length() == 1) return str;

        return (str.charAt(0) == str.charAt(1)? "": Character.toString(str.charAt(0))) + removeAdjacentDuplicatesRecursivelyAlt(str.substring(1));
    }

    public static void main(String[] args) {
        AdjacentDuplicatesRemover adr = new AdjacentDuplicatesRemover();
        System.out.println(adr.removeAdjacentDuplicatesRecursively("careermonk"));
        System.out.println(adr.removeAdjacentDuplicatesRecursively("mississippi"));
        System.out.println(adr.removeAdjacentDuplicatesRecursivelyAlt("careermonk"));
        System.out.println(adr.removeAdjacentDuplicatesRecursivelyAlt("mississippi"));
        System.out.println(adr.removeAdjacentDuplicates("mississippi"));
        System.out.println(adr.removeAdjacentDuplicates("careermonk"));
    }
}
