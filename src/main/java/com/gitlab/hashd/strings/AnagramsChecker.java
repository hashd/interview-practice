package com.gitlab.hashd.strings;

import java.util.Arrays;

/**
 * Given two strings, check whether two given strings are anagram of each other or not.
 * An anagram of a string is another string that contains same characters,
 * only the order of characters can be different.
 *
 * http://practice.geeksforgeeks.org/problems/anagram/0
 */
public class AnagramsChecker {
    public boolean areAnagrams(String a, String b) {
        if (a == null || b == null) return false;
        if (a.length() != b.length()) return false;

        char[] aChars = a.toCharArray(), bChars = b.toCharArray();
        Arrays.sort(aChars); Arrays.sort(bChars);
        for (int i = 0; i < a.length(); i++) {
            if (aChars[i] != bChars[i]) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        AnagramsChecker ac = new AnagramsChecker();
        System.out.println(ac.areAnagrams("rats", "arts"));
        System.out.println(ac.areAnagrams("rats", "artsy"));
    }
}
