package com.gitlab.hashd.strings;

import java.util.*;
import java.util.stream.Collectors;

public class AnagramsClassifier {
    public List<Set<String>> classify(String[] strings, boolean isCaseSensitive) {
        Map<String, Set<String>> anagramsMap = new HashMap<>();
        for (String str: strings) {
            char[] strChars = (isCaseSensitive ? str: str.toLowerCase()).toCharArray();
            Arrays.sort(strChars);
            String sortedString = String.valueOf(strChars);

            if (!anagramsMap.containsKey(sortedString)) {
                anagramsMap.put(sortedString, new HashSet<>());
            }
            anagramsMap.get(sortedString).add(str);
        }

        return anagramsMap.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public List<Set<String>> classify(String[] strings) {
        return classify(strings, false);
    }
}
