package com.gitlab.hashd.strings;

/*
 Implement atoi to convert a string to an integer.
 */
public class AsciiToIntegerConverter {
    public int toInteger(String str) {
        int number = 0, pivot = 0;
        boolean isNegative = false;

        String trimmedStr = str.trim();
        char[] chars = trimmedStr.toCharArray();
        isNegative = (chars[0] == '-');

        if (chars[0] == '-' || chars[0] == '+') pivot++;
        while (pivot < chars.length && chars[pivot] >= '0' && chars[pivot] <= '9') {
            int digit = chars[pivot++] - '0';
            if (Integer.MAX_VALUE / 10 < number) {
                return Integer.MAX_VALUE;
            }
            number = number * 10 + digit;
        }

        return (isNegative ? -1: 1) * number;
    }

    public static void main(String[] args) {
        AsciiToIntegerConverter atoi = new AsciiToIntegerConverter();
        System.out.println(atoi.toInteger("-345"));
        System.out.println(atoi.toInteger("  58219"));
    }
}
