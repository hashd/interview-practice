package com.gitlab.hashd.strings;

public class BinaryAdder {
    public String add(String b1, String b2) {
        if (b1 == null && b2 == null) return null;
        if (b1 == null || b2 == null) return b1 == null ? b2: b1;

        StringBuilder sb = new StringBuilder();
        int b1Length = b1.length(), b2Length = b2.length(), carry = 0;
        while ((b1Length > 0) || (b2Length > 0)) {
            char b1Ch = b1Length > 0? b1.charAt(b1Length - 1): '0', b2Ch = b2Length > 0? b2.charAt(b2Length - 1): '0';

            int addResult = (b1Ch - '0') + (b2Ch - '0') + carry;
            carry = addResult / 2;
            sb.append((char) ('0' + (addResult % 2)));

            b1Length--;
            b2Length--;
        }

        while (carry != 0) {
            sb.append(carry % 2);
            carry = carry / 2;
        }

        return sb.reverse().toString();
    }
}
