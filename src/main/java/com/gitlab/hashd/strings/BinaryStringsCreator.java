package com.gitlab.hashd.strings;

import java.util.LinkedList;
import java.util.List;

public class BinaryStringsCreator {
    public List<String> getBinaryStrings(int n) {
        List<String> binaryStrings = new LinkedList<>();
        addBinaryStrings("", n, binaryStrings);
        return binaryStrings;
    }

    private void addBinaryStrings(String s, int n, List<String> binaryStrings) {
        if (s.length() == n) {
            binaryStrings.add(s);
            return;
        }
        addBinaryStrings(s + "0", n, binaryStrings);
        addBinaryStrings(s + "1", n, binaryStrings);
    }

    public static void main(String[] args) {
        List<String> binaryStrings = new BinaryStringsCreator().getBinaryStrings(5);
        binaryStrings.forEach(System.out::println);
    }
}
