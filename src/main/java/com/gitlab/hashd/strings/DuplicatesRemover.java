package com.gitlab.hashd.strings;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Given a string, remove duplicates from it. Note that original order of characters must be kept same.
 * Expected time complexity O(n) where n is length of input string and extra space O(1) under the assumption
 * that there are total 256 possible characters in a string.
 *
 * http://practice.geeksforgeeks.org/problems/remove-duplicates/0
 */
public class DuplicatesRemover {
    public String removeDuplicates(String str) {
        if (str == null) return str;

        Set<Character> charset = new LinkedHashSet<>();
        for (char c: str.toCharArray()) {
            charset.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (char c: charset) {
            sb.append(c);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        DuplicatesRemover dr = new DuplicatesRemover();
        System.out.println(dr.removeDuplicates("hello"));
    }
}
