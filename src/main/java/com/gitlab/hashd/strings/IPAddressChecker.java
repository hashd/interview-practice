package com.gitlab.hashd.strings;

public class IPAddressChecker {
    public boolean check(String ip) {
        if (ip == null) return false;

        String[] ipParts = ip.split("\\.");
        if (ipParts.length != 4) return false;

        boolean isValid = true;
        for (String ipPart : ipParts) {
            int ipNum = Integer.parseInt(ipPart);
            isValid = isValid & (ipNum >= 0 && ipNum < 256);
        }

        return isValid;
    }
}
