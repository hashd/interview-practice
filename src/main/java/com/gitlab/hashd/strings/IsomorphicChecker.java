package com.gitlab.hashd.strings;

import java.util.HashMap;
import java.util.Map;

public class IsomorphicChecker {
    public boolean isIsomorphic(String a, String b) {
        if (a == null || b == null) return false;
        if (a.length() != b.length()) return false;

        Map<Character, Character> charMap = new HashMap<>();
        int length = a.length();

        for (int i = 0; i < length; i++) {
            char aCh = a.charAt(i), bCh = b.charAt(i);
            if (charMap.containsKey(aCh) && charMap.get(aCh) != bCh) {
                return false;
            }
            charMap.put(aCh, bCh);
        }

        return true;
    }

    public static void main(String[] args) {
        IsomorphicChecker ic = new IsomorphicChecker();
        System.out.println(ic.isIsomorphic("add", "egg"));
        System.out.println(ic.isIsomorphic("foo", "bar"));
    }
}
