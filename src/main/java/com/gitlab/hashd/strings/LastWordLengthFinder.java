package com.gitlab.hashd.strings;

/*
Given a string s consists of upper/lower-case alphabets and empty space characters ' ',
return the length of last word in the string. If the last word does not exist, return 0.
 */
public class LastWordLengthFinder {
    public int length(String str) {
        if (str == null) return 0;

        String[] words = str.split(" ");
        if (words.length == 0) return 0;

        return words[words.length - 1].length();
    }
}
