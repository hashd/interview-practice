package com.gitlab.hashd.strings;

/**
 * Given a string S, find the longest palindromic substring in S.
 *
 * http://practice.geeksforgeeks.org/problems/longest-palindrome-in-a-string/0
 */
public class LongestPalindromeSubstringFinder {
    public String getLongestPalindromeSubstring(String string) {
        char[] chars = string.toCharArray();

        int maxLength = 1, start = 0, end = 0;
        for (int i = 0; i < chars.length; i++) {
            int pivot = 1;
            while ((i - pivot >= 0 && i + pivot < chars.length) && (chars[i - pivot] == chars[i + pivot])) {
                pivot++;
            }

            int newLen = (pivot - 1) * 2 + 1;
            if (newLen > maxLength) {
                maxLength = newLen;
                start = i - pivot + 1;
                end = i + pivot - 1;
            }
        }

        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == chars[i + 1]) {
                int pivot = 1;
                while ((i - pivot >= 0 && i + 1 + pivot < chars.length) && (chars[i - pivot] == chars[i + 1 + pivot])) {
                    pivot++;
                }

                int newLen = (pivot - 1) * 2 + 2;
                if (newLen > maxLength) {
                    maxLength = newLen;
                    start = i - pivot + 1;
                    end = i + pivot;
                }
            }
        }

        return string.substring(start, end + 1);
    }

    public static void main(String[] args) {
        LongestPalindromeSubstringFinder lpsf = new LongestPalindromeSubstringFinder();
        System.out.println(lpsf.getLongestPalindromeSubstring("aaaabbaa"));
    }
}
