package com.gitlab.hashd.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string, find length of the longest substring with all distinct characters.
 * For example, for input "abca", the output is 3 as "abc" is the longest substring with all distinct characters.
 *
 * http://practice.geeksforgeeks.org/problems/longest-distinct-characters-in-string/0
 */
public class LongestSubstringWithoutRepeatingCharacter {
    public String longestSubstringWithoutRepeatingCharacter(String str) {
        if (str == null) return null;
        if (str.length() == 0) return str;

        int length = str.length(), pivot = 0, maxLength = 0;
        int maxSubstringStartIndex = 0, maxSubstringEndIndex = 0;
        Map<Character, Integer> charIndexMap = new HashMap<>();
        while (pivot < length) {
            char key = str.charAt(pivot);
            if (charIndexMap.containsKey(key)) {
                int startIndex = charIndexMap.get(key);
                int endIndex = pivot;
                if (endIndex - startIndex > maxLength) {
                    maxLength = endIndex - startIndex;
                    maxSubstringStartIndex = startIndex;
                    maxSubstringEndIndex = endIndex;
                }
            }
            charIndexMap.put(key, pivot);
            pivot++;
        }

        return str.substring(maxSubstringStartIndex, maxSubstringEndIndex);
    }
}
