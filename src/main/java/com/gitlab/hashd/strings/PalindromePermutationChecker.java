package com.gitlab.hashd.strings;

import java.util.HashMap;
import java.util.Map;

public class PalindromePermutationChecker {
    public boolean hasPalindromicPermutation(String str) {
        if (str == null) return false;

        int length = str.length();
        str = str.toLowerCase();
        Map<Character, Integer> occurrencesMap = new HashMap<>();
        for (int i = 0; i < length; i++) {
            char key = str.charAt(i);
            if (key == ' ') {
                continue;
            }

            if (!occurrencesMap.containsKey(key)) {
                occurrencesMap.put(key, 0);
            }
            occurrencesMap.put(key, occurrencesMap.get(key) + 1);
        }

        int oddOccurrences = 0;
        for (Map.Entry<Character, Integer> occurrences: occurrencesMap.entrySet()) {
            if (occurrences.getValue() % 2 == 1) {
                oddOccurrences = oddOccurrences + 1;
                if (oddOccurrences > 1) {
                    return false;
                }
            }
        }

        return true;
    }
}
