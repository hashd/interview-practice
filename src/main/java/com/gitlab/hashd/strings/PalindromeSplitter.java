package com.gitlab.hashd.strings;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class PalindromeSplitter {
    public List<List<String>> getPalindromicSplits(String str) {
        if (str == null) {
            return Collections.emptyList();
        }
        char[] chars = str.toCharArray();

        List<List<String>> palindromicSplits = new LinkedList<>();
        addToPalindromicSplits(chars, 0, new Stack<String>(), palindromicSplits);
        return palindromicSplits;
    }

    private void addToPalindromicSplits(char[] chars, int pos, Stack<String> stringsStack, List<List<String>> palindromicSplits) {
        if (pos == chars.length) {
            List<String> strings = new LinkedList<>(stringsStack);
            palindromicSplits.add(strings);
            return;
        }

        for (int i = pos; i < chars.length; i++) {
            if (isPalindrome(String.valueOf(chars, pos, i - pos + 1))) {
                stringsStack.add(String.valueOf(chars, pos, i - pos + 1));
                addToPalindromicSplits(chars, i + 1, stringsStack, palindromicSplits);
                stringsStack.pop();
            }
        }
    }

    private boolean isPalindrome(String s) {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (s.charAt(i) != s.charAt(len - 1 - i)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        PalindromeSplitter ps = new PalindromeSplitter();
        List<List<String>> palindromeSplits = ps.getPalindromicSplits("geeks");
        for (List<String> palindromeSplit: palindromeSplits) {
            palindromeSplit.forEach(System.out::println);
        }
    }
}
