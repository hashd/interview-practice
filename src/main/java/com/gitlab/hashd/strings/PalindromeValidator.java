package com.gitlab.hashd.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
 Given a string, determine if it is a palindrome, considering only
 alphanumeric characters and ignoring cases.
 */
public class PalindromeValidator {
    public boolean isPalindrome(String str) {
        if (str == null) return false;

        int length = str.length();
        List<Character> filtered = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            if (isAlphanumeric(str.charAt(i))) {
                filtered.add(toLowercase(str.charAt(i)));
            }
        }

        char[] chars = new char[filtered.size()];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = filtered.get(i);
        }

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != chars[chars.length - 1 - i]) {
                return false;
            }
        }

        return true;
    }

    private static char toLowercase(char ch) {
        return Character.toLowerCase(ch);
    }

    private static boolean isAlphanumeric(char ch) {
        return ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
    }
}
