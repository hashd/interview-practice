package com.gitlab.hashd.strings;

import com.gitlab.hashd.utils.Tuple;

import java.util.Stack;

public class ParenthesisLongestFinder {
    public int length(String str) {
        Stack<Tuple> tupleStack = new Stack<>();
        int result = 0, length = str.length();

        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            if (c == '(') {
                tupleStack.push(new Tuple(i, 0));
            } else {
                if (tupleStack.isEmpty() || tupleStack.peek().getB() == 1) {
                    tupleStack.push(new Tuple(i, 1));
                } else {
                    tupleStack.pop();

                    int currentLen = 0;
                    if (tupleStack.isEmpty()) {
                        currentLen = i + 1;
                    } else {
                        currentLen = i - tupleStack.peek().getA();
                    }
                    result = Math.max(currentLen, result);
                }
            }
        }

        return result;
    }
}
