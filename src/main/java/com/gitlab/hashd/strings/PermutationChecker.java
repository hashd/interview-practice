package com.gitlab.hashd.strings;

import java.util.Arrays;

public class PermutationChecker {
    public boolean check(String a, String b, boolean ignoreCase) {
        if (a == null || b == null) return false;
        if (a.length() != b.length()) return false;

        char[] aCh = ignoreCase ? a.toLowerCase().toCharArray(): a.toCharArray();
        char[] bCh = ignoreCase ? b.toLowerCase().toCharArray(): b.toCharArray();

        Arrays.sort(aCh);
        Arrays.sort(bCh);

        for (int i = 0; i < aCh.length; i++) {
            if (aCh[i] != bCh[i]) {
                return false;
            }
        }
        return true;
    }

    public boolean check(String a, String b) {
        return check(a, b, false);
    }
}
