package com.gitlab.hashd.strings;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Given a string, print all permutations of a given string.
 *
 * http://practice.geeksforgeeks.org/problems/permutations-of-a-given-string/0
 */
public class PermutationsGenerator {
    public List<String> getPermutations(String str) {
        List<String> permutations = new LinkedList<>();
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        char[] permChars = new char[chars.length];
        boolean[] flags = new boolean[chars.length];

        addToPermutations(chars, 0, flags, permChars, permutations);
        return permutations;
    }

    private void addToPermutations(char[] chars, int len, boolean[] isSelected, char[] permChars, List<String> permutations) {
        if (len == chars.length) {
            permutations.add(String.valueOf(permChars));
            return;
        }

        for (int i = 0; i < chars.length; i++) {
            if (!isSelected[i]) {
                permChars[len] = chars[i];
                isSelected[i] = true;
                addToPermutations(chars, len + 1, isSelected, permChars, permutations);
                isSelected[i] = false;
            }
        }
    }

    public static void main(String[] args) {
        PermutationsGenerator pg = new PermutationsGenerator();
        List<String> permutations = pg.getPermutations("colin");
        permutations.forEach(System.out::println);

        System.out.println(permutations.size());
    }
}
