package com.gitlab.hashd.strings;

public class RotatedStringsChecker {
    public static boolean areRotatedForms(String a, String b) {
        if (a == null || b == null) return false;
        if (a.length() != b.length()) return false;

        return (a + a).contains(b);
    }

    public static boolean isRotatedBy(String a, String b, int k) {
        if (a == null || b == null) return false;

        int aLen = a.length(), bLen = b.length();
        if (aLen != bLen) return false;

        for (int i = 0; i < aLen; i++) {
            if (a.charAt(i) != b.charAt((i + k + aLen) % aLen)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(RotatedStringsChecker.isRotatedBy("amazon", "onamaz",  2));
        System.out.println(RotatedStringsChecker.isRotatedBy("amazon", "azonam", -2));
    }
}
