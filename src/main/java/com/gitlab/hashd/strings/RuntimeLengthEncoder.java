package com.gitlab.hashd.strings;

public class RuntimeLengthEncoder {
    public static String encode(String str) {
        if (str == null) return null;
        if ("".equals(str)) return "";

        StringBuilder encoded = new StringBuilder();
        Character ch = str.charAt(0);
        int count = 1;

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) != ch) {
                encoded.append(ch + count == 1? "": count);
                ch = str.charAt(i);
                count = 0;
            } else {
                count = count + 1;
            }
        }

        return encoded.toString();
    }
}
