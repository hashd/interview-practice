package com.gitlab.hashd.strings;

public class StringReverser {
    public String reverse(String s) {
        if (s == null) return null;

        char[] chars = s.toCharArray();
        int left = 0, right = chars.length - 1;

        while (left < right) {
            swap(chars, left++, right--);
        }

        return new String(chars);
    }

    public static void swap(char[] chars, int idxA, int idxB) {
        char tmp = chars[idxA];
        chars[idxA] = chars[idxB];
        chars[idxB] = tmp;
    }
}
