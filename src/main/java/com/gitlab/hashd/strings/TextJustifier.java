package com.gitlab.hashd.strings;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TextJustifier {
    public List<String> justify(String[] words, int lineWidth) {
        if (words == null || words.length == 0) return Collections.emptyList();
        List<String> justifiedStrings = new LinkedList<>();

        int wordsLength = words.length, pivot = 0;
        while (pivot < wordsLength) {
            int currentLineLength = words[pivot].length(), pos = pivot + 1;
            for (; pos < wordsLength && currentLineLength + words[pos].length() < lineWidth; pos++) {
                currentLineLength = currentLineLength + words[pos].length() + 1;
            }

            int noOfWords = pos - pivot, gaps = noOfWords - 1;
            currentLineLength = currentLineLength - gaps;
            int availableWhitespaceLength = lineWidth - currentLineLength;

            String justifiedString = words[pivot++];
            if (gaps != 0) {
                int equalSpace = availableWhitespaceLength / gaps, extraSpace = availableWhitespaceLength - equalSpace * gaps;
                StringBuilder justifiedStringBuilder = new StringBuilder(justifiedString);
                while (pivot < pos) {
                    justifiedStringBuilder.append(new String(new char[equalSpace + (extraSpace > 0 ? 1 : 0)]).replace('\0', ' ')).append(words[pivot++]);
                    extraSpace = extraSpace - 1;
                }
                justifiedString = justifiedStringBuilder.toString();
            } else {
                justifiedString = justifiedString + (new String(new char[availableWhitespaceLength]).replace('\0', ' '));
            }
            justifiedStrings.add(justifiedString);
        }

        return justifiedStrings;
    }

    public static void main(String[] args) {
        TextJustifier tj = new TextJustifier();
        String[] words = {"This", "is", "an", "example", "of", "text", "justification."};

        List<String> lines = tj.justify(words, 16);
        lines.forEach(System.out::println);
    }
}