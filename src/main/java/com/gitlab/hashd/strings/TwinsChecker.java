package com.gitlab.hashd.strings;

import java.util.HashMap;

public class TwinsChecker {
    public boolean areTwins(String a, String b) {
        if (a == null || b == null) return false;
        if (a.length() != b.length()) return false;

        HashMap<Character, Integer> charMap = new HashMap<>();
        char[] aChs = a.toCharArray(), bChs = b.toCharArray();

        for (int i = 0; i < aChs.length; i++) {
            if (!charMap.containsKey(aChs[i])) {
                charMap.put(aChs[i], 0);
            }
            charMap.put(aChs[i], charMap.get(aChs[i]) + 1);
        }

        for (int i = 0; i < bChs.length; i++) {
            if (!charMap.containsKey(bChs[i]))
            charMap.put(bChs[i], charMap.get(bChs[i]) - 1);
        }

        return false;
    }
}
