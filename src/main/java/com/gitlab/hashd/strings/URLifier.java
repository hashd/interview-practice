package com.gitlab.hashd.strings;

public class URLifier {
    public void replaceSpaces(Character[] chars) {
        if (chars == null) return;

        int length = chars.length, nullPos = 0;
        for (nullPos = 0; nullPos < length; nullPos++) {
            if (chars[nullPos] == null) {
                break;
            }
        }

        int pivot = 0;
        for (int i = nullPos - 1; i >= 0; i--) {
            chars[length - 1 - pivot++] = chars[i];
        }

        pivot = 0;
        for (int i = length - nullPos; i < length; i++) {
            if (chars[i] != ' ') {
                chars[pivot++] = chars[i];
            } else {
                chars[pivot++] = '%';
                chars[pivot++] = '2';
                chars[pivot++] = '0';
            }
        }

        return;
    }
}
