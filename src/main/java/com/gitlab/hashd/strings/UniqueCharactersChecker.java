package com.gitlab.hashd.strings;

import java.util.HashSet;
import java.util.Set;

public class UniqueCharactersChecker {
    public boolean check(String str) {
        if (str == null) return true;

        Set<Character> characterSet = new HashSet<>();
        for (int i = 0; i < str.length(); i++) {
            char curr = str.charAt(i);
            if (characterSet.contains(curr)) {
                return false;
            }
            characterSet.add(curr);
        }

        return true;
    }
}
