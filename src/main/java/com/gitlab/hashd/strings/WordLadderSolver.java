package com.gitlab.hashd.strings;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/*
Given two words (start and end), and a dictionary, find the length of
shortest transformation sequence from start to end, such that only one
letter can be changed at a time and each intermediate word must exist in the dictionary.
 */
public class WordLadderSolver {
    class WordNode {
        String word;
        int moves;

        public WordNode(String word, int moves) {
            this.word = word;
            this.moves = moves;
        }
    }

    public int solve(String src, String des, Set<String> dictionary) {
        Queue<WordNode> queue = new LinkedList<>();
        queue.add(new WordNode(src, 1));
        dictionary.add(des);

        while (!queue.isEmpty()) {
            WordNode currNode = queue.remove();
            String currWord = currNode.word;
            int currMoves = currNode.moves;

            if (currWord.equals(des)) {
                return currMoves;
            }

            char[] currWordChars = currWord.toCharArray();
            for (int i = 0; i < currWordChars.length; i++) {
                for (char ch = 'a'; ch <= 'z'; ch++) {
                    if (currWordChars[i] == ch) {
                        continue;
                    }
                    char tmp = currWordChars[i];
                    currWordChars[i] = ch;

                    String newWord = String.valueOf(currWordChars);
                    if (dictionary.contains(newWord)) {
                        queue.add(new WordNode(newWord, currMoves + 1));
                    }

                    currWordChars[i] = tmp;
                }
            }
        }

        return -1;
    }
}
