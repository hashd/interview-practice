package com.gitlab.hashd.strings;

import java.util.Arrays;

public class WordsReverser {
    public String reverse(String string, String separator) {
        return Arrays.stream(new StringBuilder(string).reverse().toString().split(separator))
                .map(s -> new StringBuilder(s).reverse().toString())
                .reduce("", (s, acc) -> s + acc + " ");
    }

    public static void main(String[] args) {
        WordsReverser wr = new WordsReverser();
        System.out.println(wr.reverse("This is just the beginning", " "));
    }
}
