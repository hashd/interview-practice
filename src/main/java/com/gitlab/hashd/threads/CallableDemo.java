package com.gitlab.hashd.threads;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

class Process implements Callable<String> {
    private int id;

    public Process(int id) { this.id = id; }

    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return "ID: " + id;
    }
}

public class CallableDemo {
    public static void main(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(4);
        List<Future<String>> futuresList = new LinkedList<>();
        for (int i = 0; i < 20; i++) {
            futuresList.add(es.submit(new Process(i)));
        }

        for (Future<String> future: futuresList) {
            try {
                System.out.println(future.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        es.shutdown();
    }
}
