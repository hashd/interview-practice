package com.gitlab.hashd.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ReentrantWorker {
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void produce() throws InterruptedException {
        lock.lock();
        System.out.println("Producer method ...");
        condition.await();
        System.out.println("Producer again ...");
        lock.unlock();
    }

    public void consume() throws InterruptedException {
        lock.lock();
        Thread.sleep(2000);
        System.out.println("Consumer method ...");
        condition.signal();
        System.out.println("Consumer again ...");
        lock.unlock();
    }

    public void print(int mod, int div) throws InterruptedException {
        lock.lock();
        for (int i = 0; i < 100; i++) {
            if (i % div == mod) {
                condition.signal();
                System.out.println(i);
                condition.await();
            }
        }
        lock.unlock();
    }
}

public class ReentrantDemo {
    public static void main(String[] args) {
        ReentrantWorker rew = new ReentrantWorker();
        Thread t1 = new Thread(() -> {
            try {
                rew.print(1, 2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                rew.print(0, 2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t2.start();
        t1.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
