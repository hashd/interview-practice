package com.gitlab.hashd.threads;

class Runner implements Runnable {
    private static final int COUNT = 100;
    private int index;

    public Runner(int index) {
        this.index = index;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT; i++) {
            System.out.println("Runner[" + index + "]: " + i);
        }
    }
}

public class RunnerExample {
    public static void main(String[] args) {
        Runner r1 = new Runner(1);
        Runner r2 = new Runner(2);
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();
    }
}
