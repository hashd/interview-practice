package com.gitlab.hashd.threads;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

enum Downloader {
    INSTANCE;
    private Random random = new Random();
    private Semaphore semaphore = new Semaphore(3, true);

    void downloadData() {
        try {
            semaphore.acquire();
            download();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    private void download() {
        int timeInSeconds = 1 + random.nextInt(5);
        System.out.println("Downloading data from the web: " + timeInSeconds + " secs remaining");
        try {
            Thread.sleep(timeInSeconds * 2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class SemaphoreDemo {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 12; i++) {
            executorService.execute(Downloader.INSTANCE::downloadData);
        }
    }
}
