package com.gitlab.hashd.threads;

public class SimultAdder {
    private static int counter = 0;

    public static void main(String[] args) {
        runThreads();
        System.out.println(counter);
    }

    public static void runThreads() {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) increment();
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) increment();
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private synchronized static void increment() {
        counter++;
    }
}
