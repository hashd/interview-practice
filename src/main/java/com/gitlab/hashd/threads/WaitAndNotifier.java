package com.gitlab.hashd.threads;

import java.util.LinkedList;
import java.util.List;

class Processor {
    private final Object oddLock = new Object();
    private final List<Integer> values = new LinkedList<>();
    private final int LIMIT = 10;
    private int currentValue = 0;

    public void produce() throws InterruptedException {
        synchronized (oddLock) {
            while (true) {
                if (values.size() == LIMIT) {
                    System.out.println("Waiting for consumption to finish");
                    oddLock.notify();
                    oddLock.wait();
                } else {
                    values.add(currentValue++);
                }
            }
        }
    }

    public void consume() throws InterruptedException {
        synchronized (oddLock) {
            while (true) {
                if (values.size() == 0) {
                    System.out.println("Waiting for production to finish");
                    oddLock.notify();
                    oddLock.wait();
                } else {
                    int value = values.remove(0);
                    System.out.println("Value consumed: " + value);
                }
            }
        }
    }

    public void print(int mod, int div) throws InterruptedException {
        synchronized (oddLock) {
            for (int i = 0; i < 100; i++) {
                if (i % div == mod) {
                    System.out.println(i);
                    oddLock.notify();
                    oddLock.wait();
                }
            }
        }
    }
}

public class WaitAndNotifier {
    public static void main(String[] args) {
        Processor p = new Processor();
        Thread t1 = new Thread(() -> {
            try {
                p.print(2, 3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                p.print(1, 3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t2.start();
        t1.start();

        try {
            t1.join(); t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
