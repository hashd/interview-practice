package com.gitlab.hashd.trees;

public class BSTChecker {
    public boolean isBST(BinaryTree root) {
        return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private boolean isBST(BinaryTree node, int min, int max) {
        if (node == null) return true;

        return (node.data > min && node.data < max) &&
                isBST(node.left, min, node.data) &&
                isBST(node.right, node.data, max);
    }
}
