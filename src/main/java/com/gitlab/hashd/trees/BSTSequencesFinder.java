package com.gitlab.hashd.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class BSTSequencesFinder {
    public List<List<Integer>> getSequences(BinaryTree root) {
        List<List<Integer>> sequenceList = new LinkedList<>();

        Stack<BinaryTree> sequenceStack = new Stack<>();
        sequenceStack.add(root);

        List<BinaryTree> sequenceQueue = new ArrayList<>();
        if (root.left != null) sequenceQueue.add(root.left);
        if (root.right != null) sequenceQueue.add(root.right);

        addSequences(sequenceQueue, sequenceStack, sequenceList);
        return sequenceList;
    }

    private void addSequences(List<BinaryTree> sequenceQueue, Stack<BinaryTree> sequenceStack, List<List<Integer>> sequenceList) {
        if (sequenceQueue.size() == 0) {
            addToSequences(sequenceStack, sequenceList);
        }

        for (int i = 0; i < sequenceQueue.size(); i++) {
            BinaryTree node = sequenceQueue.remove(i);
            sequenceStack.push(node);
            if (node.left != null) {
                sequenceQueue.add(node.left);
                addSequences(sequenceQueue, sequenceStack, sequenceList);
            }
            if (node.right != null) {
                sequenceQueue.add(node.right);
                addSequences(sequenceQueue, sequenceStack, sequenceList);
            }
            if (node.left == null && node.right == null && sequenceQueue.isEmpty()) {
                addToSequences(sequenceStack, sequenceList);
            }
            sequenceQueue.add(node);
            sequenceStack.pop();
        }
    }

    private void addToSequences(Stack<BinaryTree> sequenceStack, List<List<Integer>> sequenceList) {
        Stack<BinaryTree> tmpStack = new Stack<>();
        while (!sequenceStack.isEmpty()) {
            tmpStack.push(sequenceStack.pop());
        }
        List<Integer> sequence = new LinkedList<>();
        while (!tmpStack.isEmpty()) {
            BinaryTree node = tmpStack.pop();
            sequence.add(node.data);
            sequenceStack.push(node);
        }
        sequenceList.add(sequence);
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinarySearchTreeCreator().create(2, 1, 3);

        List<List<Integer>> sequenceList = new BSTSequencesFinder().getSequences(bt);
        for (List<Integer> sequence: sequenceList) {
            sequence.forEach(System.out::println);
        }
    }
}
