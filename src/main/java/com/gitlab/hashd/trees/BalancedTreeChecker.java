package com.gitlab.hashd.trees;

public class BalancedTreeChecker {
    private int checkHeight(BinaryTree node) {
        if (node == null) return 0;

        int leftHeight = checkHeight(node.left);
        int rightHeight = checkHeight(node.right);

        if (leftHeight == -1 || rightHeight == -1) {
            return -1;
        }

        if (Math.abs(leftHeight - rightHeight) > 1) {
            return -1;
        }

        return 1 + Math.max(leftHeight, rightHeight);
    }

    public boolean isBalanced(BinaryTree node) {
        return checkHeight(node) != -1;
    }
}
