package com.gitlab.hashd.trees;

public class BinarySearchTreeCreator {
    public BinaryTree create(int... numbers) {
        if (numbers.length == 0) return null;
        BinaryTree root = new BinaryTree(numbers[0]);
        for (int i = 1; i < numbers.length; i++) {
            connect(root, new BinaryTree(numbers[i]));
        }
        return root;
    }

    private void connect(BinaryTree root, BinaryTree node) {
        if (node.data >= root.data && root.right == null) {
            root.right = node;
        } else if (node.data < root.data && root.left == null) {
            root.left = node;
        } else if (node.data < root.data) {
            connect(root.left, node);
        } else {
            connect(root.right, node);
        }
    }
}
