package com.gitlab.hashd.trees;

public class BinaryTree {
    int data;
    BinaryTree left;
    BinaryTree right;
    BinaryTree parent;

    public BinaryTree(int d) {
        data = d;
    }

    public static BinaryTree getSampleTree(int n) {
        BinaryTree[] btNodes = new BinaryTree[n + 1];
        for (int i = 1; i <= n; i++) {
            btNodes[i] = new BinaryTree(i);
        }
        for (int i = 1; i <= n/2; i++) {
            if (2 * i <= n) {
                btNodes[i].left = btNodes[2 * i];
            }
            if (2 * i <= n - 1) {
                btNodes[i].right = btNodes[2 * i + 1];
            }
        }
        return btNodes[1];
    }

    public static void preorderProcess(BinaryTree node, BinaryTreeProcessor fn) {
        if (node == null) {
            return;
        }

        preorderProcess(node.left, fn);
        fn.apply(node);
        preorderProcess(node.right, fn);
    }


    public static void inorderProcess(BinaryTree node, BinaryTreeProcessor fn) {
        if (node == null) {
            return;
        }

        fn.apply(node);
        inorderProcess(node.left, fn);
        inorderProcess(node.right, fn);
    }


    public static void postProcess(BinaryTree node, BinaryTreeProcessor fn) {
        if (node == null) {
            return;
        }

        postProcess(node.left, fn);
        postProcess(node.right, fn);
        fn.apply(node);
    }

    public static boolean canOverlap(BinaryTree a, BinaryTree b) {
        // If either is null, they can be overlapped
        if (a == null || b == null) {
            return true;
        }

        // If data being contained isn't same, they can't be overlapped
        if (a.data != b.data) {
            return false;
        }

        // check if the left and right subtrees can be overlapped
        return canOverlap(a.left, b.left) && canOverlap(a.right, b.right);
    }

    public static boolean isSymmetric(BinaryTree a) {
        return isSymmetric(a, a);
    }

    private static boolean isSymmetric(BinaryTree a, BinaryTree b) {
        if (a == null && b == null) return true;
        if (a == null || b == null) return false;

        return (a.data == b.data) && isSymmetric(a.left, b.right) && isSymmetric(a.right, b.left);
    }

    public static int height(BinaryTree tree) {
        if (tree == null) return 0;

        return 1 + Math.max(height(tree.left), height(tree.right));
    }

    public static int diameter(BinaryTree tree) {
        if (tree == null) return 0;

        return 1 + height(tree.left) + height(tree.right);
    }

    public static int sumOfAllNodes(BinaryTree tree) {
        if (tree == null) return 0;

        return tree.data + sumOfAllNodes(tree.left) + sumOfAllNodes(tree.right);
    }
}
