package com.gitlab.hashd.trees;

public interface BinaryTreeProcessor {
    void apply(BinaryTree tree);
}
