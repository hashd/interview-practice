package com.gitlab.hashd.trees;

public class BinaryTreeSerializer {
    public String serialize(BinaryTree root) {
        if (root == null) {
            return "()";
        }

        return "(" + root.data + serialize(root.left) + serialize(root.right) + ")";
    }

    public static void main(String[] args) {
        BinaryTreeSerializer bts = new BinaryTreeSerializer();
        System.out.println(bts.serialize(BinaryTree.getSampleTree(15)));
    }
}
