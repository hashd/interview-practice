package com.gitlab.hashd.trees;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BreadthWiseLinkedListCreator {
    public List<List<BinaryTree>> get(BinaryTree root) {
        List<List<BinaryTree>> depthWiseList = new LinkedList<>();
        Queue<BinaryTree> btq = new LinkedList<>();
        btq.add(root);
        btq.add(null);

        while (!btq.isEmpty()) {
            List<BinaryTree> list = new LinkedList<>();
            while(btq.peek() != null) {
                BinaryTree node = btq.poll();
                btq.add(node.left);
                btq.add(node.right);
            }

            depthWiseList.add(list);

            btq.poll();
            if (!btq.isEmpty()) {
                btq.add(null);
            }
        }

        return depthWiseList;
    }
}
