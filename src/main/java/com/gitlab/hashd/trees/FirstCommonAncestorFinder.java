package com.gitlab.hashd.trees;

public class FirstCommonAncestorFinder {
    public BinaryTree getFirstCommonAncestor(BinaryTree root, BinaryTree nodeA, BinaryTree nodeB) {
        if (root == null) return null;
        if (root == nodeA || root == nodeB) return root;

        BinaryTree leftAncestor = getFirstCommonAncestor(root.left, nodeA, nodeB);
        BinaryTree rightAncestor = getFirstCommonAncestor(root.right, nodeA, nodeB);

        if (leftAncestor != null && rightAncestor != null) {
            if (leftAncestor != rightAncestor) {
                return root;
            } else {
                return leftAncestor;
            }
        } else if (leftAncestor != null) {
            return leftAncestor;
        } else if (rightAncestor != null) {
            return rightAncestor;
        }
        return null;
    }
}
