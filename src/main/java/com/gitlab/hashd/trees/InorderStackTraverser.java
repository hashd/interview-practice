package com.gitlab.hashd.trees;

import java.util.Stack;

public class InorderStackTraverser {
    public void traverseInorder(BinaryTree root) {
        if (root == null) return;

        Stack<BinaryTree> inorderStack = new Stack<>();
        inorderStack.push(root);

        while (!inorderStack.isEmpty()) {
            BinaryTree current = inorderStack.pop();
            System.out.println(current.data);

            if (current.left != null) {
                inorderStack.push(current.left);
            }
            if (current.right != null) {
                inorderStack.push(current.right);
            }
        }
    }
}
