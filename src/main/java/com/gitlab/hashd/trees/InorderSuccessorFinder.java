package com.gitlab.hashd.trees;

public class InorderSuccessorFinder {
    public BinaryTree getInorderSuccessor(BinaryTree tree) {
        if (tree == null) return null;

        BinaryTree pivot = tree.right;
        if (pivot == null) {
            BinaryTree parent = tree.parent;
            while (parent.parent != null && parent.parent.right == parent) {
                parent = parent.parent;
            }
            return parent;
        }

        while (pivot.left != null) {
            pivot = pivot.left;
        }
        return pivot;
    }
}
