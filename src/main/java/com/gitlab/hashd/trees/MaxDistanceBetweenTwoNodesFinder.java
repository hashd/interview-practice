package com.gitlab.hashd.trees;

class InfoTuple {
    int height;
    int maxDistance;

    public InfoTuple(int height, int maxDistance) {
        this.height = height;
        this.maxDistance = maxDistance;
    }
}

/**
 * Find the distance between the farthest two elements in a binary tree.
 *
 * https://www.careercup.com/question?id=5756812753108992
 */
public class MaxDistanceBetweenTwoNodesFinder {
    public InfoTuple getMaxDistanceBetweenTwoNodes(BinaryTree bt) {
        if (bt == null) {
            return new InfoTuple(0, 0);
        }

        InfoTuple left = getMaxDistanceBetweenTwoNodes(bt.left);
        InfoTuple right = getMaxDistanceBetweenTwoNodes(bt.right);
        int height = Math.max(left.height, right.height) + 1;
        int maxDistance = Math.max(Math.max(left.maxDistance, right.maxDistance), 1 + left.height + right.height);

        return new InfoTuple(height, maxDistance);
    }

    public static void main(String[] args) {
        MaxDistanceBetweenTwoNodesFinder mdbtnf = new MaxDistanceBetweenTwoNodesFinder();
        BinaryTreeSerializer bts = new BinaryTreeSerializer();
        BinaryTree bt = new BinarySearchTreeCreator().create(0, 2, 3, 9, 8, 5, 7, 6, 1, 4);

        System.out.println(mdbtnf.getMaxDistanceBetweenTwoNodes(bt).maxDistance);
        System.out.println(bts.serialize(bt));
    }
}
