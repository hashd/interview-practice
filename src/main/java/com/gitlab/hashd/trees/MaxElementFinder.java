package com.gitlab.hashd.trees;

public class MaxElementFinder {
    public int getMax(BinaryTree root) {
        if (root == null) return Integer.MIN_VALUE;

        return Math.max(root.data, Math.max(getMax(root.left), getMax(root.right)));
    }
}
