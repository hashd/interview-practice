package com.gitlab.hashd.trees;

public class MinHeightBSTCreator {
    public BinaryTree create(int[] arr) {
        if (arr == null || arr.length == 0) return null;

        return create(arr, 0, arr.length);
    }

    private BinaryTree create(int[] arr, int start, int end) {
        int mid = start + (end - start) / 2;
        if (start > end) return null;
        if (start == end) return new BinaryTree(arr[mid]);

        BinaryTree node = new BinaryTree(arr[mid]);
        node.left = create(arr, start, mid);
        node.right = create(arr, mid + 1, end);

        return node;
    }
}
