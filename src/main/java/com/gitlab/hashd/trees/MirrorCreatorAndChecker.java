package com.gitlab.hashd.trees;

public class MirrorCreatorAndChecker {
    public void createMirror(BinaryTree root) {
        if (root == null) return;

        BinaryTree leftChild = root.left;
        BinaryTree rightChild = root.right;

        root.left = rightChild;
        root.right = leftChild;

        createMirror(leftChild);
        createMirror(rightChild);
    }

    public boolean isMirror(BinaryTree a, BinaryTree b) {
        if (a == null && b == null) return true;
        if (a == null || b == null) return false;

        return (a.data == b.data) && isMirror(a.left, b.right) && isMirror(a.right, b.left);
    }
}
