package com.gitlab.hashd.trees;

public class NodesCalculator {
    public int getNoOfLeaves(BinaryTree root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;

        return getNoOfLeaves(root.left) + getNoOfLeaves(root.right);
    }

    public int getNoOfFullNodes(BinaryTree root) {
        if (root == null) return 0;

        return getNoOfFullNodes(root.left) + getNoOfFullNodes(root.right) + ((root.left != null && root.right != null) ? 1: 0);
    }

    public int getNoOfHalfNodes(BinaryTree root) {
        if (root == null) return 0;

        return getNoOfHalfNodes(root.left) + getNoOfHalfNodes(root.right) + ((root.left != null && root.right != null) ? 0: 1);
    }
}
