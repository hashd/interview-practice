package com.gitlab.hashd.trees;

public class RightLeafSumCalculator {
    public int calculate(BinaryTree tree) {
        if (tree == null) return 0;

        BinaryTree rightChild = tree.right;
        if (rightChild != null && rightChild.left == null && rightChild.right == null) {
            return rightChild.data + calculate(tree.left);
        } else {
            return calculate(tree.right) + calculate(tree.left);
        }
    }

    public static void main(String[] args) {
        BinaryTree tree = BinaryTree.getSampleTree(15);
        System.out.println(new RightLeafSumCalculator().calculate(tree));
    }
}
