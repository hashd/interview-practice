package com.gitlab.hashd.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Given two Binary Search Trees consisting of unique positive elements, we have to check whether the two BSTs
 * contains same set or elements or not.
 *
 * http://www.geeksforgeeks.org/check-two-bsts-contain-set-elements/
 */
public class SameBSTChecker {
    public boolean isSame(BinaryTree bt1, BinaryTree bt2) {
        List<Integer> bt1List = resolve(bt1);
        List<Integer> bt2List = resolve(bt2);

        for (int i = 0; i < bt1List.size(); i++) {
            if (bt1List.get(i) != (int) bt2List.get(i)) {
                return false;
            }
        }

        return bt1List.size() == bt2List.size();
    }

    private List<Integer> resolve(BinaryTree bt1) {
        Stack<BinaryTree> stack = new Stack<>();
        stack.push(bt1);

        List<Integer> list = new ArrayList<>();
        while (!stack.isEmpty()) {
            BinaryTree bt = stack.pop();
            if (bt.right != null) {
                stack.push(bt.right);
                bt.right = null;
            }

            if (bt.left != null) {
                stack.push(bt);
                stack.push(bt.left);
                bt.left = null;
            } else {
                list.add(bt.data);
            }
        }

        return list;
    }

    public static void main(String[] args) {
        BinarySearchTreeCreator bstc = new BinarySearchTreeCreator();
        BinaryTree bt1 = bstc.create(5, 10, 12, 15, 20, 25);
        BinaryTree bt2 = bstc.create(20, 12, 5, 15, 25, 10);

        SameBSTChecker sbc = new SameBSTChecker();
        System.out.println(sbc.isSame(bt1, bt2));
    }
}
