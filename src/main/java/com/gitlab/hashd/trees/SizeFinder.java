package com.gitlab.hashd.trees;

public class SizeFinder {
    public int size(BinaryTree root) {
        if (root == null) {
            return 0;
        }

        return 1 + size(root.left) + size(root.right);
    }

    public int height(BinaryTree root) {
        if (root == null) {
            return 0;
        }

        return 1 + Math.max(height(root.left), height(root.right));
    }

    public int diameter(BinaryTree root) {
        if (root == null) return 0;

        return 1 + height(root.left) + height(root.right);
    }
}
