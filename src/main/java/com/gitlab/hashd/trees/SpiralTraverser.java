package com.gitlab.hashd.trees;

import java.util.Stack;

public class SpiralTraverser {
    public void print(BinaryTree root) {
        if (root == null) return;
        Stack<BinaryTree> s1 = new Stack<>();
        Stack<BinaryTree> s2 = new Stack<>();
        s1.add(root);

        while (!s1.isEmpty() || !s2.isEmpty()) {
            if (!s1.isEmpty()) {
                while (!s1.isEmpty()) {
                    BinaryTree node = s1.pop();
                    System.out.println(node.data);
                    if (node.right != null) s2.push(node.right);
                    if (node.left != null) s2.push(node.left);
                }
            } else {
                while (!s2.isEmpty()) {
                    BinaryTree node = s2.pop();
                    System.out.println(node.data);
                    if (node.left != null) s1.push(node.left);
                    if (node.right != null) s1.push(node.right);
                }
            }
        }
    }

    public static void main(String[] args) {
        BinaryTree root = BinaryTree.getSampleTree(100);
        new SpiralTraverser().print(root);
    }
}
