package com.gitlab.hashd.trees;

public class StructurallySimilarChecker {
    public boolean isStructurallySimilar(BinaryTree a, BinaryTree b) {
        if (a == null && b == null) return true;
        if (a == null || b == null) return false;

        return a.data == b.data && isStructurallySimilar(a.left, b.left) && isStructurallySimilar(a.right, b.right);
    }
}
