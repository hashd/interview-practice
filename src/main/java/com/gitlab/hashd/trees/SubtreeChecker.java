package com.gitlab.hashd.trees;

public class SubtreeChecker {
    public boolean isSubtree(BinaryTree t1, BinaryTree t2) {
        if (t1 == null) return false;
        if (t1 == t2) return true;

        return isSubtree(t1.left, t2) || isSubtree(t1.right, t2);
    }

    private boolean isSame(BinaryTree t1, BinaryTree t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null || t2 == null) return false;

        boolean hasLeftSubtree = isSame(t1.left, t2.left);
        boolean hasRightSubtree = isSame(t1.right, t2.right);

        return (t1.data == t2.data) && hasLeftSubtree && hasRightSubtree;
    }

    public boolean hasSubtreeFull(BinaryTree t1, BinaryTree t2) {
        return isSame(t1, t2) || hasSubtreeFull(t1.left, t2) || hasSubtreeFull(t1.right, t2);
    }
}
