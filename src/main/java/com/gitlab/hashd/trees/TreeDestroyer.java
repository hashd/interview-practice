package com.gitlab.hashd.trees;

public class TreeDestroyer {
    public void destroy(BinaryTree root) {
        if (root == null) {
            return;
        }

        BinaryTree left = root.left, right = root.right;
        if (left != null) {
            destroy(left);
            root.left = null;
        }

        if (right != null) {
            destroy(right);
            root.right = null;
        }
    }

    public static void main(String[] args) {
        BinaryTree bt1 = new BinaryTree(5);
        BinaryTree bt2 = new BinaryTree(4);
        BinaryTree bt3 = new BinaryTree(3);
        BinaryTree bt4 = new BinaryTree(2);
        BinaryTree bt5 = new BinaryTree(1);
        BinaryTree bt6 = new BinaryTree(6);
        BinaryTree bt7 = new BinaryTree(7);

        bt1.left = bt2; bt1.right = bt3; bt2.left = bt4; bt2.right = bt5; bt5.left = bt6; bt3.right = bt7;
        new TreeDestroyer().destroy(bt1);

        System.out.println();
    }
}
