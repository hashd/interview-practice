package com.gitlab.hashd.trees;

/**
 * Check if there exists a path from the root of the tree to a leaf with a particular sum.
 */
public class TreePathSumChecker {
    public boolean checkIfPathWithSumExists(BinaryTree root, int sum) {
        return checkIfPathWithSumExists(root, sum, 0);
    }

    private boolean checkIfPathWithSumExists(BinaryTree root, int sum, int currentSum) {
        if (root == null) {
            return currentSum == sum;
        }

        return checkIfPathWithSumExists(root.left, sum, currentSum + root.data)
            || checkIfPathWithSumExists(root.right, sum, currentSum + root.data);
    }
}
