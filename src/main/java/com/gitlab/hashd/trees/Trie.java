package com.gitlab.hashd.trees;

import java.util.*;

class TrieNode<T> {
    T data;
    Map<Character, TrieNode<T>> children;

    TrieNode() {
        this.children = new HashMap<>();
    }

    TrieNode(T data) {
        this.data = data;
        this.children = new HashMap<>();
    }
}

public class Trie<T> {
    private TrieNode<T> root;

    public Trie() {
        this.root = new TrieNode<>(null);
    }

    public Trie(T data) {
        this.root = new TrieNode<>(data);
    }

    private TrieNode<T> getNode(String str) {
        if (str == null) return null;

        int length = str.length();
        TrieNode<T> pivot = root;

        for (int i = 0; i < length; i++) {
            if (pivot == null) {
                return null;
            }
            pivot = pivot.children.get(str.charAt(i));
        }

        return pivot;
    }

    public void put(String str, T value) {
        if (str == null) return;

        int length = str.length();

        TrieNode<T> pivot = root;
        for (int i = 0; i < length; i++) {
            if (pivot.children.get(str.charAt(i)) == null) {
                pivot.children.put(str.charAt(i), new TrieNode<>());
            }
            pivot = pivot.children.get(str.charAt(i));
        }
        pivot.data = value;
    }

    public T get(String str) {
        TrieNode<T> node = getNode(str);
        return node == null? null: node.data;
    }

    public static void main(String[] args) {
        Trie<List<String>> trie = new Trie<>();
        Set<String> keySet = new HashSet<>();

        String[] strings = { "star", "start", "rats", "tarts", "art", "rat", "arts", "tar"};
        for (String str: strings) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String sortedStr = String.valueOf(chars);

            keySet.add(sortedStr);
            if (trie.get(sortedStr) == null) {
                trie.put(sortedStr, new LinkedList<>());
            }
            trie.get(sortedStr).add(str);
        }

        for (String string: keySet) {
            System.out.println(string);
            System.out.println("--------------");
            List<String> anagrams = trie.get(string);
            for (String str: anagrams) {
                System.out.println(str);
            }
            System.out.println();
        }
    }
}
