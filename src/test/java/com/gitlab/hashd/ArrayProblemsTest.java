package com.gitlab.hashd;

import com.gitlab.hashd.arrays.*;
import com.gitlab.hashd.arrays.easy.*;
import com.gitlab.hashd.arrays.hard.DutchNationalFlagProblem;
import com.gitlab.hashd.arrays.medium.BinaryArrayZeroSwapper;
import com.gitlab.hashd.arrays.medium.DecodeArrayGivenPairSums;
import com.gitlab.hashd.arrays.medium.InsertPositionSearcher;
import com.gitlab.hashd.combinations.BracketsFormer;
import com.gitlab.hashd.combinations.PermutationsCreator;
import com.gitlab.hashd.dp.HouseRobber;
import com.gitlab.hashd.dp.LongestDecreasingSubsequenceFinder;
import com.gitlab.hashd.dp.LongestIncreasingSubsequenceFinder;
import com.gitlab.hashd.interval.MinimumPlatformsRequiredFinder;
import com.gitlab.hashd.sort.BinaryArraySorter;
import com.gitlab.hashd.utils.Tuple;
import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gitlab.hashd.ArrayProblems.*;
import static org.junit.Assert.*;

public class ArrayProblemsTest {
    @Test
    public void testSortBinaryArray() {
        int[] arr = {0, 0, 1, 0, 0, 1};
        new BinaryArraySorter().sortBinaryArray(arr);

        int[] expected = {0, 0, 0, 0, 1, 1};
        assertArrayEquals(arr, expected);
    }

    @Test
    public void findDuplicateInRange() {
        int[] arr = {1, 2, 3, 4, 4};
        int value = new DuplicateElementFinder().findDuplicateInRange(arr, 1, 4);

        assertEquals(value, 4);
    }

    @Test
    public void findMaximumLengthSubArrayWithSum() {
        int[] arr = {5, 6, -5, 5, 3, 5, 3, -2, 0};
        int value = new MaxLengthSubarrayWithSumSolver().findMaximumLengthSubArrayWithSum(arr, 8);

        assertEquals(4, value);

        int[] secondArr = {4, 2, -3, -1, 0, 4};
        value = new MaxLengthSubarrayWithSumSolver().findMaximumLengthSubArrayWithSum(secondArr, 0);

        assertEquals(4, value);
    }

    @Test
    public void solveDutchNationalFlagProblem() {
        int[] arr = {0, 1, 2, 2, 1, 0, 0, 2, 0, 1, 1, 0};
        new DutchNationalFlagProblem().solve(arr);

        int[] sortedArr = {0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2};
        assertArrayEquals(sortedArr, arr);
    }

    @Test
    public void mergeSortedArraysInPlaceTest() {
        int[] a = {1, 5, 10, 20, 30}, b = {2, 3, 7, 8, 22};
        new MergeSortedArraysInPlace().merge(a, b);

        int[] sortedA = {1, 2, 3, 5, 7};
        int[] sortedB = {8, 10, 20, 22, 30};

        assertArrayEquals(sortedA, a);
        assertArrayEquals(sortedB, b);
    }

    @Test
    public void mergeSortedArrays() {
        int[] x = {0, 2, 0, 3, 0, 5, 6, 0, 0};
        int[] y = {1, 8, 9, 10, 15};

        new MergeSortedArrays().merge(x, y);

        int[] sortedArray = {1, 2, 3, 5, 6, 8, 9, 10, 15};
        assertArrayEquals(sortedArray, x);
    }

    @Test
    public void findIndexProblem1Test() {
        int[] x = {0, 0, 1, 0, 1, 1, 1, 0, 1, 1};
        assertEquals(6, new BinaryArrayZeroSwapper().findIndex(x));
    }

    @Test
    public void findMaximumProductInArrayTest() {
        int[] x = {-10, -4, 5, 6, -2};
        assertEquals(40, new MaximumProductInArrayFinder().findMaxProduct(x));
    }

    @Test
    public void findEquilibriumIndexTest() {
        int[] arr = {7, -3, 5, -4, -2, 3, 5, 3};
        assertEquals(3, new EquilibriumFinder().findEquilibriumIndex(arr));
    }

    @Test
    public void findMajorityElementTest() {
        int[] arr = {2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2};
        assertEquals(2, new MajorElementFinder().findMajorityElement(arr));
    }

    @Test
    public void moveZeroesToEndTest() {
        int[] arr = {6, 0, 8, 2, 3, 0, 4, 0, 1};
        int[] expectedArr = {6, 8, 2, 3, 4, 1, 0, 0, 0};
        new ZeroesToEndMover().moveZeroesToEnd(arr);

        assertArrayEquals(expectedArr, arr);
    }

    @Test
    public void replaceWithProductOtherThanIt() {
        long[] arr = {1, 2, 3, 4, 5};
        long[] exp = {120, 60, 40, 30, 24};
        new ProductOtherThanItself().replaceWithProductOtherThanIt(arr);

        assertArrayEquals(exp, arr);
    }

    @Test
    public void findLargestBitonicSubarrayTest() {
        int[] arr = {3, 5, 8, 4, 5, 9, 10, 8, 5, 3, 4};
        int max = new LargestBitonicSubarrayFinder().findLargestBitonicSubarray(arr);

        assertEquals(7, max);
    }

    @Test
    public void findLargestDifferenceProgressiveTest() {
        int[] arr = {2, 7, 9, 5, 1, 3, 5};
        int max = new StockProfitFinder().profit(arr);

        assertEquals(7, max);
    }

    @Test
    public void findMaximumSubarrayTest() {
        int[] arr = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        assertEquals(new Tuple(3, 6), new MaximumSubarraySumFinder().findMaximumSubarray(arr));
    }

    @Test
    public void findMaximumSubarraySumTest() {
        int[] arr = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        assertEquals(6, new MaximumSubarraySumFinder().findMaximumSubarraySum(arr));
    }

    @Test
    public void testFindMaximumSequenceOfOnes() {
        int[] arr = {1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0};
        assertEquals(4, new MaximumSubsequenceOfOnesFinder().findMaximumSequenceOfOnes(arr, 0));
        assertEquals(7, new MaximumSubsequenceOfOnesFinder().findMaximumSequenceOfOnes(arr, 1));
        assertEquals(10, new MaximumSubsequenceOfOnesFinder().findMaximumSequenceOfOnes(arr, 2));
    }

    @Test
    public void testFindMinimumSumSubarray() {
        int[] arr = {10, 4, 2, 5, 6, 3, 8, 1};
        assertEquals(11, new MinimumSizeSubarraySum().solve(arr, 3));
    }

    @Test
    public void testFindLargestPossibleNumber() {
        int[] arr = {10, 68, 75, 7, 21, 12};
        assertEquals("77568211210", new LargestNumberPossibleFinder().findLargestPossibleNumberS(arr));
    }

    @Test
    public void testFindSmallestWindowToSortToSortArray() {
        int[] arr1 = {1, 2, 3, 7, 5, 6, 4, 8};
        assertEquals(new Pair<>(3, 6), new SmallestWindowToSort().findSmallestWindowToSortToSortArray(arr1));

        int[] arr2 = {1, 3, 2, 7, 5, 6, 4, 8};
        assertEquals(new Pair<>(1, 6), new SmallestWindowToSort().findSmallestWindowToSortToSortArray(arr2));
    }

    @Test
    public void testFindMaxProfitWithMultipleSales() {
        int[] arr = {1, 5, 2, 3, 7, 6, 4, 5};
        assertEquals(10, new StockProfitOneShare().findMaxProfitWithMultipleSales(arr));

        int[] arr2 = {10, 8, 6, 5, 4, 2};
        assertEquals(0, new StockProfitOneShare().findMaxProfitWithMultipleSales(arr2));
    }

    @Test
    public void testFindMaximumAmountOfTrappedWater() {
        int[] arr = {7, 0, 4, 2, 5, 0, 6, 4, 0, 5};
        assertEquals(25, new TrappedWater().findMaximumAmountOfTrappedWater(arr));
        assertEquals(25, new TrappedWater().findMaximumAmountOfTrappedWaterAlt(arr));
    }

    @Test
    public void testFindLongestIncreasingSubsequence() {
        int[] arr = {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
        assertEquals(6, new LongestIncreasingSubsequenceFinder().findLongestIncreasingSubsequence(arr));
    }

    @Test
    public void testFindLongestDecreasingSubsequence() {
        int[] arr = {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
        assertEquals(5, new LongestDecreasingSubsequenceFinder().findLongestDecreasingSubsequence(arr));
    }

    @Test
    public void testFindMaxProductSubarray() {
        int[] arr = {-6, 4, -5, 8, -10, 0, 8};
        assertEquals(1600, ArrayProblems.findMaxProductSubarray(arr));

        int[] arr2 = {40, 0, -20, -10};
        assertEquals(200, ArrayProblems.findMaxProductSubarray(arr2));
    }

    @Test
    public void testFindMaximumSumSubsequenceWithNoAdjacentElements() {
        int[] arr = {1, 2, 9, 4, 5, 0, 4, 11, 6};
        assertEquals(26, new HouseRobber().maximize(arr));
    }

    @Test
    public void testFindMinimumNumberOfPlatformsNeeded() {
        int[] arrivals = {200, 210, 300, 320, 350, 500};
        int[] departures = {230, 340, 320, 430, 400, 520};

        assertEquals(2, new MinimumPlatformsRequiredFinder().findMinimumNumberOfPlatformsNeeded(arrivals, departures));
    }

    @Test
    public void testDecodeOriginalArray() {
        int[] array = {3, 4, 5, 5, 6, 7};
        int[] orig = {1, 2, 3, 4};

        assertArrayEquals(orig, new DecodeArrayGivenPairSums().decodeOriginalArray(array));
    }

    @Test
    public void testSortArrayWithSingleSwap() {
        int[] arr1 = {3, 8, 6, 7, 5, 9};
        int[] arr2 = {3, 5, 6, 9, 8, 7};
        int[] arr3 = {3, 5, 7, 6, 8, 9};
        int[] sorted = {3, 5, 6, 7, 8, 9};

        assertArrayEquals(sorted, new SortAlmostSortedArray().sortArrayWithSingleSwap(arr1));
        assertArrayEquals(sorted, new SortAlmostSortedArray().sortArrayWithSingleSwap(arr2));
        assertArrayEquals(sorted, new SortAlmostSortedArray().sortArrayWithSingleSwap(arr3));
    }

    @Test
    public void testFindTripletsWithSum() {
        int[] arr = {2, 7, 4, 0, 9, 5, 1, 3};
        int[] arr1 = {0, 1, 5};
        int[] arr2 = {0, 2, 4};
        int[] arr3 = {1, 2, 3};

        List<int[]> expected = new ArrayList<>();
        expected.add(arr1);
        expected.add(arr2);
        expected.add(arr3);

        List<int[]> solutions = new TripletsWithSumFinder().findTripletsWithSum(arr, 6);
        for (int i = 0; i < expected.size(); i++) {
            assertArrayEquals(expected.get(i), solutions.get(i));
        }
    }

    @Test
    public void testFindLongestSequenceWithSameSum() {
        int[] x = {0, 0, 1, 1, 1, 1};
        int[] y = {0, 1, 1, 0, 1, 0};

        Pair<Integer, Integer> expected = new Pair<>(0, 4);
        assertEquals(expected, new LargestContiguousSequenceWithSameSum().findLongestSequenceWithSameSum(x, y));
    }

    @Test
    public void testRearrangeWithConstraints() {
        int[] arr = {1, 3, 4, 2, 0};
        int[] exp = {4, 0, 3, 1, 2};
        ArrayProblems.rearrangeWithConstraints1(arr);
        assertArrayEquals(exp, arr);
    }

    @Test
    public void testReverseConsecutiveElementsWithConstraints1() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] exp1 = {1, 4, 3, 2, 7, 6, 5, 8, 9, 10};
        int[] exp2 = {1, 4, 3, 2, 7, 6, 5, 10, 9, 8};
        int[] exp3 = {1, 2, 3, 6, 5, 4, 7, 8, 9, 10};
        int[] exp4 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        assertArrayEquals(exp1, ArrayProblems.reverseConsecutiveElementsWithConstraints1(arr, 3, 1, 7));
        assertArrayEquals(exp2, ArrayProblems.reverseConsecutiveElementsWithConstraints1(arr, 3, 1, 9));
        assertArrayEquals(exp3, ArrayProblems.reverseConsecutiveElementsWithConstraints1(arr, 3, 3, 5));
        assertArrayEquals(exp4, ArrayProblems.reverseConsecutiveElementsWithConstraints1(arr, 3, 4, 5));
    }

    @Test
    public void testFindMaximumProductOfSubset() {
        int[] arr1 = {-6, 4, -5, 8, -10, 0, 8};
        int[] arr2 = {4, -8, 0, 8};

        assertEquals(15360, ArrayProblems.findMaximumProductOfSubset(arr1));
        assertEquals(32, ArrayProblems.findMaximumProductOfSubset(arr2));
    }

    /*@Test
    public void testFindPairsWithDifference() {
        int[] arr = {1, 5, 2, 2, 2, 5, 5, 4};

        assertEquals(2, ArrayProblems.findPairsWithDifference(arr, 3).size());
        assertEquals(2, ArrayProblems.findPairsWithDifferenceAlt(arr, 3).size());
    }

    @Test
    public void testCheckIfQuadrapletWithSumExists() {
        int[] arr = {2, 7, 4, 0, 9, 5, 1, 3};
        assertTrue(ArrayProblems.checkIfQuadrapletWithSumExists(arr, 20));
    }

    @Test
    public void testFindOddOccurringElement() {
        int[] arr = {4, 3, 6, 2, 6, 4, 2, 3, 4, 3, 3};
        assertEquals(4, ArrayProblems.findOddOccurringElement(arr));
    }

    @Test
    public void testFindOddOccurringElements() {
        int[] arr = {4, 3, 6, 2, 4, 2, 3, 4, 3, 3};
        Pair<Integer, Integer> pair = ArrayProblems.findOddOccurringElements(arr);

        assertEquals(6, (int) pair.getKey());
        assertEquals(4, (int) pair.getValue());
    }

    @Test
    public void testQuickSort() {
        int[] arr = {4, 3, 6, 2, 4, 2, 3, 4, 3, 3};
        int[] sorted = {2, 2, 3, 3, 3, 3, 4, 4, 4, 6};
        ArrayProblems.quickSort(arr);

        // assertArrayEquals(sorted, arr);
    }

    @Test
    public void testMergeOverlappingIntervals() {
        List<Pair<Integer, Integer>> intervals = new LinkedList<>();
        intervals.add(new Pair<>(1, 5));
        intervals.add(new Pair<>(2, 3));
        intervals.add(new Pair<>(4, 6));
        intervals.add(new Pair<>(7, 8));
        intervals.add(new Pair<>(8, 10));
        intervals.add(new Pair<>(12, 15));

        assertEquals(3, ArrayProblems.mergeIntervals(intervals).size());
    }

    @Test
    public void testSelectMaximumActivities() {
        List<Pair<Integer, Integer>> intervals = new ArrayList<>();
        intervals.add(new Pair<>(1, 5));
        intervals.add(new Pair<>(2, 3));
        intervals.add(new Pair<>(4, 6));
        intervals.add(new Pair<>(7, 8));
        intervals.add(new Pair<>(8, 10));
        intervals.add(new Pair<>(12, 15));

        assertEquals(5, ArrayProblems.selectMaximumActivities(intervals).size());
    }

    @Test
    public void testIsMinHeap() {
        int[] arr1 = {2, 3, 4, 5, 10, 15};
        int[] arr2 = {2, 10, 4, 5, 3, 15};

        assertTrue(ArrayProblems.isMinHeap(arr1));
        assertFalse(ArrayProblems.isMinHeap(arr2));
    }

    @Test
    public void testSortKSortedArray() {
        int[] arr = {1, 4, 5, 2, 3, 7, 8, 6, 10, 9};
        int[] sorted = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ArrayProblems.sortKSortedArray(arr, 2);
        assertArrayEquals(sorted, arr);
    }

    @Test
    public void testFindKthLargestElement() {
        int[] arr = {7, 4, 6, 3, 9, 1};
        assertEquals(7, ArrayProblems.findKthLargestElement(arr, 2));
    }

    @Test
    public void testMergeKSortedArrays() {
        int[] arr1 = {10, 20, 30, 40};
        int[] arr2 = {15, 25, 35};
        int[] arr3 = {27, 29, 37, 48, 93};
        int[] arr4 = {32, 33};
        int[][] arrays = {arr1, arr2, arr3, arr4};
        int[] sorted = {10, 15, 20, 25, 27, 29, 30, 32, 33, 35, 37, 40, 48, 93};

        assertArrayEquals(sorted, ArrayProblems.mergeKSortedArrays(arrays));
    }

    @Test
    public void testInsertionSort() {
        int[] sorted = {-10, 0, 10, 20, 30, 40, 50};
        int[] arr = {-10, 0, 10, 20, 30, 40, 50};
        new ArrayShuffler().shuffle(arr);
        ArrayProblems.insertionSort(arr);

        assertArrayEquals(sorted, arr);
    }

    @Test
    public void testSelectionSort() {
        int[] sorted = {-10, 0, 10, 20, 30, 40, 50};
        int[] arr = {-10, 0, 10, 20, 30, 40, 50};
        new ArrayShuffler().shuffle(arr);
        ArrayProblems.selectionSort(arr);

        assertArrayEquals(sorted, arr);
    }

    @Test
    public void testBinarySearch() {
        int[] sortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals(3, ArrayProblems.binarySearch(sortedArray, 4));
        assertEquals(0, ArrayProblems.binarySearch(sortedArray, 1));
        assertEquals(8, ArrayProblems.binarySearch(sortedArray, 9));
        assertEquals(-1, ArrayProblems.binarySearch(sortedArray, 10));
    }

    @Test
    public void testBinarySearchLowerBound() {
        int[] sortedArray = {1, 1, 1, 2, 2, 2, 2, 3, 4, 4, 4};
        assertEquals(0, ArrayProblems.binarySearchLowerBound(sortedArray, 1));
        assertEquals(3, ArrayProblems.binarySearchLowerBound(sortedArray, 2));
        assertEquals(7, ArrayProblems.binarySearchLowerBound(sortedArray, 3));
        assertEquals(8, ArrayProblems.binarySearchLowerBound(sortedArray, 4));
        assertEquals(-1, ArrayProblems.binarySearchLowerBound(sortedArray, 5));
    }

    @Test
    public void testBinarySearchUpperBound() {
        int[] sortedArray = {1, 1, 1, 2, 2, 2, 2, 3, 4, 4, 4};
        assertEquals(2, ArrayProblems.binarySearchUpperBound(sortedArray, 1));
        assertEquals(6, ArrayProblems.binarySearchUpperBound(sortedArray, 2));
        assertEquals(7, ArrayProblems.binarySearchUpperBound(sortedArray, 3));
        assertEquals(10, ArrayProblems.binarySearchUpperBound(sortedArray, 4));
        assertEquals(-1, ArrayProblems.binarySearchUpperBound(sortedArray, 5));
    }

    @Test
    public void testFindNoOfOccurrences() {
        int[] arr = {2, 5, 5, 5, 6, 6, 8, 9, 9, 9};
        assertEquals(3, findNoOfOccurrences(arr, 5));
        assertEquals(2, findNoOfOccurrences(arr, 6));
    }*/
/*
    @Test
    public void segregatePositiveNegativeNumbers() {
        int[] arr = {-10, 20, -30, 40, -50, 60, -70};
        ArrayProblems.segregatePositiveNegativeNumbers(arr);

        int negativeSum = Arrays.stream(Arrays.copyOfRange(arr, 0, 4)).sum();
        int positiveSum = Arrays.stream(Arrays.copyOfRange(arr, 4, 7)).sum();
        assertEquals(-160, negativeSum);
        assertEquals(120, positiveSum);
    }

    @Test
    public void testSortUsingSecondaryArray() {
        int[] arr1 = {5, 8, 9, 3, 5, 7, 1, 3, 4, 9, 3, 5, 1, 8, 4};
        int[] arr2 = {3, 5, 7, 2};

        int[] sorted = {3, 3, 3, 5, 5, 5, 7, 1, 1, 4, 4, 8, 8, 9, 9};
        assertArrayEquals(sorted, sortBasedOnSecondaryArray(arr1, arr2));
    }*/

    @Test
    public void testFindNoOfRotationsInCircularSortedArray() {
        int[] arr1 = {8, 9, 10, 2, 5, 6};
        int[] arr2 = {2, 5, 6, 8, 9, 10};
        int[] arr3 = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5};

        assertEquals(3, findNoOfRotationsInCircularSortedArray(arr1));
        assertEquals(0, findNoOfRotationsInCircularSortedArray(arr2));
        assertEquals(10, findNoOfRotationsInCircularSortedArray(arr3));
    }

    @Test
    public void testfindSmallestMissingElement() {
        int[] arr1 = {0, 1, 2, 6, 9, 11, 15};
        int[] arr2 = {1, 2, 3, 4, 6, 9, 11, 15};
        int[] arr3 = {0, 1, 2, 3, 4, 5, 6};

        assertEquals(3, findSmallestMissingElement(arr1));
        assertEquals(0, findSmallestMissingElement(arr2));
        assertEquals(7, findSmallestMissingElement(arr3));
    }

    /*@Test
    public void testGetRangesSummary() {
        int[] arr = {0, 1, 2, 4, 5, 7};
        List<String> ranges = ArrayProblems.getRangesSummary(arr);

        assertEquals("0 -> 2", ranges.get(0));
        assertEquals("4 -> 5", ranges.get(1));
        assertEquals("7", ranges.get(2));
    }

    @Test
    public void testFindCircularTourThatVisitsAllPetrolPumps() {
        int[] petrol = {4, 6, 7, 4};
        int[] distances = {6, 5, 3, 5};

        assertEquals(1, findCircularTourThatVisitsAllPetrolPumps(distances, petrol));
    }

    @Test
    public void testSortElementsByFrequency() {
        int[] array = {2, 5, 2, 8, 5, 6, 8, 8};
        int[] another = {2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};

        int[] sortedArray = {8, 8, 8, 2, 2, 5, 5, 6};
        int[] sortedAnother = {8, 8, 8, 2, 2, 5, 5, -1, 6, 9999999};

        assertArrayEquals(sortedAnother, sortElementsByFrequency(another));
        assertArrayEquals(sortedArray, sortElementsByFrequency(array));
    }
*/
    @Test
    public void testGetDistinctElementsInSortedArray() {
        int[] array = {1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5};
        int[] exp = {1, 2, 3, 4, 5};

        assertArrayEquals(exp, new DistinctElementsInSortedArray().getUniqueElements(array));
    }

    @Test
    public void testFindMinimumPositivePointsToReachDestination() {
        int[][] array = {{-2, -3, 3},
                {-5, -10, 1},
                {10, 30, -5}
        };

        assertEquals(7, minimumPositivePointsToReachDestination(array));
    }

    @Test
    public void testMinimumSizeSubarraySum() {
        MinimumSizeSubarraySum msss = new MinimumSizeSubarraySum();
        int[] arr = {2, 3, 1, 2, 4, 3};

        assertEquals(2, msss.solve(arr, 7));
    }

    @Test
    public void testInsertPositionSearcher() {
        InsertPositionSearcher ips = new InsertPositionSearcher();
        int[] arr = {1, 3, 5, 6};

        assertEquals(2, ips.findPosition(arr, 5));
        assertEquals(1, ips.findPosition(arr, 2));
        assertEquals(4, ips.findPosition(arr, 7));
        assertEquals(0, ips.findPosition(arr, 0));
    }

    @Test
    public void testLongestConsequenceSequenceFinder() {
        LongestConsequenceSequenceFinder lcsf = new LongestConsequenceSequenceFinder();
        int[] arr = {100, 4, 200, 1, 3, 2};

        assertEquals(4, lcsf.length(arr));
    }

    @Test
    public void testTriangleMinimumPathSumFinder() {
        TriangleMinimumPathSumFinder tmpsf = new TriangleMinimumPathSumFinder();
        int[][] triangle = {
                {2},
                {3, 4},
                {6, 5, 7},
                {4, 1, 8, 3}
        };

        assertEquals(11, tmpsf.sum(triangle));
    }

    @Test
    public void testDuplicatesRemover() {
        DuplicatesRemover dr = new DuplicatesRemover();
        int[] array = {1, 1, 2};
        int[] expected = {1, 2};
        assertArrayEquals(expected, dr.removeDuplicates(array));
    }

    @Test
    public void testElementsRemover() {
        ElementsRemover er = new ElementsRemover();
        int[] array = {1, 1, 2, 3, 2, 4, 5};

        int[] expected = {1, 1, 3, 4, 5};
        assertArrayEquals(expected, er.removeElements(array, 2));
        assertArrayEquals(array, er.removeElements(array, 7));
    }

    @Test
    public void testZeroesToEndMover() {
        ZeroesToEndMover ztem = new ZeroesToEndMover();
        int[] array = {0, 1, 0, 3, 12};
        int[] expected = {1, 3, 12, 0, 0};

        assertArrayEquals(expected, ztem.moveZeroesToEnd(array));
    }

    @Test
    public void testPermutationsCreator() {
        PermutationsCreator pc = new PermutationsCreator();
        int[] array = {1,2,3,4,5};
        List<int[]> permutations = pc.getPermutations(array);

        assertEquals(120, permutations.size());
        for (int[] permutation: permutations) {
            System.out.println(Arrays.toString(permutation));
        }
    }

    @Test
    public void testBracketsFormer() {
        BracketsFormer bf = new BracketsFormer();
        List<String> wellFormedStrings = bf.getWellFormedStrings(3);
        wellFormedStrings.forEach(System.out::println);
    }

    @Test
    public void testGeometricMean() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] rra = {15, 12, 13, 19, 10};
        assertEquals(3.7685, new GeometricMean().calculate(arr), 0.01);
        assertEquals(13.477, new GeometricMean().calculate(rra), 0.01);
    }

    @Test
    public void testUnsortedArraySolver() {
        int[] arr1 = {4, 2, 5, 7};
        int[] arr2 = {11, 9, 12};
        int[] arr3 = {4, 3, 2, 7, 8, 9};

        assertEquals(5, new UnsortedArraySolver().getValue(arr1));
        assertEquals(-1, new UnsortedArraySolver().getValue(arr2));
        assertEquals(7, new UnsortedArraySolver().getValue(arr3));
    }
}
