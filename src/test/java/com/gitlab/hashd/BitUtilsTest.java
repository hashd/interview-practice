package com.gitlab.hashd;

import com.gitlab.hashd.bit.BitUtils;
import org.junit.Assert;
import org.junit.Test;

public class BitUtilsTest {
    @Test
    public void testToUppercase() {
        for (char c = 'a'; c <= 'z'; c++) {
            Assert.assertEquals(Character.toUpperCase(c), BitUtils.toUppercase(c));
        }
    }

    @Test
    public void testToLowercase() {
        for (char c = 'A'; c <= 'Z'; c++) {
            Assert.assertEquals(Character.toLowerCase(c), BitUtils.toLowercase(c));
        }
    }
}
