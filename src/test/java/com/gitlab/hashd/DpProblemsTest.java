package com.gitlab.hashd;

import org.junit.Test;

import static com.gitlab.hashd.DpProblems.*;
import static org.junit.Assert.assertEquals;

public class DpProblemsTest {
    @Test
    public void testFindNumberOfPaths() {
        assertEquals(2, findNumberOfPaths(2, 2));
        assertEquals(6, findNumberOfPaths(3, 3));
        assertEquals(20, findNumberOfPaths(4, 4));
        assertEquals(10, findNumberOfPaths(4, 3));
    }

    @Test
    public void testGetLongestCommonSubstring() {
        assertEquals(4, getLongestCommonSubstring("ABABC", "BABCA"));
    }

    @Test
    public void testGetLCS() {
        assertEquals(4, getLCS("XMJYAUZ", "MZJAWXU"));
        assertEquals(5, getLCS("apple", "pineapple"));
        assertEquals(0, getLCS("apple", ""));
    }

    @Test
    public void testGetLIS() {
        int[] arr1 = {2, 5, 3, 4, 8, 7, 9};
        int[] arr2 = {1, 2, 3, 5, 9, 3, 4};
        int[] arr3 = {};

        assertEquals(5, getLIS(arr1));
        assertEquals(5, getLIS(arr2));
        assertEquals(0, getLIS(arr3));
    }

    @Test
    public void testGetLongestPalindromicSequence() {
        assertEquals(5, getLongestPalindromicSubsequence("ABBDCACB"));
    }

    @Test
    public void testGetSumMax() {
        int[][] matrix = {
                {1, 2, -1, -4, -20},
                {-8, -3, 4, 2, 1},
                {3, 8, 10, 1, 3},
                {-4, -1, 1, 7, -6}
        };

        assertEquals(29, getMaxSum(matrix));
    }
}
