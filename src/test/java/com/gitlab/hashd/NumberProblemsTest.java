package com.gitlab.hashd;

import com.gitlab.hashd.numbers.CountAndSayer;
import com.gitlab.hashd.numbers.SquareRootFinder;
import org.junit.Assert;
import org.junit.Test;

public class NumberProblemsTest {
    @Test
    public void testGcd() {
        Assert.assertEquals(5, NumberProblems.gcd(5, 10));
        Assert.assertEquals(50, NumberProblems.gcd(100, 150));
        Assert.assertEquals(1, NumberProblems.gcd(4, 5));
        Assert.assertEquals(1, NumberProblems.gcd(5, 2));
    }

    @Test
    public void testSquareRootFinder() {
        SquareRootFinder srf = new SquareRootFinder();
        System.out.println(srf.squareRoot(4));
        System.out.println(srf.squareRoot(5));
    }

    @Test
    public void testCountAndSayer() {
        CountAndSayer cas = new CountAndSayer();
        cas.countAndSay(15);
    }
}
