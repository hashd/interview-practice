package com.gitlab.hashd;

import com.gitlab.hashd.strings.*;
import com.gitlab.hashd.trees.BinaryTree;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static com.gitlab.hashd.StringProblems.getBinaryStrings;
import static com.gitlab.hashd.StringProblems.getCombinationStrings;
import static com.gitlab.hashd.StringProblems.getLongestSubstringOfVowels;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StringProblemsTest {
    @Test
    public void testGetBinaryStrings() {
        assertEquals(4, getBinaryStrings(2).size());
        assertEquals(8, getBinaryStrings(3).size());
        assertEquals(16, getBinaryStrings(4).size());
    }

    @Test
    public void testGetCombinationStrings() {
        char[] chars = {'k', 'i', 'r', 'a', 'n'};

        assertEquals(25, getCombinationStrings(chars,2).size());
        assertEquals(125, getCombinationStrings(chars,3).size());
        assertEquals(625, getCombinationStrings(chars,4).size());
    }

    @Test
    public void testGetLongestSubstringOfVowels() {
        assertEquals(2, getLongestSubstringOfVowels("geeksforgeeks"));
        assertEquals(3, getLongestSubstringOfVowels("theaerre"));
    }

    @Test
    public void testAnagramsClassifier() {
        String[] strings = {"star", "tars", "Arts", "rat", "art", "train", "rant", "antr"};
        List<Set<String>> anagramSets = new AnagramsClassifier().classify(strings);

        assertEquals(4, (new AnagramsClassifier().classify(strings).size()));
        assertEquals(5, (new AnagramsClassifier().classify(strings, true).size()));
    }

    @Test
    public void testPalindromeValidator() {
        PalindromeValidator pv = new PalindromeValidator();

        assertEquals(true, pv.isPalindrome("Red rum, sir, is murder"));
        assertEquals(false, pv.isPalindrome("Open source is awesome"));
    }

    @Test
    public void testBinaryAdder() {
        BinaryAdder ba = new BinaryAdder();
        assertEquals("100", ba.add("11", "1"));
    }

    @Test
    public void testLongestSubstringWithoutRepeatingCharacter() {
        LongestSubstringWithoutRepeatingCharacter lswrc = new LongestSubstringWithoutRepeatingCharacter();
        assertEquals("abc", lswrc.longestSubstringWithoutRepeatingCharacter("abcabcbb"));
        assertEquals("b", lswrc.longestSubstringWithoutRepeatingCharacter("bbbbb"));
    }

    @Test
    public void testURLifier() {
        Character[] chars = {'M', 'r', ' ', 'A', 'g', ' ', 'M', 'a', 'x', null, null, null, null};
        new URLifier().replaceSpaces(chars);

        Character[] fixed = {'M', 'r', '%', '2', '0', 'A', 'g', '%', '2', '0', 'M', 'a', 'x'};
        assertArrayEquals(fixed, chars);
    }

    @Test
    public void testPalindromePermutationChecker() {
        assertTrue(new PalindromePermutationChecker().hasPalindromicPermutation("Tact Coa"));
    }
}
