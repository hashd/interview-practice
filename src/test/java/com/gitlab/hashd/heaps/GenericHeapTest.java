package com.gitlab.hashd.heaps;

import com.gitlab.hashd.arrays.easy.ArrayShuffler;
import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;

public class GenericHeapTest {
    @Test
    public void testHeapOperations() {
        int[] arr = {2, 3, 6, 8, 10, 15, 18, 20, 25};
        new ArrayShuffler().shuffle(arr);

        Heap heap = new GenericHeap(Comparator.naturalOrder());
        for (int num: arr) {
            heap.push(num);
        }

        int[] sorted = {2, 3, 6, 8, 10, 15, 18, 20, 25};
        int idx = 0;
        while (heap.size() != 0) {
            assertEquals(sorted[idx++], heap.peek());

            heap.pop();
        }
    }
}
