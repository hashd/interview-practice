package com.gitlab.hashd.heaps;

import com.gitlab.hashd.arrays.easy.ArrayShuffler;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaxHeapTest {
    @Test
    public void testHeapOperations() {
        int[] arr = {2, 3, 6, 8, 10, 15, 18, 20, 25};
        new ArrayShuffler().shuffle(arr);

        Heap heap = new MaxHeap();
        for (int num: arr) {
            heap.push(num);
        }

        int[] sorted = {25, 20, 18, 15, 10, 8, 6, 3, 2};
        int idx = 0;
        while (heap.size() != 0) {
            assertEquals(sorted[idx++], heap.peek());

            heap.pop();
        }
    }
}
