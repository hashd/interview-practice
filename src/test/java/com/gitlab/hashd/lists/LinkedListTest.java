package com.gitlab.hashd.lists;

import org.junit.Assert;
import org.junit.Test;

import static com.gitlab.hashd.lists.LinkedList.*;

public class LinkedListTest {
    private LinkedList getPalindromicLinkedList() {
        LinkedList n0 = new LinkedList(0);
        LinkedList n1 = new LinkedList(1);
        LinkedList n2 = new LinkedList(2);
        LinkedList n3 = new LinkedList(3);
        LinkedList n4 = new LinkedList(4);
        LinkedList n5 = new LinkedList(3);
        LinkedList n6 = new LinkedList(2);
        LinkedList n7 = new LinkedList(1);
        LinkedList n8 = new LinkedList(0);

        connect(n0, n1, n2, n3, n4, n5, n6, n7, n8);
        return n0;
    }

    private LinkedList getLinkedListWithDuplicates() {
        LinkedList n0 = new LinkedList(0);
        LinkedList n1 = new LinkedList(1);
        LinkedList n2 = new LinkedList(2);
        LinkedList n3 = new LinkedList(2);
        LinkedList n4 = new LinkedList(1);
        LinkedList n5 = new LinkedList(0);
        LinkedList n6 = new LinkedList(3);
        LinkedList n7 = new LinkedList(0);
        LinkedList n8 = new LinkedList(4);

        connect(n0, n1, n2, n3, n4, n5, n6, n7, n8);
        // 0 => 1 => 2 => 2 => 1 => 0 => 3 => 0 => 4 => null
        return n0;
    }

    public LinkedList getSortedLinkedList() {
        return null;
    }

    @Test
    public void removeDuplicatesTest() {
        LinkedList n = getLinkedListWithDuplicates();
        new DuplicatesRemover().removeDuplicates(n);

        Assert.assertEquals("0 -> 1 -> 2 -> 3 -> 4 -> ()", LinkedList.toString(n));
        Assert.assertEquals(5, size(n));
    }

    @Test
    public void testConnectAlternates() {
        LinkedList n = getLinkedListWithDuplicates();
        connectAlternateNodes(n);

        Assert.assertEquals("0 -> 2 -> 1 -> 3 -> 4 -> 1 -> 2 -> 0 -> 0 -> ()", LinkedList.toString(n));
    }

    @Test
    public void testAddLastToFirst() {
        LinkedList n = getLinkedListWithDuplicates();
        n = addLastToFirst(n);
        n = addLastToFirst(n);
        n = addLastToFirst(n);

        Assert.assertEquals("3 -> 0 -> 4 -> 0 -> 1 -> 2 -> 2 -> 1 -> 0 -> ()", LinkedList.toString(n));
    }

    @Test
    public void testSplitIntoTwoHalves() {
        LinkedList n = getLinkedListWithDuplicates();
        LinkedList o = splitIntoTwoHalves(n);

        Assert.assertEquals("0 -> 1 -> 2 -> 2 -> 1 -> ()", LinkedList.toString(n));
        Assert.assertEquals("0 -> 3 -> 0 -> 4 -> ()", LinkedList.toString(o));

        LinkedList n1 = getLinkedListWithDuplicates();
        add(n1, new LinkedList(5));
        LinkedList o1 = splitIntoTwoHalves(n1);

        Assert.assertEquals("0 -> 1 -> 2 -> 2 -> 1 -> ()", LinkedList.toString(n1));
        Assert.assertEquals("0 -> 3 -> 0 -> 4 -> 5 -> ()", LinkedList.toString(o1));
    }

    @Test
    public void testIsPalindrome() {
        LinkedList n = getPalindromicLinkedList();
        Assert.assertTrue(isPalindrome(n));
    }

    @Test
    public void testMergeAlternativesTest() {
        LinkedList n = getPalindromicLinkedList();
        LinkedList o = reverse(splitIntoTwoHalves(n));
        n = mergeAlternateNodes(n, o);

        Assert.assertEquals("0 -> 0 -> 1 -> 1 -> 2 -> 2 -> 3 -> 3 -> 4 -> ()", LinkedList.toString(n));
    }

    @Test
    public void testFindIntersectionOfSortedLists() {
        LinkedList n1 = new LinkedList(1);
        LinkedList n2 = new LinkedList(2);
        LinkedList n3 = new LinkedList(3);
        LinkedList n4 = new LinkedList(4);
        LinkedList n5 = new LinkedList(5);
        LinkedList n6 = new LinkedList(6);

        connect(n1, n3, n5, n6);
        connect(n2, n4, n5);

        Assert.assertEquals(n5.data, LinkedList.findIntersectionOfSortedLists(n1, n2).data);
    }

    @Test
    public void testFindKthNodeFromTheEnd() {
        LinkedList ll = getLinkedListWithDuplicates();

        Assert.assertEquals(4, findKthNodeFromEnd(ll, 1).data);
        Assert.assertEquals(0, findKthNodeFromEnd(ll, 2).data);
    }

    @Test
    public void testPrintFromEnd() {
        LinkedList ll = getLinkedListWithDuplicates();
        printFromEnd(ll);
    }

    @Test
    public void testRotationCounter() {
        LinkedList n1 = new LinkedList(1);
        LinkedList n2 = new LinkedList(2);
        LinkedList n3 = new LinkedList(3);
        LinkedList n4 = new LinkedList(4);
        LinkedList n5 = new LinkedList(5);
        LinkedList n6 = new LinkedList(6);

        connect(n5, n6, n1, n2, n3, n4);
        Assert.assertEquals(2, new RotationCounter().findRotations(n5));
        Assert.assertEquals(0, new RotationCounter().findRotations(n1));
    }

    @Test
    public void testNumberAdder() {
        LinkedList a1 = new LinkedList(7);
        LinkedList a2 = new LinkedList(1);
        LinkedList a3 = new LinkedList(8);
        connect(a1, a2, a3);

        LinkedList b1 = new LinkedList(5);
        LinkedList b2 = new LinkedList(9);
        LinkedList b3 = new LinkedList(2);
        connect(b1, b2, b3);

        NumberAdder na = new NumberAdder();
        LinkedList c = na.add(a1, b1);
        System.out.println(LinkedList.toString(c));
    }
}
