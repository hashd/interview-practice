package com.gitlab.hashd.sets;

import org.junit.Assert;
import org.junit.Test;

public class MaximumDisjointSetTest {
    @Test
    public void testGetMaxSizeDisjointSet() {
        char[][] grid = {
            {'.','X','X','X','X','X','X','.','.','.','.'},
            {'.','.','.','X','.','.','X','X','.','.','X'},
            {'.','.','.','X','X','X','X','.','.','.','.'},
            {'.','.','X','.','.','.','.','.','X','.','.'},
            {'.','.','X','X','X','.','.','X','X','.','.'},
            {'.','.','.','.','.','X','X','.','.','.','.'}
        };

        Assert.assertEquals(13, new MaximumDisjointSet().getMaxDisjointSize(grid, 'X', '.'));
    }
}
