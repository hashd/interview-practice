package com.gitlab.hashd.strings;

import org.junit.Assert;
import org.junit.Test;

public class ParenthesisLongestFinderTest {
    @Test
    public void testLength() {
        ParenthesisLongestFinder plf = new ParenthesisLongestFinder();
        Assert.assertEquals(2, plf.length("(()"));
        Assert.assertEquals(4, plf.length(")()())"));
    }
}
