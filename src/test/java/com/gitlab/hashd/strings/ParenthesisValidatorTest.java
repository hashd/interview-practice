package com.gitlab.hashd.strings;

import com.gitlab.hashd.stacks.ParenthesisValidator;
import org.junit.Assert;
import org.junit.Test;

public class ParenthesisValidatorTest {
    @Test
    public void testIsValid() {
        ParenthesisValidator pv = new ParenthesisValidator();
        Assert.assertEquals(true, pv.isValid("()"));
        Assert.assertEquals(true, pv.isValid("()[]{}"));
        Assert.assertEquals(false, pv.isValid("(]"));
        Assert.assertEquals(false, pv.isValid("([)]"));
    }
}
