package com.gitlab.hashd.strings;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringReverserTest {
    @Test
    public void testReverse() {
        StringReverser sr = new StringReverser();
        assertEquals("", sr.reverse(""));
        assertEquals("olleh", sr.reverse("hello"));
        assertEquals("a", sr.reverse("a"));
    }
}
