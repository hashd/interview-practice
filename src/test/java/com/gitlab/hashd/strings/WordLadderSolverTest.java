package com.gitlab.hashd.strings;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class WordLadderSolverTest {
    @Test
    public void testSolve() {
        Set<String> dict = new HashSet<>();
        dict.add("hot");
        dict.add("dot");
        dict.add("dog");
        dict.add("lot");
        dict.add("log");

        Assert.assertEquals(5, new WordLadderSolver().solve("hit", "cog", dict));
    }
}
